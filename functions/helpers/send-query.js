const axios = require("axios").default
require("dotenv").config()

module.exports = async (query, variables) => {
  const result = await axios({
    url: "https://graphql.fauna.com/graphql",
    method: "POST",
    headers: {
      Authorization: `Bearer ${process.env.FAUNADB_SERVER_SECRET}`
    },
    data: {
      query,
      variables
    }
  })

  return result.data
}