"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var axios = require("axios")["default"];

require("dotenv").config();

var _callee = function _callee(query, variables) {
  var result;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(axios({
            url: "https://graphql.fauna.com/graphql",
            method: "POST",
            headers: {
              Authorization: "Bearer ".concat(process.env.FAUNA_SERVER_SECRET)
            },
            data: {
              query: query,
              variables: variables
            }
          }));

        case 2:
          result = _context.sent;
          return _context.abrupt("return", result.data);

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports["default"] = _callee;