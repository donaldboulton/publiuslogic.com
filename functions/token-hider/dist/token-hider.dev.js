"use strict";

var axios = require("axios");

var qs = require("qs");

exports.handler = function _callee(event, context) {
  var API_PARAMS, _process$env$API_SECR, API_SECRET, URL, _ref, data, _error$response, status, statusText, headers, _data;

  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          // apply our function to the queryStringParameters and assign it to a variable
          API_PARAMS = qs.stringify(event.queryStringParameters); // Get env var values defined in our Netlify site UI
          // TODO: change this

          _process$env$API_SECR = process.env.API_SECRET, API_SECRET = _process$env$API_SECR === void 0 ? "shiba" : _process$env$API_SECR; // TODO: customize your URL
          // this is secret too, your frontend won't see this

          URL = "https://dog.ceo/api/breed/".concat(API_SECRET, "/images");
          console.log("Constructed URL is ...", URL);
          _context.prev = 4;
          _context.next = 7;
          return regeneratorRuntime.awrap(axios.get(URL));

        case 7:
          _ref = _context.sent;
          data = _ref.data;
          return _context.abrupt("return", {
            statusCode: 200,
            body: JSON.stringify(data)
          });

        case 12:
          _context.prev = 12;
          _context.t0 = _context["catch"](4);
          _error$response = _context.t0.response, status = _error$response.status, statusText = _error$response.statusText, headers = _error$response.headers, _data = _error$response.data;
          return _context.abrupt("return", {
            statusCode: _context.t0.response.status,
            body: JSON.stringify({
              status: status,
              statusText: statusText,
              headers: headers,
              data: _data
            })
          });

        case 16:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[4, 12]]);
};