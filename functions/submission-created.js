if (!process.env.NETLIFY) {
    // get local env vars if not in CI
    // if in CI i expect its already set via the Netlify UI
    require('dotenv').config();
}

if (!process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL) throw new Error('no GOOGLE_SERVICE_ACCOUNT_EMAIL env var set');
if (!process.env.GOOGLE_PRIVATE_KEY) throw new Error('no GOOGLE_PRIVATE_KEY env var set');
if (!process.env.GOOGLE_SPREADSHEET_ID_FROM_URL) throw new Error('no GOOGLE_SPREADSHEET_ID_FROM_URL env var set');

const credentials = require("../credentials.json");
const { GoogleSpreadsheet } = require("google-spreadsheet");

exports.handler = async (event, context) => { 
    const doc = new GoogleSpreadsheet('1YJqm-A2XuIEnmGV7WSbpWW6xhxcGJOQX2z6im3R8xz8'); 
    const loadInfo = async function() {     
      await document.loadInfo(),
      console.log(loadInfo);
    }
    
    const userCredentials = async function() {    
      await doc.useServiceAccountAuth(credentials),
      console.log(userCredentials);
    }

    const accountInfo = async function() {    
      await doc.useServiceAccountAuth({
        client_email: process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL,          
        private_key: process.env.GOOGLE_PRIVATE_KEY.replace(/\\n/g, '\n')
      });
      console.log(accountInfo);
    }
    
    const sheet = doc.sheetsByIndex[0];

    try {
        const data = JSON.parse(event.body).payload.data;
        const addedRow = (async function() {
          await sheet.addRow(data);
        }());
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: `POST Success - added row ${addedRow._rowNumber - 1}`
            })
        };
    } catch (err) {
        console.error('error ocurred in processing ', event);
        console.error(err);
        return {
            statusCode: 500,
            body: err.toString()
        };
    }
};
