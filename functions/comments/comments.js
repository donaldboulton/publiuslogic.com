const faunadb = require('faunadb')
const query = faunadb.query
// Push comments from Netlify to FaunaDB. Fires on commment submission
// Based on:
// https://github.com/chrisjm/chrisjmears.com/blob/0361a03991b6a9ddd27d0514bc714b29a37908e2/js/spam-filter.js

// EXTERNAL LIBS
const https = require('https') // For POST requests

// CONSTANTS
// Or, if you have it set up through Netlify:
// let apiKey = process.env.FAUNA_KEY

const faunadbEndpoint = "https://graphql.fauna.com/graphql"

exports.handler = (event, context, callback) => {
    const apiKey = new faunadb.Client({
      secret: process.env.FAUNADB_SERVER_SECRET
    }) 
  // 1. Parse the form
  try {
    var body = JSON.parse(event.body)
  } catch (e) {
    console.log(`Invalid JSON - ${e.message}`)
    callback(null, {
      statusCode: 400,
      body: `[ERROR] Invalid JSON - ${e.message}`
    })
  }

  const payloadData = body.payload.data

  // 2. Ensure it contains the relevant pieces
  if (!(payloadData.name && payloadData.comment && payloadData.post_slug)) {
    console.log(`ERROR - Required fields not defined.`)
    callback(null, {
      statusCode: 400,
      body: `[ERROR] - Required fields not defined`
    })
  }

  // 3. Push data to FaunaDB via graphQL

  const faunadbPayload = JSON.stringify({
    "query": `mutation{
      createComment(
        data: {
          submitter: "${payloadData.name}",
          post_slug: "${payloadData.post_slug}",
          body: "${payloadData.comment}"
        }
      )
      {submitter, post_slug, body}
    }`
  })

  const postOptions = {
    method: "POST",
    headers: {
      Authorization: `Bearer ${apiKey}`,
      'Content-Type': 'application/json',
      'Content-Length': faunadbPayload.length},
  }

  const req = https.request(
    faunadbEndpoint,
    postOptions,
    res => {
      if (res.statusCode == 200) {
        res.setEncoding('utf8')

        res.on('data', function (chunk) {
          const chunkData = JSON.parse(chunk)
          if (chunkData.errors) {

            console.log(`ERROR - ${JSON.stringify(chunkData.errors)}`)

            callback(null, {
              statusCode: 400,
              body: `[ERROR] ${JSON.stringify(chunkData.errors)}`
            })
          } else {
            callback(null, {
              statusCode: 200,
              body: `[SUCCESS]`
            })
          }
        })
      } else {

        res.setEncoding('utf8')

        res.on('data', function (chunk) {
          console.log(`ERROR - ${chunk}`)

          callback(null, {
            statusCode: 400,
            body: `[ERROR] ${chunk}`
          })
        })
      }
    }
  )

  req.write(faunadbPayload)

  req.end()  
}