const faunadb = require('faunadb');

const url = typeof window !== 'undefined' ? window.location.href : ''

if (!process.env.NETLIFY) {
  // get local env vars if not in CI
  // if in CI i expect its already set via the Netlify UI
  require('dotenv').config();
}

exports.handler = async (event, context) => {
  const data = JSON.parse(event.url)
  console.log(event.queryStringParameters)
  console.log('Function `new-hit` invoked', data)
  const q = faunadb.query;
  const client = new faunadb.Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  if (!url) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'Article url not provided',
      }),
    };
  }
  // Check and see if the doc exists.
  const doesDocExist = await client.query(
    q.Exists(q.Match(q.Index('hitsByUrl'), url))
  );
  if (!doesDocExist) {
    await client.query(
      q.Create(q.Collection('hits'), {
        data: { url: url, hits: 0 },
      })
    );
  }
  // Fetch the document for-real
  const document = await client.query(
    q.Get(q.Match(q.Index('hitsByUrl'), url))
  );
  await client.query(
    q.Update(document.ref, {
      data: {
        hits: document.data.hits + 1,
      },
    })
  );
  return {
    statusCode: 200,
    body: JSON.stringify({
      hits: document.data.hits,
    }),
  };
};