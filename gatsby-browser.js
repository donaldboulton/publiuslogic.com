import React from 'react'
import { IdentityContextProvider } from 'react-netlify-identity-widget'
import { ApolloProvider, ApolloClient, InMemoryCache, HttpLink } from '@apollo/client'
import fetch from "isomorphic-fetch"

require('clipboard')
require('prismjs')
require('prismjs/plugins/toolbar/prism-toolbar.js')
require('prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard.js')
require('prismjs/components/prism-javascript')
require('prismjs/components/prism-css')
require('prismjs/components/prism-scss')
require('prismjs/components/prism-jsx')
require('prismjs/components/prism-bash')
require('prismjs/components/prism-json')
require('prismjs/components/prism-diff')
require('prismjs/components/prism-markdown')
require('prismjs/components/prism-graphql')
require("prismjs/themes/prism-okaidia.css")
require("prismjs/plugins/toolbar/prism-toolbar.css")
require ('./static/scss/styles.css')

const token = typeof window !== 'undefined' ? localStorage.getItem('auth-token') : null

const client = new ApolloClient({
    cache: new InMemoryCache(),
    ssrMode: true,
    link: new HttpLink({
        uri: 'https://graphql.fauna.com/graphql',        
        headers: {
          authorization: token ? `Bearer ${token}` : "",
        },
        fetchOptions: {
          credentials: "same-origin"
        },
    }),
    fetch
});

// eslint-disable-next-line react/prop-types
export const wrapRootElement = ({element}) => (
<ApolloProvider client={client}>{element}</ApolloProvider>
)

export const wrapPageElement = ({ element }) => (
  <IdentityContextProvider url='https://publiuslogic-old.netlify.app'>
    {element}
  </IdentityContextProvider>
)
const transitionDelay = 0

export const shouldUpdateScroll = ({
  routerProps: { location },
  getSavedScrollPosition,
}) => {
  if (location.action === 'PUSH') {
    window.setTimeout(() => {
      // console.log('scroll to top')
      window.scrollTo({
        top: 0,
        behavior: 'smooth' // feel free to use or not
      })
    }, transitionDelay)
  } else {
    const savedPosition = getSavedScrollPosition(location) || [0, 0]
    const top = savedPosition[1]
    window.setTimeout(() => {
      // console.log('scroll to saved position')
      window.scrollTo({
        top,
        behavior: 'smooth'
      })
    }, transitionDelay)
  }
  return false
}

export const onServiceWorkerUpdateReady = () => {
  const answer = window.confirm(
    `This application has been updated. ` +
      `Reload to display the latest version?`
  )
  if (answer === true) {
    window.location.reload()
  }
}
