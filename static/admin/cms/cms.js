import CMS from 'netlify-cms-app'

import PagesPreview from './preview-templates/PagesPreview'
import PostsPreview from './preview-templates/PostsPreview'

CMS.registerPreviewTemplate('pages', PagesPreview)
CMS.registerPreviewTemplate('posts', PostsPreview)

export default {
  CMS,
}
