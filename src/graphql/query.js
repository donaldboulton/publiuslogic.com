import { gql } from '@apollo/client'

export const GET_LOGGED_IN = gql`
  query loginUser {
    isLoggedIn
  }
`