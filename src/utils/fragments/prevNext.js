import React from "react"
import { useStaticQuery, graphql } from "gatsby"

function PrevNext() {
  const data = useStaticQuery(graphql`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/blog/"}}, sort: {order: DESC, fields: frontmatter___date}) {
        edges {
          node {
            frontmatter {
              slug
              date(formatString: "MMMM D, Y")
              title
              cover
            }
          }
          next {
            fields {
              slug
            }
            frontmatter {
              title
              cover
            }
          }
          previous {
            fields {
              slug
            }
            frontmatter {
              title
              cover
            }
          }
        }
      }
    }
  `)
  return <pre>{JSON.stringify(data, null, 4)}</pre>
}

export default PrevNext
