import Analytics from 'analytics'
import googleAnalytics from '@analytics/google-analytics'
const { GOOGLE_ANALYTICS_ID } = process.env

let plugins = []
// If google analytics ID set attach plugin
if (GOOGLE_ANALYTICS_ID) {
  plugins = [ googleAnalytics({
      trackingId: GOOGLE_ANALYTICS_ID
    })
  ]
}

export default Analytics({
  app: 'puliuslogic',
  plugins: plugins
})
