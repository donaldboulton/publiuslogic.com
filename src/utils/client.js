import { ApolloClient, InMemoryCache, HttpLink } from "@apollo/client"
import fetch from "isomorphic-fetch";

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: "/.netlify/functions/apollo-graphql",
  }),
  fetch,
});
