import React, { useRef, useState } from 'react'
import { useOnClickOutside } from '../hooks/useOnClickOutside'
import RehypeReact from 'rehype-react'
import { Calendar, FileSymlinkFile } from '@styled-icons/octicons/'
import GithubButtonsRepo from '../components/GithubButtonsRepo'
import Fork from '../components/GithubButtonsRepo/fork'
import Issues from '../components/GithubButtonsRepo/issues'
import { Timer } from '@styled-icons/material/Timer'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'
import { HTMLContent } from '../components/Content'
import PostsPageTemplate from '../components/PostsPageTemplate'
import Layout from '../components/Layout'
import PostCover from '../components/PostCover'
import Counter from '../components/Counter'
import ReactPlayer from 'react-player/lazy'
import Player from '../components/Cloudinary/CloudinaryVideo'
import Todo from '../components/Todo'
import Bio from '../components/Bio'
import HotJar from '../components/HotJar'
import ColorBox from '../components/ColorBox'
import Sparkles from '../components/Sparkles'
import Center from '../components/Center'
import Accordion from '../components/Accordion'
import CenterStyledSpan from '../components/CenterSpan'
import ArrowTrails from '../components/ArrowTrails/ArrowTrails'
import LogRocket from '../components/LogRocket'
import Meta from '../components/Meta/Meta'
import Toc from '../components/Toc'
import SortDate from '../components/SortDate'
import Tags from '../components/SiteTags'
import SiteCategories from '../components/SiteCategories'
import TechLogos from '../components/TechLogos'
import Comments from '../components/Comments'
import CustomImage from '../components/CustomImage'
import { StyledH1, MetaPage, GithubButtons, TagList } from '../components/styles/ArticleStyles'
import { VideoDiv, VideoTitle, VideoToggle, VideoIcon } from '../components/Cloudinary/styles'
import { PageBody, AsideWrapper, BodyWrapper } from '../components/styles/PageBody'
import AudioPlayer from "../components/AudioPlayer"
import Prism from '../utils/prism'

const renderAst = new RehypeReact({
  createElement: React.createElement,
  components: {
    'interactive-counter': Counter,
    'interactive-todo': Todo,
    'interactive-colorbox': ColorBox,
    'interactive-fork': Fork,
    'interactive-issues': Issues,
    'audio': AudioPlayer,
    'interactive-cloudinary-video': Player,
    'interactive-sparkles': Sparkles,
    'interactive-center': Center,
    'interactive-center-span': CenterStyledSpan,
    'interactive-arrow': ArrowTrails,
    'custom-image': CustomImage,
    'interactive-accordion': Accordion,
   },
}).Compiler

const PostsPage = ({ data, location, pageContext }) => {
  const { markdownRemark: post } = data
  const postNode = data.markdownRemark
  const coverHeight = '100%'
  const { prev, next } = pageContext
  const videoUrl = post.frontmatter.videoUrl
  const videoTitle = post.frontmatter.videoTitle
  const showComment = post.frontmatter.showComment
  const showToc = post.frontmatter.showToc
  const showDate = post.frontmatter.showDate
  const showCategory = post.frontmatter.showCategory
  const showTags = post.frontmatter.showTags
  const showHotJar = post.frontmatter.showHotJar
  const showStack = post.frontmatter.showStack
  const showVideo = post.frontmatter.showVideo
  const showLogRocket = post.frontmatter.showLogRocket
  const [open, setOpen] = useState(false)
  const ref = useRef()
  useOnClickOutside(ref, () => setOpen(false))
  return (
    <Layout pageTitle={post.frontmatter.title} location={location}>
      <Prism />
      <Meta
        data={{
          ...post,
          description: post.frontmatter.meta_description,
        }}
      />
      <section className='post-cover'>
        <PostCover
          postNode={postNode}
          coverHeight={coverHeight}
          coverClassName='post-cover'
        />
      </section>
      <PageBody as='div' className='content'>
        <BodyWrapper>          
          <StyledH1>
            {post.frontmatter.title}
          </StyledH1>          
          <Bio />
          <MetaPage>
            <span>
              <Calendar size='1.2em' />
              &ensp;
              {post.frontmatter.date}
            </span>
            <span>
              <Timer size='1.2em' />
              &ensp;
              {postNode.timeToRead} min read
            </span>
            <Link aria-label='Tags' to='/tags/'><TagList tags={post.frontmatter.tags} /></Link>
            <span>
              <FileSymlinkFile size='1.2em' />
              &ensp;
              Category&ensp;
              <Link aria-label='Categories' to='/categories/'>{post.frontmatter.category}</Link>
            </span>
          </MetaPage>
          <main>{renderAst(postNode.htmlAst)}</main>
          <PostsPageTemplate
            content={postNode.html}
            contentComponent={HTMLContent}
            timeToRead={postNode.timeToRead}
            category={post.frontmatter.category}
            date={post.frontmatter.date}
            tweet_id={post.frontmatter.tweet_id}
            title={post.frontmatter.title}
            slug={post.frontmatter.slug}
            cover={post.frontmatter.cover}
            meta_title={post.frontmatter.meta_title}
            meta_description={post.frontmatter.meta_description}
            videoUrl={post.frontmatter.videoUrl}
            videTitle={post.frontmatter.videoTitle}
            tags={post.frontmatter.tags}
            showComment={post.frontmatter.showComment}
            showToc={post.frontmatter.showToc}
            showDate={post.frontmatter.showDate}
            showCategory={post.frontmatter.showCategory}
            showTags={post.frontmatter.showTags}
            showHotJar={post.frontmatter.showHotJar}
            showStack={post.frontmatter.showStack}
            showVideo={post.frontmatter.showVideo}
            showLogRocket={post.frontmatter.showLogRocket}
            published={post.frontmatter.published}
          />
          <hr
            style={{
              marginBottom: 2,
            }}
          />               
          <GithubButtons>
            <GithubButtonsRepo />
          </GithubButtons>
        </BodyWrapper>
        <hr
          style={{
            marginBottom: 2,
          }}
        />
        <AsideWrapper>
          {showToc && <Toc />}
          {showDate && <SortDate />}
          {showCategory && <SiteCategories />}
          {showTags && <Tags />}
          {showHotJar && <HotJar />}
          {showLogRocket && <LogRocket />}
          {showVideo && <>
            <VideoToggle opener open={open} onClick={() => setOpen(true)} />
            <VideoDiv ref={ref} open={open} className='nav-background'>
              <VideoTitle>
                <VideoIcon />
                  {videoTitle}
                  <VideoToggle onClick={() => setOpen(false)} />
                </VideoTitle>
              <div className='center'>
                <ReactPlayer
                  url={videoUrl}
                  pip='true'
                  playing='true'
                  controls='true'
                  width='270px'
                  height='160px'
                  pip='true'
                  config={{
                    file: {
                      attributes: {
                        style: { height: "100%", objectFit: "cover" },
                      },                
                    },
                  }}
               />
              </div>
           </VideoDiv></>}           
        </AsideWrapper>
        <section>
          {showComment && <Comments slug={post.frontmatter.slug} />}            
        </section>
          <div>
            {showStack && <TechLogos />}            
          </div>
      </PageBody>
    </Layout>
  )
}

PostsPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
    helmet: PropTypes.object,
  }),
}

export default PostsPage

export const postQuery = graphql`
  query PostsBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id      
      htmlAst
      timeToRead
      excerpt(pruneLength: 300, truncate: true)                                
      frontmatter {
        date(formatString: "MMM D, YYYY")
        title
        tweet_id
        path
        slug
        category
        meta_title
        meta_description
        published
        videoUrl
        videoTitle
        videoPoster
        tags
        showComment
        showToc
        showDate
        showCategory
        showHotJar
        showTags
        showStack
        showVideo
        showLogRocket
        cover
      }
    }
  }
`