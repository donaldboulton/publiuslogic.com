import React from 'react'
import RehypeReact from 'rehype-react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import PostCover from '../components/PostCover'
import Bio from '../components/Bio'
import { PageBody, AsideWrapper, BodyWrapper } from '../components/styles/PageBody'
import { VideoDiv, VideoTitle, VideoIcon } from '../components/Cloudinary/styles'
import Toc from '../components/Toc'
import { HTMLContent } from '../components/Content'
import PagesTemplate from '../components/PagesTemplate'
import Layout from '../components/Layout'
import Tags from '../components/SiteTags'
import SiteCategories from '../components/SiteCategories'
import HotJar from '../components/HotJar'
import TechLogos from '../components/TechLogos'
import Sparkles from '../components/Sparkles'
import config from '../../_data/config'
import ReactPlayer from 'react-player/lazy'
import AdvancedVideo from '../components/Cloudinary/AdvancedVideo'
import Accordion from '../components/Accordion'
import Subscriptions from '../components/Subscriptions'
import Contact from '../components/Contact'
import Fork from '../components/GithubButtonsRepo/fork'
import LogRocket from '../components/LogRocket'
import Issues from '../components/GithubButtonsRepo/issues'
import { StyledH1 } from '../components/styles/ArticleStyles'
import CustomImage from "../components/CustomImage"
import ImageGallery from '../components/ImageGallery'
import UploadWidget from '../components/Cloudinary/UploadWidget'

const renderAst = new RehypeReact({
  createElement: React.createElement,
  components: {
    'interactive-contact': Contact,
    'interactive-fork': Fork,
    'interactive-issues': Issues,
    'custom-image': CustomImage,
    'interactive-cloudinary-video': AdvancedVideo,
    'interactive-sparkles': Sparkles,
    'interactive-gallery': ImageGallery,
    'interactive-upload-widget': UploadWidget,
    'subscriptions': Subscriptions,
    'interactive-accordion': Accordion,
  },
}).Compiler

const PagePages = ({ data, location }) => {
  const { markdownRemark: post } = data
  const postNode = data.markdownRemark
  const videoUrl = post.frontmatter.videoUrl
  const videoTitle = post.frontmatter.videoTitle
  const coverHeight = '100%'
  const author = config.author
  const logo = config.siteLogo
  const image = post.frontmatter.cover
  const title = post.frontmatter.title
  const showToc = post.frontmatter.showToc
  const showCategory = post.frontmatter.showCategory
  const showTags = post.frontmatter.showTags
  const showHotJar = post.frontmatter.showHotJar
  const showLogRocket = post.frontmatter.showLogRocket
  const showStack = post.frontmatter.showStack
  const showVideo = post.frontmatter.showVideo

  const schemaOrgWebPage = {
    '@context': 'http://schema.org',
    '@type': 'WebPage',
    url: `${config.siteUrl}${post.frontmatter.slug}`,
    inLanguage: config.siteLanguage,
    mainEntityOfPage: {
      '@type': 'WebPage',
      '@id': `${config.siteUrl}${post.frontmatter.slug}`,
    },
    description: config.siteDescription,
    name: title,
    author: {
      '@type': 'Person',
      name: 'donaldboulton',
    },
    copyrightHolder: {
      '@type': 'Person',
      name: 'donaldboulton',
    },
    copyrightYear: config.copyrightYear,
    creator: {
      '@type': 'Person',
      name: 'donboulton',
    },
    publisher: {
      '@type': 'Organization',
      name: 'donaldboulton',
      logo: {
        '@type': 'ImageObject',
        url: logo,
        width: '450px',
        height: '450px',
      },
    },
    datePublished: '2019-07-12T10:30:00+01:00',
    dateModified: '2020-04-12T10:30:00+01:00',
    image: {
      '@type': 'ImageObject',
      url: image,
    },
  }

  return (
    <Layout pageTitle={post.frontmatter.title} location={location}>
      <Helmet>
        <title>{post.frontmatter.meta_title}</title>
        <meta name='description' content={post.frontmatter.meta_description} />
        <meta name='keywords' content={post.frontmatter.tags} />
        <meta name='image' content={image} />
        <meta name='url' content={`${config.siteUrl}${post.frontmatter.slug}`}/>
        <meta name='video' content={post.frontmatter.videoUrl} />
        <meta name='video-title' content={post.frontmatter.videoTitle} />
        <meta name='video-poster' content={post.frontmatter.videoPoster} />
        <meta name='author' content={author} />
        <meta property='og:type' content='article' />
        <meta property='og:title' content={post.frontmatter.title} />
        <meta property='og:description' content={post.frontmatter.meta_description} />
        <meta property='og:image' content={post.frontmatter.cover} />
        <meta property='og:image:alt' content={post.frontmatter.meta_title} />
        <meta property='og:image:width' content='100%' />
        <meta property='og:image:height' content='400px' />
        <meta property='og:url' content={`${config.siteUrl}${post.frontmatter.slug}`} />
        <meta name='rel' content={`${config.siteUrl}${post.frontmatter.slug}`} />
        <meta name='key' content={`${config.siteUrl}${post.frontmatter.slug}`} />
        <meta name='twitter:author' content='donboulton' />
        <meta name='twitter:card' content='summary_large_image' />
        <meta name='twitter:title' content={post.frontmatter.title} />
        <meta name='twitter:image' content={image} />
        <meta name='twitter:description' content={post.frontmatter.meta_description} />
        <meta name='twitter:widgets:autoload' content='off' />
        <meta name='twitter:widgets:theme' content='dark' />
        <meta name='twitter:widgets:link-color' content='#d64000' />
        <meta name='twitter:widgets:border-color' content='#000000' />
        <meta name='twitter:dnt' content='on' />
        <link rel='canonical' href={`${config.siteUrl}${post.frontmatter.slug}`} />
        <link rel='image_src' href={`${config.siteUrl}${config.logo}`} />
        <link rel='me' href='https://twitter.com/donboulton' />
        <link rel="sitemap" type="application/xml" title="Sitemap Index" href="/static/sitemap.xml" />
        <link rel="sitemap" type="application/xml" title="Sitemap Page" href="/static/sitemap-pages.xml" />
        <link rel="sitemap" type="application/xml" title="Sitemap Posts" href="/static/sitemap-posts.xml" />
        <link rel="sitemap" type="application/xml" title="Sitemap Tags" href="/static/sitemap-tags.xml" />
        <link rel="sitemap" type="application/xml" title="Sitemap Categories" href="/static/sitemap-categories.xml" />
        {/* Schema.org tags */}
        <script type='application/ld+json'>{JSON.stringify(schemaOrgWebPage)}</script>
      </Helmet>
      <section className='post-cover'>
        <PostCover
          postNode={postNode}
          coverHeight={coverHeight}
          coverClassName='post-cover'
        />
      </section>
      <PageBody as='div' className='content'>
        <BodyWrapper>
          <StyledH1>
            <span>{post.frontmatter.title}</span>
          </StyledH1>
          <Bio />
          <main>{renderAst(postNode.htmlAst)}</main>
          <PagesTemplate
            content={postNode.html}
            contentComponent={HTMLContent}
            date={post.frontmatter.date}
            title={post.frontmatter.title}
            cover={post.frontmatter.cover}
            meta_title={post.frontmatter.meta_title}
            meta_description={post.frontmatter.meta_description}
            videoUrl={post.frontmatter.videoUrl}
            videTitle={post.frontmatter.videoTitle}
            videoPoster={post.frontmatter.videoPoster}
            tags={post.frontmatter.tags}
            showToc={post.frontmatter.showToc}
            showCategory={post.frontmatter.showCategory}
            showTags={post.frontmatter.showTags}
            showHotJar={post.frontmatter.showHotJar}
            showStack={post.frontmatter.showStack}
            showVideo={post.frontmatter.showVideo}
            showLogRocket={post.frontmatter.showLogRocket}
          />
          <hr
            style={{
              marginBottom: 2,
            }}
          />     
        </BodyWrapper>
        <AsideWrapper>
          {showToc && <Toc />}
          {showCategory && <SiteCategories />}
          {showTags && <Tags />}
          {showHotJar && <HotJar />}
          {showLogRocket && <LogRocket />}
          {showVideo && 
            <VideoDiv className='nav-background'>
              <VideoTitle>
                <VideoIcon />
                  {videoTitle}
                </VideoTitle>
              <div className='center'>
                <ReactPlayer
                  url={videoUrl}
                  muted='false'
                  pip='true'
                  playing='true'
                  controls='true'
                  width='270px'
                  height='160px'
               />
              </div>
           </VideoDiv>}
        </AsideWrapper>
          <div>
            {showStack && <TechLogos />}            
          </div>
      </PageBody>
    </Layout>
  )
}

PagePages.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
    helmet: PropTypes.object,
  }),
}

export default PagePages

export const pageQuery = graphql`
  query PagesBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      htmlAst
      excerpt(pruneLength: 200, truncate: true) 
      frontmatter {
        date(formatString: "MMM D, YYYY")
        title
        path
        slug
        meta_title
        meta_description
        published
        videoUrl
        videoTitle
        tags
        showToc
        showCategory
        showTags
        showHotJar
        showStack
        showVideo
        showLogRocket
        cover
      }
    }
  }
`
