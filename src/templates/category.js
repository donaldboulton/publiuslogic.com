import React from 'react'
import Helmet from 'react-helmet'
import { Link, graphql } from 'gatsby'
import Masonry from 'react-masonry-css'
import Layout from '../components/Layout'
import Tags from '../components/SiteTags'
import SiteCategories from '../components/SiteCategories'
import { StyledH1 } from '../components/styles/ArticleStyles'
import { Meta, Category, Post, PostContent, Cover } from '../components/PostCard/styles'
import { Calendar, FileSymlinkFile } from '@styled-icons/octicons/'
import { Timer } from '@styled-icons/material/Timer'
import useSound from 'use-sound'
import pong from '../../static/audio/pong-pop.wav'
import ArrowTrails from '../components/ArrowTrails/ArrowTrails'
import { PageBody, BodyWrapper, AsideWrapper } from '../components/styles/PageBody'
import styled from 'styled-components'
import mediaQuery from '../utils/mediaQuery'

const breakpointColumnsObj = {
  default: 2,
  1100: 2,
  700: 1,
  500: 1
}

const breakpointColumnsObjKeys = Object.keys(breakpointColumnsObj);

const Root = styled.div`
  .my-masonry-grid_column {
    width: ${props => props.dataLength === 3 && "33.3333% !important"};
    width: ${props => props.dataLength === 2 && "50% !important"};
    width: ${props => props.dataLength === 1 && "100% !important"};
  }

  ${mediaQuery.minPhablet} { 
    (max-width: ${breakpointColumnsObjKeys[2]}px) {
      .my-masonry-grid_column {
        width: ${props => props.dataLength === 2 && "50% !important"};
        width: ${props => props.dataLength === 1 && "100% !important"};
      }
    }
  }

  ${mediaQuery.minPhablet} {
    (max-width: ${breakpointColumnsObjKeys[1]}px) {
      .my-masonry-grid_column {
        width: ${props => props.dataLength === 1 && "100% !important"};
      }
    }
  }
`;


const CategoryRoute = ({ data, slug, cover, location, pageContext, timeToRead, postNode, coverClassName, frontmatter, inTitle = false, ...rest }) => {
    const posts = data.allMarkdownRemark.edges
    const title = data.site.siteMetadata.title
    const [play] = useSound(pong)
    const category = pageContext.category
    const totalCount = data.allMarkdownRemark.totalCount    
    const categoryHeader = `${totalCount} post${
      totalCount === 1 ? '' : 's'
    } categorized with “${category}”`

    return (
      <Layout pageTitle={title} location={location}>
        <PageBody as='div'>
          <BodyWrapper>
            <StyledH1>
              Blog Posts Categories
            </StyledH1>
            <section
              style={{ marginBottom: '6rem' }}
            >
              <Helmet title={`${category} | ${title}`} />
              <h3 
                style={{ margin: '2em', }}
                className='title box'>
                  {categoryHeader}
              </h3>  
            </section>
            <Root>
            <Masonry
              breakpointCols={breakpointColumnsObj}
              className="my-masonry-grid"
              columnClassName="my-masonry-grid_column"
            >
             {posts.map((post, index) => (
               <Post key={`${index}-posts`}>
                 <Link to={post.node.fields.slug}>
                    <div className='container'>
                      <Cover src={post.node.frontmatter.cover} alt={post.node.frontmatter.title} />
                    </div>
                    <div
                      style={{
                        marginLeft: '1em',
                      }}
                    >
                      <h3 class='h3 a' aria-label='Page Title'>{post.node.frontmatter.title}</h3>               
                    </div>
                </Link>
                    <Meta inTitle={inTitle}>
                    <div
                      style={{
                        marginLeft: '1em',
                      }}
                    >
                      <span>
                        <Calendar size='1em' /><small>&nbsp;{post.node.frontmatter.date}</small>&nbsp;
                        <Timer size='1em' />&nbsp;
                          <small key={post.node.timeToRead}>{post.node.timeToRead}&nbsp;min read</small>&nbsp;
                        <Category><FileSymlinkFile size='1em' />&nbsp;<small>Category:</small>
                          &nbsp;
                          <Link aria-label='Categories' to='/categories/'>
                            <small>{post.node.frontmatter.category}</small>
                          </Link>
                        </Category>
                      </span>
                    </div>
                    </Meta>            
                    <PostContent>
                      <div
                        style={{
                          marginBottom: '1em',
                        }}
                      >
                        {post.node.excerpt}
                      </div>
                    </PostContent> 
                    <div
                      style={{
                        display: 'inline-block',
                        marginBottom: '1.5em',
                        marginLeft: '1.5em',
                      }}
                    >
                    <span
                      onMouseEnter={() => {
                        play();
                      }}
                    >
                      <Link
                        style={{                    
                          position: 'absolute',
                        }}
                        aria-label='Read More'
                        className='h2 a'
                        to={post.node.fields.slug}
                      >
                        <span>Page <ArrowTrails /></span>
                      </Link>                  
                    </span>
                  </div>               
                </Post>     
              ))}
            </Masonry>
            </Root>
            <section>
              <p>
                <Link aria-label='Browse all Categories' to='/categories/'>
                  <span
                    style={{ marginBottom: '2em', marginLeft: '2em', }}
                    onMouseEnter={() => {
                      play();
                    }}
                  >
                    Browse all Categories&nbsp;
                    <span>
                      <ArrowTrails />  
                    </span>                  
                  </span>                   
                </Link>
              </p>
            </section>
          </BodyWrapper>
          <AsideWrapper>
            <SiteCategories />
            <Tags />
          </AsideWrapper>
        </PageBody>
      </Layout>
    )
}

export default CategoryRoute

export const categoryPageQuery = graphql`
  query CategoryPage($category: String) {
    site {
      siteMetadata {
        title
      }
    } 
    allMarkdownRemark(
      limit: 1000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { category: { in: [$category] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            category
            date
            cover
          }
          timeToRead
          excerpt(pruneLength: 300, truncate: true)
        }
      }
    }
  }
`

