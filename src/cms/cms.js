import CMS from 'netlify-cms-app'
import cloudinary from 'netlify-cms-media-library-cloudinary'
import PagesPreview from './preview-templates/PagesPreview'
import PostsPreview from './preview-templates/PostsPreview'

CMS.registerMediaLibrary(cloudinary)
CMS.registerPreviewTemplate('pages', PagesPreview)
CMS.registerPreviewTemplate('posts', PostsPreview)

export default {
  CMS,
}
