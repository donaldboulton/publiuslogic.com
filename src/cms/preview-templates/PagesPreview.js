import React from 'react'
import PropTypes from 'prop-types'
import PagesTemplate from '../../components/PagesTemplate'

const PagesPreview = ({ entry, widgetFor }) => (
  <PagesTemplate
    title={entry.getIn(['data', 'title'])}
    cover={entry.getIn(['data', 'cover'])}
    date={entry.getIn(['data', 'datetime'])}
    meta_title={entry.getIn(['data', 'meta_title'])}
    meta_description={entry.getIn(['data', 'meta_description'])}
    category={entry.getIn(['data', 'category'])}
    tags={entry.getIn(['data', 'tags'])}
    showToc={entry.getIn(['data', 'showToc'])}
    showTags={entry.getIn(['data', 'showTags'])}
    showHotJar={entry.getIn(['data', 'showHotJar'])}
    showStack={entry.getIn(['data', 'showStack'])}
    showVideo={entry.getIn(['data', 'showVideo'])}
    slug={entry.getIn(['data', 'slug'])}
    content={widgetFor('body')}
  />
)

PagesPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default PagesPreview
