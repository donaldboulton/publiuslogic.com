import React from 'react'
import PropTypes from 'prop-types'
import PostsPageTemplate from '../../components/PostsPageTemplate'

const PostsPreview = ({ entry, widgetFor }) => {
  return (
    <PostsPageTemplate
      title={entry.getIn(['data', 'title'])}
      cover={entry.getIn(['data', 'cover'])}
      meta_title={entry.getIn(['data', 'meta_title'])}
      meta_description={entry.getIn(['data', 'meta_description'])}
      date={entry.getIn(['data', 'datetime'])}
      category={entry.getIn(['data', 'category'])}
      tweet_id={entry.getIn(['data', 'tweet_id'])}
      tags={entry.getIn(['data', 'tags'])}
      showComment={entry.getIn(['data', 'showComment'])}
      showToc={entry.getIn(['data', 'showToc'])}
      showTags={entry.getIn(['data', 'showTags'])}
      showHotJar={entry.getIn(['data', 'showHotJar'])}
      showStack={entry.getIn(['data', 'showStack'])}
      showVideo={entry.getIn(['data', 'showVideo'])}
      slug={entry.getIn(['data', 'slug'])}
      content={widgetFor('body')}
    />
  )
}

PostsPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default PostsPreview
