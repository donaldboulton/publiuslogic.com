import React from 'react'
import { Router, Link, createMemorySource, createHistory, LocationProvider } from '@reach/router'
import Helmet from 'react-helmet'
import Layout from '../components/Layout'
import { PageBody, BodyWrapper } from '../components/styles/PageBody'
import PrivateRoute from '../components/App/privateRoute'
import Bookmarks from '../components/App/bookmarks'
import Courses from '../components/App/courses'
import CreateCourse from '../components/App/createCourse'
import UpdateCourse from '../components/App/updateCourse'
import CoursesInReview from '../components/App/coursesInReview'
import CoursesInReviewDetails from '../components/App/coursesInReviewDetails'
import Spinner from '../components/App/spinner'
import {
  getUser,
  isLoggedIn,
  getPrivateRoute,
  isDeveloper,
  isAuthor,
} from "../networking/auth"

import Login from '../components/Login'
import Logout from '../components/App/logout'

const Home = React.lazy(() => import("../components/App/Home"))

const LazyLoadComponent = ({ Component, ...props }) => (
  <React.Suspense fallback={<Spinner />}>
    <Component {...props} />
  </React.Suspense>
)
let source = createMemorySource("/app/login")
let history = createHistory(source)

const App = () => {
  return (
    <>
        <Layout>
          <Helmet title='Administration' description='Administration'/>
          <header
            style={{
             background: `rebeccapurple`,
             marginBottom: `1.45rem`,
            }}
          >
            <div
              style={{
                margin: `0 auto`,
                maxWidth: 960,
                padding: `1.45rem 1.0875rem`,
              }}
            >
            <div style={{ display: `flex`, justifyContent: `space-between` }}>
              <div style={{ margin: 0 }}>
                <Link
                  to="/app/"
                  style={{
                    color: `white`,
                    marginRight: `0.5rem`,
                  }}
                >
                  Home
                </Link>
                <Link
                  to='/course'
                  style={{
                    color: `white`,
                    marginRight: `0.5rem`,
                  }}
                >
                  Course
                </Link> 
                <Link
                  to='/app/home'
                  style={{
                    color: `white`,
                    marginRight: `0.5rem`,
                  }}
                >
                  Api Test
                </Link> 
              </div>
              <div>
                {isLoggedIn() ? (
                  <>
                    <span
                      style={{
                        color: `white`,
                        marginRight: `0.5rem`,
                      }}
                    >{`Hello ${getUser().email}`}</span>
                    <Link
                      to={getPrivateRoute()}
                      style={{
                        color: `white`,
                         marginRight: `0.5rem`,
                      }}
                    >
                    {isDeveloper() ? (
                      <span>Bookmarks</span>
                    ) : isAuthor() ? (
                      <span>Courses</span>
                    ) : (
                      <span>Courses for review</span>
                    )}
                  </Link>
                  <Logout />
                </>
                ) : (
                  <Link
                    to="/app/login"
                    style={{
                      color: `white`,
                      marginRight: `0.5rem`,
                    }}
                  >
                    Log in
                  </Link>
                )}
               </div>
              </div>
            </div>
          </header>
          <PageBody>
            <BodyWrapper>              
              <LocationProvider history={history}>
                <Router>
                  <LazyLoadComponent Component={Home} path='/app/home' />               
                  <PrivateRoute path='/app/bookmarks' component={Bookmarks} />
                  <PrivateRoute path='/app/courses' component={Courses} />
                  <PrivateRoute path='/app/courses/create' component={CreateCourse} />
                  <PrivateRoute path='/app/courses/:id/update' component={UpdateCourse} />
                  <PrivateRoute path='/app/courses/review' component={CoursesInReview} />
                  <PrivateRoute
                    path="/app/courses/:id/review"
                    component={CoursesInReviewDetails}
                  />
                  <Login path='/app/login' />
                </Router>
              </LocationProvider>    
            </BodyWrapper>
          </PageBody>
        </Layout>
    </>
  )
}

export default App
