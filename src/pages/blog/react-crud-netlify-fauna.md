---
templateKey: posts
title: React CRUD Netlify Fauna
path: /react-crud-netlify-fauna
slug: /react-crud-netlify-fauna
date: "2019-11-10"
category: "Tech"
cover: "/images/netlify-fauna.webp"
tags:
  - React
  - Netlify
  - Fauna
meta_title: React CRUD Netlify Fauna
meta_description: React CRUD Netlify FaunaDB
videoUrl: https://youtu.be/7OB_IjTTcwg
videoTitle: Identity FaunaDB
videoPoster: https://youtu.be/7OB_IjTTcwg
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: true
showVideo: false
related: true
showLogRocket: false
published: true
---

## React FaunaDB

🖥️ Based on 🔗 [Nelifty post](https://www.netlify.com/blog/2018/07/09/building-serverless-crud-apps-with-netlify-functions-faunadb/?_ga=2.21698171.1777301599.1576092869-301685750.1573932436#wrapping-up)

And some code functions and the Database schema mixed in from 🔗 [Netlify FaunaDB ToDo](https://github.com/fauna/netlify-faunadb-todomvc) for Netlify Identity Login and to set up the database.

Serverless functions seem to be all the rage these days. But why?

Devs are adopting the FAAS (Functions-as-a-Service) because of:

### Pay per execution

Pay-per-execution pricing: You only pay for the how long your function code runs, not for idle server time.
Scalability: Load balancing, security patches, logging, etc. are all handled by the FAAS provider. That leaves more time for companies to focus on their app instead of the underlying infrastructure.
Most importantly, perhaps, is that server-less functions give frontend developers superpowers.

If you can write JavaScript, you can build out robust backend applications and APIs using simple AWS Lambda functions.

Need to process payments? Functions have your back.

Need to build a backend API? Yep, functions can do that.

Need to send transactional emails/SMS to users? Functions got you.

We’ve previously explored how functions work together with Identity on Netlify, but today we’re going a step further. This post will focus on using functions to build a backend API for a React App.

We will be walking through how you can use FaunaDB with Netlify Functions to build a CRUD (Create, Read, Update, Delete) API.

![FaunaDB](/ststic/img/noplugins.webp "FaunaDB")
_[`FaunaDB` serverless functions](https://www.netlify.com/docs/functions/)_

## Functions ToDo

This Functions Todo Sample is in the 🔗 [Netlify Fauna ToDo](https://github.com/jchris/netlify-fauna-todo), GitHub Repo.

The Fauna post on the 🔗 [FaunaDB ToDo}(https://fauna.com/blog/building-a-serverless-jamstack-app-with-faunadb-cloud-part-1)

`video: https://youtu.be/7OB_IjTTcwg`

> See the component in this markdown page at the bottom, Netlify Functions Example!

### ToDo

The Below ToDo is build with Netlify Functions and requires no site rebuild for data changes.
Try it out its instantaneous and anonymous users can CRUD as Create, Read, edit and Update, Delete and bulk delete. This is not a full todo with lists and user list items, just an example of CRUD.

<interactive-todo></interactive-todo>
