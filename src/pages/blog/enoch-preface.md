﻿---
templateKey: posts
title: Enoch Preface
path: /enoch-preface
slug: /enoch-preface
date: "2018-07-13"
category: "Enoch"
cover: "/images/book-of-enoch.webp"
author: Donald Boulton
tags:
  - Original Bible
  - Enoch
  - God
meta_title: "Preface for Books 1 and 2"
meta_description: "Preface use in Enoch Books 1 and 2"
videoUrl: https://youtu.be/HEj4p8Ax3b4
videoTitle: Magnetic Flux
videoPoster: https://youtu.be/HEj4p8Ax3b4
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: false
showVideo: false
showLogRocket: true
published: true
---

## Preface

The book of Enoch consists of multiple parts that were put together as one book by a earlier editor.

The book consists of the following parts:

Book of Watchers – Chapters 1-36 including fragments of the Book of Noah in chapters 6-11

Book of the Parables – Chapters 37-71 is also known as the Book of Similitudes

Book of the Luminaries – Chapters 72-82

The Dream Visions – Chapters 83-90

Critical Structure – Chapters 91-104

God and Messiah to dwell in Man – Chapter 105

Latin fragment of the book of Noah – Chapters 106-107

### Appendix added l

The _first parable_ (xxxviii.-xliv.) is a prophecy of coming judgement upon the wicked, and especially the Kings and Mighty ones on the Earth.

This is Enoch's books in Greek and Ethiopian, The Egyptian versions of Enoch are his design in stone as Cheops, "The Great Pyramid at Giza"! An interesting occurrence of Gematria also helps pinpoint the Great Pyramid as God's special revelation, as a dream vision to Enoch

## Hebrew of Isaiah 19:19,20

Gematria is the science of finding meaning in the numerical value of words. In the Hebrew language each individual letter has a numerical value. Thus every word has the numerical value of the sum of the value of its letters.

![Hebrew Text](/static/img/hebrew-text.jpg "hebrew text")
_[`hebrew.text` Giza Description](https://publiuslogic-old.netlify.app/img/hebrew-text.jpg)_

![Enoch Circle](/static/img/enochw.png "Enoch Circle")
_[`enoch.circle` Structural Enoch Circle](https://publiuslogic-old.netlify.app/img/chrono-trans-400.png)_

![Giza Description](/static/img/chrono-trans-400.png "Structural Bible")
_[`structural.bible` Giza Description](https://publiuslogic-old.netlify.app/img/chrono-trans-400.png)_

If one adds up the numerical value of all the Hebrew characters in the Great Pyramid text the value is 5449. This is the height in inches of the Great Pyramid!  
One person ("of many"), who measured and studied the Great Pyramid say it is a structural bible.

## EDITORS PREFACE

THE object of this series of translations is primarily to furnish students with short, cheap, and handy text-books, which, it is hoped, will facilitate the study of the particular texts in class under competent teachers.

But it is also hoped that the volumes will be acceptable to the general reader who may be interested in the subjects with which they deal. It has been thought advisable, as a general rule, to restrict the notes and comments to a small compass; more especially as, in most cases, excellent works of a more elaborate character are available.

Indeed, it is much to be desired that these translations may have the effect of inducing readers to study the larger works.

My principal aim, in a word, is to make some difficult texts, important for the study of Christian origins, more generally accessible in faithful and scholarly translations.

In most cases these texts are not available in a cheap and handy form. In one or two cases texts have been included of books which are available in the official Apocrypha; but in every such case reasons exist for putting forth these texts in a new translation, with an Introduction, in this series.

W. O. E. OESTERLEY.

G. H. Box.

D. W. Boulton Jr.
