---
templateKey: posts
title: React Hook Form
path: /react-hook-form
slug: /react-hook-form
date: "2019-04-23"
category: "Tech"
cover: "/images/react-hook-form.webp"
tags:
  - React
  - Hooks
  - Validation
meta_title: React Hook Form Builder
meta_description: I chose this module to validate my forms, its lightweight and integrates with yup schemas
videoUrl: https://www.youtube.com/watch?v=aW-BT5BKbPY
videoTitle: Media Queries
videoPoster: https://www.youtube.com/watch?v=aW-BT5BKbPY
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: true
showVideo: false
showLogRocket: false
published: true
---

## React Hook Forms

↩️ I chose this module to validate my forms, its lightweight and integrates with yup schemas. Plus a nice online form builder and right on point instructions.

You can see this form validation in action on my contact page and on my email form in a modali window, the button for the popup email form is in the email section at the lower part of every PubliusLogic page.

🔗 [简体中文](https://github.com/bluebill1049/react-hook-form/blob/master/docs/README.zh-CN.md")

![](/static/img/reacthookform.webp)

🔗 [React Hooks Form](https://react-hook-form.com/get-started)

> React hook form validation without the hassle

[![npm downloads](https://img.shields.io/npm/dm/react-hook-form.svg?style=flat-square)](https://www.npmjs.com/package/react-hook-form)
[![npm](https://img.shields.io/npm/dt/react-hook-form.svg?style=flat-square)](https://www.npmjs.com/package/react-hook-form)
[![npm](https://badgen.net/bundlephobia/minzip/react-hook-form)](https://badgen.net/bundlephobia/minzip/react-hook-form)

- Super easy to integrate and create forms
- Built with React Hooks with performance and developer experience in mind
- Follows HTML standard for validation
- Tiny size without other any dependency
- Build forms quickly with the 🔗 [form builder](https://react-hook-form.com/form-builder)

## Install

    $ npm install react-hook-form

    $ yarn add react-hook-form

    For validation schemas use yup

    $ npm install yup

    $ yarn add yup

### Yup at [code sandbox](https://codesandbox.io/s/yyyqp7y8x)

## [Docs](https://react-hook-form.com/)

- [Resources](https://react-hook-form.com/resources)
- [Get started](https://react-hook-form.com/)
- [API](https://react-hook-form.com/api)
- [FAQ's](https://react-hook-form.com/faqs)
- [Blog Post Validation Examples](https://jasonwatmore.com/post/2021/04/21/react-hook-form-7-form-validation-example)
- [Advance Usage](https://react-hook-form.com/advanced-usage)
- [Dev Tools](DevTools)
- [Form Builder](https://react-hook-form.com/form-builder)

## Quickstart

```jsx
import React from "react";
import { useForm } from "react-hook-form";

export default function App() {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const onSubmit = data => console.log(data);

  console.log(watch("example")); // watch input value by passing the name of it

  return (
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    <form onSubmit={handleSubmit(onSubmit)}>
      {/* register your input into the hook by invoking the "register" function */}
      <input defaultValue="test" {...register("example")} />
      
      {/* include validation with required or other standard HTML validation rules */}
      <input {...register("exampleRequired", { required: true })} />
      {/* errors will return when field validation fails  */}
      {errors.exampleRequired && <span>This field is required</span>}
      
      <input type="submit" />
    </form>
  );
}
```

### validationSchema

If you would like to centralize your validation rules or external validation schema, you can apply validationSchema when you invoke useForm. we use Yup for object schema validation and the example below demonstrate the usage.

```jsx
import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

const schema = yup.object().shape({
  firstName: yup.string().required(),
  age: yup.number().positive().integer().required(),
});

export default function App() {
  const { register, handleSubmit, formState:{ errors } } = useForm({
    resolver: yupResolver(schema)
  });
  const onSubmit = data => console.log(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input {...register("firstName")} />
      <p>{errors.firstName?.message}</p>
        
      <input {...register("age")} />
      <p>{errors.age?.message}</p>
      
      <input type="submit" />
    </form>
  );
}
```

## Contributors

Thanks goes to these wonderful people:

🔗 [AyumiKai]https://github.com/AyumiKai)
🔗 [garthmcrae](https://github.com/garthmcrae)
🔗 [erikras]https://github.com/erikras)
