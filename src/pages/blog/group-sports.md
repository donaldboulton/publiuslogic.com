---
templateKey: posts
title: Group Sports
path: /group-sports
slug: /group-sports
date: "2020-08-15"
category: "Sports"
cover: "/images/sports.webp"
tags:
  - Sports
  - Coaches
meta_title: Group Sports
meta_description: Group Sports = no coaches
videoUrl: https://www.youtube.com/watch?v=aW-BT5BKbPY
videoTitle: Media Queries
videoPoster: https://www.youtube.com/watch?v=aW-BT5BKbPY
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: true
showVideo: false
showLogRocket: false
published: true
---

## Group Sports and Coaches

I have always been a big Collage Football Fan, Go Sooners, & Cowboys.

I prefer individual sports and recommend them like Motocross and Tennis, the two sports I participated in on a expert basis, but never my profession.

Sports of some kind are needed for the health of our children, two many fat out of shape kids.

One thing I could never understand is why the coach is calling the plays in both Collage and Professional Group Sports.

A collage group sports athlete study's his sport to know, "hopefully", all of the aspects of there sport. This hope is that there studies will be there, "Future Profession". When it comes to test time the coaches help in the exam.

I overheard some OU student's bitching about this, somewhere on campus corner, in the 1980's and did not think much of it. Until OU hade a washed up Coaches who did not care anymore, who cost OU several national championships from play calling stupidity.

The players could have won those games if calling the plays themselves. They would have been better off having duck tape over the coaches mouths and play the game themselves.

Is not that the way it should be; the game is your weekly test?

The last game or the bowl games are your finals.

From what I have seen watching Football and other group sports all of my life is that a few professional players are to unprepared to even fully know there position on the team, let alone all aspects of the game, great athletes but not fully prepared, should never be a pro, because that would mean they do not need coaches, but obviously some do.

## Go Buccaneers

Tom Brady and Rob Gronkowski should call all the plays on offense for the Buc's, using the other players input on what to call next.

> And if so? Watch the perfection!

Go Buccaneers!
