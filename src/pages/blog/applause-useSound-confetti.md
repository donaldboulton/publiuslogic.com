---
templateKey: posts
title: Applause useSound Confetti
path: /applause-useSound-confetti
slug: /applause-useSound-confetti
date: "2020-10-10"
category: "Tech"
cover: "/images/applause.webp"
featured: true
tags:
  - React
  - Applause
  - useSound
  - Confetti
meta_title: Applause useSound Confetti
meta_description: Applause aoi with useSound Hook and React Dom Confetti
videoUrl: https://youtu.be/HEj4p8Ax3b4
videoTitle: Magnetic Flux
videoPoster: https://youtu.be/HEj4p8Ax3b4
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: true
showVideo: false
showLogRocket: true
published: true
---

## Applause Button

Click the 🙌 for claps in the Bio Section above and see an hear the Magic. 🤟🎉

I wanted a Applause button with sound and Confetti of my own design and below is what I came up with.
I will be adding a version for FaunaDB using my own API with Netlify functions💯

### Applause api

🖧 Utilizing the free api from 🔗 [Applause Button](https://applause-button.com/) to CRUD the data.
If you use this API allot, you should donate for its server time or to 🔗 [Open Collective](https://opencollective.com/).

## useSound

### A React Hook for Sound Effects

🚀 Repo 🔗 [useSound by Josh W Comeau](https://github.com/joshwcomeau/use-sound)

The web needs more (tasteful) sounds!

- 👂 Lets your website communicate using 2 human senses instead of 1
- 🔥 Declarative Hooks API
- ⚡️ <1kb bytes (gzip) in your bundle! ~10kb loaded async.
- ✨ Built with Typescript
- 🗣 Uses a powerful, battle-tested audio utility: [**Howler.js**](https://howlerjs.com/)

[![Minified file size](https://img.badgesize.io/https://www.unpkg.com/use-sound/dist/use-sound.esm.js.svg)](https://bundlephobia.com/result?p=use-sound) [![License: MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT) [![NPM version](https://img.shields.io/npm/v/use-sound)](https://www.npmjs.com/package/use-sound) [![Code of Conduct](https://img.shields.io/badge/Code%20of-conduct-d03eaf?style=flat)](https://github.com/joshwcomeau/use-sound/blob/master/CONDUCT.md)

This library only works with React DOM, but @remigallego created an alternative for React Native! Check out [react-native-use-sound](https://github.com/remigallego/react-native-use-sound).

### Confetti Cannon

💣 I am using 🔗 [react-dom-confetti](https://github.com/daniel-lundin/react-dom-confetti) for the Confetti Cannon that is positioned over the Applause button as so the explosion will come from the button count on the API response.

## Install Modules

🤟 You will have to add sound.mp3 and modules. I got my clapping.mp3 from 🔗 [FreeSound.org](https://freesound.org/)

> Your own clapping sound

And then install...

> yarn add use-sound react-dom-confetti

If you are using the below code give a Applause or use any social link or github star or add a Comment in the FaunaDB Comments Form to display your comments on this page.

## Applause Code

```jsx
import React, { useState, useEffect } from "react"
import useSound from "use-sound"
import clapping from "../../../static/audio/clapping.mp3"
import Confetti from "react-dom-confetti"
import axios from "axios"

const confettiConfig = {
  angle: 90,
  spread: 360,
  startVelocity: 40,
  elementCount: 70,
  dragFriction: 0.12,
  duration: 3000,
  stagger: 3,
  width: "10px",
  height: "10px",
  perspective: "500px",
  colors: ["#a864fd", "#29cdff", "#78ff44", "#ff718d", "#fdff6a"],
}

const Wrapper = props => <span style={{ position: "relative" }} {...props} />

const ConfettiWrapper = props => (
  <span style={{ position: "absolute", top: 0, right: 0 }} {...props} />
)
const API = "https://api.applause-button.com"
const VERSION = "3.0.0"
const mainUrl = typeof window !== "undefined" ? window.location.href : ""

const HEADERS = {
  "Content-Type": "text/plain",
}

const getClaps = async url => {
  const query = url ? `?url=${url}` : ""
  // eslint-disable-next-line no-return-await
  return await axios.get(`${API}/get-claps${query}`, {
    headers: HEADERS,
  })
}

const updateClaps = async (url, claps = 1) => {
  console.log(claps)
  const query = url ? `?url=${url}` : ""
  // eslint-disable-next-line no-return-await
  return await axios.post(
    `${API}/update-claps${query}`,
    JSON.stringify(`${claps},${VERSION}`),
    {
      headers: HEADERS,
    }
  )
}

const ApplauseButton = ({ url = mainUrl, props }) => {
  const [count, setCount] = useState(0)
  const [isClapped, setIsClapped] = useState(false)
  const [play] = useSound(clapping)
  const doApplause = () => {
    if (!isClapped) {
      console.log("do clapping")
      const callBackend = async () => {
        const result = await updateClaps(url, 1)
        setCount(result.data)
        setIsClapped(true)
      }
      callBackend()
    }
  }

  useEffect(() => {
    const fetchData = async () => {
      const result = await getClaps(url)
      console.log(result)
      setCount(result.data)
    }
    fetchData()
  }, [])

  return (
    <Wrapper>
      <span
        style={{
          cursor: "pointer",
          padding: "1em",
        }}
        onClick={() => {
          doApplause()
          play()
        }}
      >
        {isClapped ? "🤟🎉" : "🙌"}
        {` ${count}`}
      </span>
      <ConfettiWrapper>
        <Confetti active={isClapped} config={confettiConfig} {...props} />
      </ConfettiWrapper>
    </Wrapper>
  )
}

export default ApplauseButton
```
