﻿---
templateKey: posts
title: Enoch Symbols
path: /enoch-symbols
slug: /enoch-symbols
date: "2018-07-13"
category: "Enoch"
cover: "/images/book-of-enoch.webp"
author: Donald Boulton
tags:
  - Original Bible
  - Enoch
  - God
meta_title: Symbols Books 1 and 2
meta_description: "Symbols use in Enoch Books 1 and 2"
videoUrl: https://youtu.be/HEj4p8Ax3b4
videoTitle: Magnetic Flux
videoPoster: https://youtu.be/HEj4p8Ax3b4
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: false
showStack: false
showVideo: false
showLogRocket: false
published: true
---

## Symbols

> ABBREVIATIONS, BRACKETS, AND SYMBOLS SPECIALLY USED IN THE TRANSLATION OF 1 ENOCH

<interactive-center>![Translation Symbols](/img/enoch-preface-intro.jpg "Enoch Symbols")</interactive-center>

> E denotes the Ethiopic Version

### Gs Usage

Denotes the fragments of the Greek Version preserved In Syncellus: in the case of 8b-9b there are two forms of the text, Gs1 Gs2.

### Gg Usage

denotes the large fragment of the Greek Version discovered at Akhmîm, and deposited in the Gizeh Museum, Cairo.

### The following brackets are used in the translation of 1 Enoch

⌈ ⌉. The use of these brackets means that the words so enclosed are found in Gg but not In E.

## Brackets Used In E

⌈⌈ ⌉⌉. The use of these brackets means that the words so enclosed are found in E but not in Gg or Gs. . The use of these brackets means that the words so enclosed are restored.

[ ] The use of these brackets means that the words so enclosed are interpolations.

### Editor Brackets

( ). The use of these brackets means that the words so enclosed are supplied by the editor. The use of **thick type** denotes that the words so printed are emended.

### Corruption

† † corruption in the text. . . . = some words which have been lost.
