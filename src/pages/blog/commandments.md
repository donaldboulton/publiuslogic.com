---
templateKey: posts
title: Commandments
path: /commandments
slug: /commandments
date: "2021-06-19"
category: "God"
cover: "/images/commandments.webp"
featured: true
tags:
  - God
  - Jesus
  - Holy Spirit
  - Commandments
meta_title: Commandments
meta_description: Commandments from God to All within this universe
videoUrl: https://res.cloudinary.com/mansbooks/video/upload/v1619030354/videos/O-mio-babbino_caro.mp4
videoTitle: Creation
videoPoster: https://res.cloudinary.com/mansbooks/video/upload/v1619030354/videos/O-mio-babbino_caro.mp4
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: false
showVideo: false
showLogRocket: true
published: true
---

<interactive-center>The Law!</interactive-center>

Something the USA has forgotten about!

The Catholic Church doesn’t see the Ten Commandments as arbitrary rules and regulations from God but as commandments for protection.

> - Catholic Nonsense gives you an out on obeying them.

These laws were set way before Moses and were a World standard way of belief in all cultures!

Below is what the consequences will be now for violating My Laws.

> Obey them and eternal happiness can be yours.

> Disobey them and suffer the consequences within the burning flesh!

## The Ten Commandments

### First

> “I am the Lord thy God, thou shalt not have any other gods before Me.”

This commandment forbids idolatry, the worship of false gods and goddesses, and it excludes polytheism, the belief in many gods, insisting instead on monotheism, the belief in one God. This commandment forbids making golden calves, building temples to Isis, and worshipping statues of Caesar, for example.

> - No Crosses or Rosery or Depictions of the Trinity, for, "We Are All"!

> - No Worshiping the Flesh!

### Second

> “Thou shalt not take the name of the Lord thy God in vain.”

The faithful are required to honor the name of God. It makes sense that if you’re to love God with all your heart, soul, mind, and strength, then you’re naturally to respect the name of God with equal passion and vigor.

### Third

> “Remember to keep holy the Sabbath day.”

The Jewish celebration of Sabbath (Shabbat) begins at sundown on Friday evening and lasts until sundown on Saturday. Catholic, Protestant, and Orthodox Christians go to church on Sunday, treating it as the Lord’s Day instead of Saturday to honor the day Christ rose from the dead.

> - Everyday is a Holy Day and of Sabbath, If traditions prevail then the Jewish version is my choice. I do my best work on Sunday!

### Fourth

> “Honor thy father and mother.”

This commandment obliges the faithful to show respect for their parents — as children and adults. Children must obey their parents, and adults must respect and see to the care of their parents, when they become old and infirm.

### Fifth

> “Thou shalt not kill.”

The better translation from the Hebrew would be “Thou shalt not murder” — a subtle distinction but an important one to the Church. Killing an innocent person is considered murder. Killing an unjust aggressor to preserve your own life is still killing, but it isn’t considered murder or immoral.

### Sixth

> “Thou shalt not commit adultery.”

The sixth and ninth commandments honor human sexuality. This commandment forbids the actual, physical act of having immoral sexual activity, specifically adultery, which is sex with someone else’s spouse or a spouse cheating on their partner. This commandment also includes fornication, which is sex between unmarried people, prostitution, pornography, homosexual activity, group sex, rape, incest, pedophilia, bestiality, and necrophilia.

### Seventh

> “Thou shalt not steal.”

The seventh and tenth commandments focus on respecting and honoring the possessions of others. This commandment forbids the act of taking someone elses property. The Catholic Church believes that this commandment also denounces cheating people of their money or property, depriving workers of their just wage, or not giving employers a full day’s work for a full day’s pay. Embezzlement, fraud, tax evasion, and vandalism are all considered extensions of violations of the Seventh Commandment.

### Eighth

> “Thou shalt not bear false witness against thy neighbor.”

The Eighth Commandment condemns lying. Because God is regarded as the author of all truth, the Church believes that humans are obligated to honor the truth. The most obvious way to fulfill this commandment is not to lie — intentionally deceive another by speaking a falsehood.

### Ninth

> “Thou shalt not covet thy neighbor’s wife.”

The Ninth Commandment forbids the intentional desire and longing for immoral sexuality. To sin in the heart, Jesus says, is to lust after a woman or a man in your heart with the desire and will to have immoral sex with them. Just as human life is a gift from God and needs to be respected, defended, and protected, so, too, is human sexuality. Catholicism regards human sexuality as a divine gift, so it’s considered sacred in the proper context — marriage.

### Tenth

> “Thou shalt not covet thy neighbor’s goods.”

The Tenth Commandment forbids the wanting to or taking someone else’s property. Along with the Seventh Commandment, this commandment condemns theft and the feelings of envy, greed, and jealousy in reaction to what other people have.
