---
templateKey: posts
title: Earths Magnetic Flux
path: /earths-magnetic-flux
slug: /earths-magnetic-flux
date: "2020-03-02"
category: "Logic"
cover: "/images/earths-magnetic-flux.webp"
featured: true
tags:
  - Earth
  - Magnetic Flux
  - Oil
meta_title: Earths Magnetic Flux
meta_description: Earths Magnetic Flux and Common Sense on Planetary Issues
videoUrl: https://youtu.be/HEj4p8Ax3b4
videoTitle: Magnetic Flux
videoPoster: https://youtu.be/HEj4p8Ax3b4
tweet_id: "1118651504674725888"
showComment: false
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: false
showStack: false
showVideo: true
showLogRocket: false
published: true
---

## Common Sense on Planetary Issues

🌎 As a child of the 1960s I could not get enough information on Planetary issues.
I had subscriptions to all info on science and anything mechanical, and I found the magnetic flux of our planet to be quite intriguing.

## Earths Magnetic Flux

🌐 Over the years paying attention the the magnetic flux changes it became clear to me that these changes were do to mankind using up the riches of Mother Earth as in oil, iron gold and other minerals that were high in there magnetic attractions and introducing dielectric properties, thus decreasing the magnetic attraction to those areas in Mother earth.

Therefore changing the magnetic flux density points around the earth. Basically the Mother used these Magnetic flux points to balance her self and control her spinning rates.

![earths magnetic movement](/static/img/magnetic-motion.webp "earths magnetic movement")
_[`magnetic-motion` magnetic field](https://www.nature.com/articles/d41586-019-00007-1)_

We pump out the oil, store it above ground in areas away from where it came from and then send it up into our atmosphere.

Distribute the minerals all over the place and the mother cannot balance itself, and we all die.

All from our formalized and complete, stupidity and greed.
