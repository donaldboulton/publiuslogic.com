import React from 'react'
import { kebabCase } from 'lodash'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import { StyledH1 } from '../../components/styles/ArticleStyles'
import { PageBody } from '../../components/styles/PageBody'
import { Link, graphql } from 'gatsby'
import Layout from '../../components/Layout'
import FanfareButton from '../../components/FanfareButton'
import useSound from 'use-sound'
import pong from '../../../static/audio/pong-pop.wav'
import ArrowTrails from '../../components/ArrowTrails/ArrowTrails'
import ReactSpringLogo from '../../components/ReactSpringLogo'
import CircleDemo from '../../components/CircleDemo'
import { OutboundLink } from "gatsby-plugin-google-gtag"

const StyledTestDiv = styled.div`
  margin: 0;
  padding: 0;
  border: none;
  background: transparent;
  maxHeight: 600px;
  maxWidth: 800px;
`

function CategoriesPage({
  data: { allMarkdownRemark: { group }, site: { siteMetadata: { titleAlt } } }
}) {
  const [play] = useSound(pong)
  return (
    <Layout>
      <Helmet title={`Categories | ${titleAlt}`} description={titleAlt}/>
      <PageBody as='div'>
        <StyledH1>
          All Site Categories
        </StyledH1>
        <section
          style={{ margin: '2rem' }}
          className='box'
        >
          <div
            style={{ margin: '4rem' }}
          >
          <ul className='taglist field is-grouped is-grouped-multiline'>
            {group.map(category => (
              <li className='control' key={category.fieldValue}>
                <Link to={`/categories/${kebabCase(category.fieldValue)}/`}>
                  <div className='tags has-addons is-large'>
                    <span aria-label='Category' className='tag is-primary'>{category.fieldValue}</span>
                    <span aria-label='Category Count' className='tag is-dark'>{category.totalCount}</span>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
          </div>
        </section>
        <div>
          <h3 className='h3'>
            This page is boring and short so here are some test Components I am Working On!
          </h3>
        </div>
        <StyledTestDiv>
          <div>
            <h3>This Site has React Spring in the menu and everywhere</h3>
            <h3 className='h3'>React Spring Logo</h3>
            <ReactSpringLogo />
          </div>          
          <div>
            <h3 className='h3'>React Spring, "useTransition Arrow Trails", useSound</h3>
            <span
              onMouseEnter={() => {
                play();
              }} 
            >
            <ArrowTrails />
            </span>
          </div>
          <div>
            <h3>A Email MagicRainbowButton with useSound Hook</h3>
            <OutboundLink href='https://joshwcomeau.com/react/announcing-use-sound-react-hook/' rel="noopener noreferrer" target="_blank" alt="Josh Comeau"><h3>useSound post by Josh Comeau</h3></OutboundLink>
            <FanfareButton />
          </div>
          <div>
            <h3>Josh Comeau Circle Demo</h3>
              <CircleDemo />
          </div>
        </StyledTestDiv>        
      </PageBody>
    </Layout>
  )
}

export default CategoriesPage

export const categoryPageQuery = graphql`
  query CategoryQuery {
    site {
      siteMetadata {
        titleAlt
      }
    }
    allMarkdownRemark(limit: 1000) {
      group(field: frontmatter___category) {
        fieldValue
        totalCount
      }
    }
  }
`
