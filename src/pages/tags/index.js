import React from 'react'
import { kebabCase } from 'lodash'
import Helmet from 'react-helmet'
import { Link, graphql } from 'gatsby'
import Layout from '../../components/Layout'
import { PageBody } from '../../components/styles/PageBody'
import { StyledH1 } from '../../components/styles/ArticleStyles'

function TagsPage({
  data: { allMarkdownRemark: { group }, site: { siteMetadata: { title } } },
}) {
  return (
    <Layout>
      <Helmet title={`Tags | ${title}`} description='Tags page categories by tag per page.' />
      <PageBody>
        <section>
          <StyledH1>
            All Site Tags
          </StyledH1>
        </section>
        <section
          style={{ margin: '2em' }}
          className='box'
        >
          <div
            style={{ margin: '2rem' }}
          >
          <ul className='taglist field is-grouped is-grouped-multiline'>
            {group.map(tag => (
              <li className='control' key={tag.fieldValue}>
                <Link to={`/tags/${kebabCase(tag.fieldValue)}/`}>
                  <div className='tags has-addons is-large'>
                    <span aria-label='Tag' className='tag is-primary'>{tag.fieldValue}</span>
                    <span aria-label='Tag Count' className='tag is-dark'>{tag.totalCount}</span>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
          </div>
        </section>
        <section>
          <h2>Tags vs Categories</h2>
          <h3>In the context of a blog:</h3>
          <h4>Categories</h4>
            <p>I see a lot of the best Front End and Full Stack web designers get this confused, so...</p>
            <p>Categories are predefined and there should not be too many of them.</p>
            <p>Their use is closer to a folder system where a blog post gets put in one or more folders.</p>
            <p>They should be visible and easily accessible because they form the secondary organizing principle for the blog (primary being chronology).</p>
            <p>E.g. a post about the iPhone 5 on a blog that also covers laptops, tablets and other smartphones, could be filed under the categories "smartphone" and "apple".</p>
          <h4>Labels and Tags</h4>
             <p>The idea of "tagging" something is synonymous with "labeling": you put labels on something and all them together describe what it is or what it is about. 
               While re-using tags is encouraged to keep the amount of different-but-similar tags from exploding, tags can usually be created on the fly without restrictions. 
               Because they're not defined they support a more loose and free organization based on association. Tags are presented in or next to a blog post, to link to other posts with the same tag. 
               Tags don't do well as a structural feature, but it's not uncommon to show a tag cloud or list of most popular tags.</p>
             <p>E.g. a post about the iPhone 5 could be tagged with "news", "iPhone", "iPhone5", "smartphone", "iOS" and "apple".</p>
             <p>In this site google sees in my sitemap index a list of sitemaps with categories and tags as well as pages and posts which are organized according to categories that have specific tags that have relations with other pages with the same tags, as Google, "KEYWORDS", no matter what category or grouping or pages.</p>
          <h4>Keywords</h4>
            <p>Keywords generally do the same thing as tags and labels do, but it is not also a verb ("keywording"?).</p> 
            <p>Keywords on the web are also already used for something else than what tags are used for: they sit in the head of a document to describe (primarily to search engines) what the document is about. Like I explained above my tags are page specific and are my keywords.</p>
            <p>Therefore they are not interactive and cannot be used by users to find blog posts about specific subjects.</p>
        </section>
      </PageBody>
    </Layout>
  )
}

export default TagsPage

export const tagPageQuery = graphql`
  query TagsQuery {
    site {
      siteMetadata {
        title
        titleAlt
      }
    }
    allMarkdownRemark(limit: 1000) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`

