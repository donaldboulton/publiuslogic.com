---
templateKey: pages
title: Gallery
slug: /gallery
path: /gallery
id: /gallery
date: "2020-12-01"
cover: "/images/tiger.webp"
meta_title: Gallery | PubliusLogic
meta_description: >-
  This website is built as static HTML with React component-modular builds.
videoUrl: https://res.cloudinary.com/mansbooks/video/upload/v1599148726/videos/Greta_Van_Fleet_-_When_The_Curtain_Falls.mp4
videoTitle: Greta Van Fleet
videoPoster: https://res.cloudinary.com/mansbooks/video/upload/v1599148726/videos/Greta_Van_Fleet_-_When_The_Curtain_Falls.mp4
category: "Gallery"
tags:
  - Cats
  - Cloudinary
  - LightGallery
showToc: false
showDate: false
showCategory: false
showTags: false
showHotJar: false
showStack: true
showVideo: false
showLogRocket: false
published: true
---

## Gallery's of Images

Just the Cat Gallery folder for now 🐱

<interactive-gallery></interactive-gallery>

<interactive-upload-widget></interactive-upload-widget>

### My Favorite Videos

My First Concert, "Jefferson Airplane Memorial Auditorium's Dallas TX. 1970" test Video from Woodstock 1969.

`video: https://www.youtube.com/watch?v=Vl89g2SwMh4`
