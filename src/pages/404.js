import React from 'react'
import { PageBody } from '../components/styles/PageBody'
import { StyledH1 } from '../components/styles/ArticleStyles'
import Helmet from 'react-helmet'
import Layout from '../components/Layout'
import FanfareButton from '../components/FanfareButton'
import Image from './image'

const NotFoundPage = ({
}) => (
  <Layout>
    <Helmet title={`404 Not Found | PubliusLogic`} />
    <section className='post-cover'>
      <Image />
    </section>
    <PageBody as='div'>
      <section>
        <StyledH1>
          404: NOT FOUND
        </StyledH1>
      </section>
      <section
        style={{ marginBottom: '1rem' }}
      >
        <div className='columns'>
          <div className='column'>
            <h2 className='subtitle'>
              You just hit a route that doesn&#39;t exist... the sadness.
            </h2>
              Click send the email icon below to send us a email about the error!
          </div>
        </div>
        <div
          style={{ 
            display: 'center',
            textAlign: 'center',
            marginBottom: '6rem' 
          }}
        >
          <FanfareButton />
        </div>
      </section>
    </PageBody>
  </Layout>
)

export default NotFoundPage
