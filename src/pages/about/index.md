---
templateKey: pages
title: About Us
slug: /about-us
path: /about-us
id: /about-us
date: "2019-11-09"
cover: "/images/about.webp"
meta_title: About Us | Starter Publius
meta_description: >-
  This website is built as static HTML with React component-modular builds.
videoUrl: https://res.cloudinary.com/mansbooks/video/upload/v1599148726/videos/Greta_Van_Fleet_-_When_The_Curtain_Falls.mp4
videoTitle: Greta Van Fleet
videoPoster: https://res.cloudinary.com/mansbooks/video/upload/v1599148726/videos/Greta_Van_Fleet_-_When_The_Curtain_Falls.mp4
category: "Tech"
tags:
  - About
showToc: true
showDate: false
showCategory: true
showTags: true
showHotJar: true
showStack: true
showVideo: false
showLogRocket: true
published: true
---

## Technical Notes

🎁 Code Coverage

[![LICENSE](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://raw.githubusercontent.com/donaldboulton/publiuslogic/master/LICENSE.txt)
[![Tip Me via PayPal](https://img.shields.io/badge/PayPal-tip%20me-green.svg?logo=paypal)](https://www.paypal.me/donaldboulton)
[![Join the community on Spectrum](https://withspectrum.github.io/badge/badge.svg)](https://spectrum.chat/?t=fa5cdbee-00bf-4ca8-be8f-f150a6f643e1)
[![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components)
[![Netlify Status](https://api.netlify.com/api/v1/badges/27d2be12-eb4a-4da2-a471-aea92e199948/deploy-status)](https://app.netlify.com/sites/publiuslogic/deploys)

## Funding Help

[![GoFundMe](/static/img/goFundMe-100X38.png)](https://www.gofundme.com/f/help-with-programing-and-video-costs-on-publius?utm_source=customer&utm_medium=copy_link&utm_campaign=p_cf+share-flow-1)

## Security Policy and Reports

PubliusLogic security is top notch using the latest Content-Security-Policy directives we receive a A+ rating 💯

![Content-Security-Policy Speed Test](/static/img/speed-test.jpg "speed test")
_[`speed test`](https://www.webpagetest.org)_

![Headers test Content-Security-Policy](/static/img/security-report.jpg "security report")
_[`security report`](https://securityheaders.com/?q=https%3A%2F%2Fhttps://publiuslogic-old.netlify.app%2F&followRedirects=on)_

### LightHouse Reports

The above and below Reports would be all 100% if not for tracking with LogRocket and Google Tag Manager and Analytics

![LightHouse Desktop Report](/static/img/lighthouse-desktop.jpg "desktop report")
_[`desktop report`](https://www.webpagetest.org/)_

![LightHouse Mobile Report](/static/img/lighthouse-mobile.jpg "mobile report")
_[`mobile report`](https://www.webpagetest.org/)_

## Styling

No added external Style sheets no more Bluma which as of March 1, 2020 to March 6th I removed and, used the below to style my site. That was fun! my first attempt at my styling Globally.

### Styled Components

💅 with custom React Hooks screen [window.matchMedia](/blog/js-media-queries) mediaQueries's 💍

> Beautiful media queries better than CSS @media for styled-components with ability to specify custom breakpoints.

Gallery and Logo Grids with `custom-screen mediaQueriess 🍱` [Masonry Post](/blog/react-hooks-masonry) 💅.

### Photos Masonry Layout

`react-masonry-css 🍱`

> A tiny (~2kb) [CSS masonry] layout for React, & styled-components 💅.

## JavaScript Standard

> [![js-standard-style](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

## Just Having Fun

🍸 This website is built as static HTML with React component-modular Docker Container builds, using React components with GraphQL, Built with WebPack. Including a user data, todo's backend. The data structure is using FaunaDB.

![Site Structure](/static/img/site-driven.png "site driven structure")
_[`site.structure` Site design Structure](https://publiuslogic-old.netlify.app/img/site-driven.png)_

Served on Netlify via a continuous deployment (CD) workflow. Pull requests are automatically built into preview apps, while commits to the master branch trigger the production build and deploy onto Netlify CDN edge node infrastructure. Since the whole site is just a bunch of static files copied onto multiple CDN nodes around the world, time to first byte (TTFB) is consistently fast at around 1ms to 2ms. Instant Notifications through my Slack Bots or Gmail and phone notifications using Netlify Functions for my Mansbooks.com https://publiuslogic-old.netlify.app workspace.

## Starter Publius

🚀 This 🔗 [Repo v5](https://bitbucket.org/donaldboulton/https://publiuslogic-old.netlify.app/src/master/) powers the React site hosted at https://publiuslogic-old.netlify.app. I use it to write about my personal interests, ranging from theoretical physics and spiritual learning to A Human Basic Instructions Abstract, web development and spending time outdoors... oh and not to forget breaking conventions like a React Site with Json-Ld per template for SEO, Internet Positioning, and a A+ security rating. 💯

The site is built with React, styling with styled-components. It's fully responsive, relies heavily on React Hooks for stateful components such as Image Gallery's, Modal's and toTop. Prism for syntax highlighting, Disqus as blog post comments and utilizing Algolia for custom search.

Feel free to reuse any part of this repo to create your own React site.

A 🔗 [React v2](https://www.gatsbyjs.org/) and 🔗 [Netlify CMS](https://www.netlifycms.org) powered generic business website starter based on [gatsby-starter-netlify-cms](https://github.com/AustinGreen/gatsby-starter-netlify-cms)

### Server-less

🔥 No run time dependency or vulnerable server stack required Pre-built pages served over a CDN for fastest time to first byte Fast and cheap CDN scaling results in ultra-high availability worldwide Server-side processes abstracted into microservice APIs for reduced attack surface areas Modern Continuous Deployment (CD) Git workflows with instant rollbacks Headless CMS for complete separation from your app/site and with full version control Modern authentication methods such as OAuth 2 for ultimate security.

## Authentication

### FaunaDB

I am using [FaunaDB](https://fauna.com) with Netlify Identity for Auth for my CMS, Users Profile Backend and for ToDos's, Notes and Email Subscriptions which offers secure membership with roles for user authentication and attribute-based access control (ABAC) out of the box.

What makes FaunaDB’s ABAC even more powerful is that changes to access rights are reflected immediately because ABAC is evaluated for every query. This means that access can be granted or revoked without requiring a user to re-login.

### Identity Widget

🔐 A Custom Styled and Build for Publius, a component used to authenticate with Netlify's Identity service.
🆔 🔗 [Live demo](https://identity.netlify.com)

For a lower level library to the underlying 🔗 [GoTrue](https://github.com/netlify/gotrue) API, see
🔗 [gotrue-js](https://github.com/netlify/gotrue-js)

> Utilizing Netlify Identity in React to make public and private pages easily.

#### What is Identity

⁉️ Netlify’s Identity service is a plug-and-play microservice for handling site
functionalities like signup's, logins, password recovery, user metadata, and
roles. You can use it from single page apps instead of rolling your own, and
integrate with any service that understands JSON Web Tokens (JWTs).

Learn more about this service from this
🔗 [blog post](https://www.netlify.com/blog/2017/09/07/introducing-built-in-identity-service-to-streamline-user-management/)

It follows the 🔗 [JAMstack architecture](https://jamstack.org) by using Git as a single source of truth, and 🔗 [Netlify](https://www.netlify.com) for continuous deployment, and CDN distribution.

## Netlify CMS

🔗 [Static + content management = ❤️](https://www.netlifycms.org/)

🖥️ Netlify CMS is Always the latest Netlify App build.

My custom light, dark build of the Netlify Identity Widget is used on the React frontend and in my git-gateway back-end.

Get the speed, security, and scalability of a static site, while still providing a convenient editing interface for content.

An integrated part of your Git workflow
Content is stored in your Git repository along side your code for easier versioning, multi-channel publishing, and the option to handle content updates directly in Git.

### Extensible CMS

❤️ Netlify CMS is built as a single-page React app. Create custom-styled previews, UI widgets, and editor plugins or add backend's to support different Git platform APIs.
My Netlify CMS backend will build and add Pages, Posts, My layout components, Authors, Products, Site Updates and charts data using charts.js displaying build, nav and analytics charts data on individual pages and posts. All edited from a CMS Backend on a Static Website!

## Features

PubliusLogic is a complex React Site and not for React programing beginners.

> ✔️ Complete Website Suite - Pages = Home, Blog, About, Privacy, Sitemap Index for 4 sitemaps, and a Contact form with Slack Notify and Email notify.

- 🚋Serverless Functions
- 🔏Authentication (with Netlify Identity)
- 🔐Authenticated Serverless Functions (FaunaDB)
- 😻External Provider login with GitHub, Bitbucket, Google, etc.
- 🏠Protected Routes
- 👋🏼Dynamic Clientside Pages in React (enabling all the above)
- 🕵🏼‍♂️Hide API Secrets from being exposed to Frontend

* Algolia Instant Search
* Netlify CMS for Content Management
* FaunaDB Users Backend and Admin
* SEO Friendly (Sitemap Index - 4 sitemaps, Schemas, Meta Tags, GTM etc.)
* Styled Components, Styled useMediaQueries hook
* Progressive Web App & Offline Support
* Tag Lists, Categories Listings and a RSS Feed for Blog
* Time to Read and a Table Of Contents
* Comments with Disqus
* Support, Follow, Mention, Star and Fork and Issues GitHub buttons
* Share Support (Add if wanted Component Included)
* Pagination
* Contact Form (Lambda Netlify Forms) with Slack and and Email
* Easy Configuration using `config.js` file
* Incremental Netlify Builds

## Context Wrapper

PubliusLogic has a `GlobalContextProvider` wrapper for its light and dark Theme, Users{CTX} and FaunaDB{CTX}

## useDarkMode

A custom [React Hook](https://reactjs.org/docs/hooks-overview.html) to help you implement a "dark mode" component for your application.
The user setting persists to `localStorage`.

❤️ it? ⭐️ it on [GitHub](https://github.com/donavon/use-dark-mode/stargazers)
or [Tweet](https://twitter.com/intent/tweet?text=Check%20out%20the%20useDarkMode%20custom%20React%20Hook%20that%20simplifies%20adding%20a%20persistent%20dark%20mode%20setting%20to%20your%20app.&url=https%3A%2F%2Fgithub.com%2Fdonavon%2Fuse-dark-mode&via=donavon&hashtags=reactjs,hooks,darkmode)
about it.

PubliusLogic is using [use-dark-mode](https://github.com/donavon/use-dark-mode) and its suggested install instructions with [gatsby-plugin-dark-mode | GatsbyJS](https://www.gatsbyjs.org/packages/gatsby-plugin-dark-mode/), plugin for React integration injecting noflash.js.

> ☀️🌜 This Site is an example of the above's usage.
