import { useApolloClient } from '@apollo/client'
import { useCookies } from 'react-cookie'

const token = process.env.DEVELOPER_FAUNADB_SERVER_SECRET

// custom hook to handle authToken - we use composition to decouple the auth system and it's storage
export const useAuthToken = () => {
  const [cookies, setCookie, removeCookie] = useCookies([token])
  const setAuthToken = (authToken) => setCookie(token, authToken)
  const removeAuthToken = () => removeCookie(token);
  return [cookies[token], setAuthToken, removeAuthToken]
}

export const useLogout = () => {
  const [, , removeAuthToken] = useAuthToken()
  const apolloClient = useApolloClient()

  const logout = async () => {
    await apolloClient.clearStore() // we remove all information in the store
    removeAuthToken()
  };
  return logout
};
