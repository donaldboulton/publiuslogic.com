import { gql, useQuery } from "@apollo/client";

const userQueryGQL = gql`
  query loginUser {
    user_by_email {
      _id
      name
      email
    }
  }
`;

export const useUserQuery = () => useQuery(userQueryGQL);
