"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactSpring = require("react-spring");

var _usePrefersReducedMotion = _interopRequireDefault(require("./usePrefersReducedMotion"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function useBoop(_ref) {
  var _ref$x = _ref.x,
      x = _ref$x === void 0 ? 0 : _ref$x,
      _ref$y = _ref.y,
      y = _ref$y === void 0 ? 0 : _ref$y,
      _ref$rotation = _ref.rotation,
      rotation = _ref$rotation === void 0 ? 0 : _ref$rotation,
      _ref$scale = _ref.scale,
      scale = _ref$scale === void 0 ? 1 : _ref$scale,
      _ref$timing = _ref.timing,
      timing = _ref$timing === void 0 ? 150 : _ref$timing,
      _ref$springConfig = _ref.springConfig,
      springConfig = _ref$springConfig === void 0 ? {
    tension: 300,
    friction: 10
  } : _ref$springConfig;
  var prefersReducedMotion = (0, _usePrefersReducedMotion["default"])();

  var _React$useState = _react["default"].useState(false),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      isBooped = _React$useState2[0],
      setIsBooped = _React$useState2[1];

  var style = (0, _reactSpring.useSpring)({
    transform: isBooped ? "translate(".concat(x, "px, ").concat(y, "px)\n         rotate(").concat(rotation, "deg)\n         scale(").concat(scale, ")") : "translate(0px, 0px)\n         rotate(0deg)\n         scale(1)",
    config: springConfig
  });

  _react["default"].useEffect(function () {
    if (!isBooped) {
      return;
    }

    var timeoutId = window.setTimeout(function () {
      setIsBooped(false);
    }, timing);
    return function () {
      window.clearTimeout(timeoutId);
    };
  }, [isBooped]);

  var trigger = _react["default"].useCallback(function () {
    setIsBooped(true);
  }, []);

  var appliedStyle = prefersReducedMotion ? {} : style;
  return [appliedStyle, trigger];
}

var _default = useBoop;
exports["default"] = _default;