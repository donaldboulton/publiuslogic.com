"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = require("react");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useViewportWidth = function useViewportWidth() {
  var windowGlobal = typeof window !== 'undefined' && window;

  var getViewportWidth = function getViewportWidth() {
    var e = windowGlobal,
        a = "inner";

    if (!("innerWidth" in windowGlobal)) {
      a = "client";
      e = document.documentElement || document.body;
    }

    return e[a + "Width"];
  };

  var _useState = (0, _react.useState)(getViewportWidth()),
      _useState2 = _slicedToArray(_useState, 2),
      viewportWidth = _useState2[0],
      setViewportWidth = _useState2[1];

  (0, _react.useEffect)(function () {
    var setFromEvent = function setFromEvent() {
      return setViewportWidth(getViewportWidth());
    };

    window.addEventListener("resize", setFromEvent);
    return function () {
      window.removeEventListener("resize", setFromEvent);
    };
  }, []);
  return viewportWidth;
};

var _default = useViewportWidth;
exports["default"] = _default;