"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _utils = require("../utils");

var _useIncrementingNumber = _interopRequireDefault(require("./useIncrementingNumber"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var rainbowColors = ['hsl(1deg, 96%, 55%)', // red
'hsl(25deg, 100%, 50%)', // orange
'hsl(40deg, 100%, 50%)', // yellow
'hsl(45deg, 100%, 50%)', // yellow
'hsl(66deg, 100%, 45%)', // lime
'hsl(130deg, 100%, 40%)', // green
'hsl(177deg, 100%, 35%)', // aqua
'hsl(230deg, 100%, 45%)', // blue
'hsl(240deg, 100%, 45%)', // indigo
'hsl(260deg, 100%, 55%)', // violet
'hsl(325deg, 100%, 48%)' // pink
];
var paletteSize = rainbowColors.length;
var WINDOW_SIZE = 3; // During compile-time build, we have to assume no browser support.
// On mount, we'll check if `CSS.registerProperty` exists

var hasBrowserSupport = typeof window !== 'undefined' ? typeof window.CSS.registerProperty === 'function' : false;

var getColorPropName = function getColorPropName(id, index) {
  return "--magic-rainbow-color-".concat(id, "-").concat(index);
};

var useRainbow = function useRainbow(_ref) {
  var _ref$intervalDelay = _ref.intervalDelay,
      intervalDelay = _ref$intervalDelay === void 0 ? 2000 : _ref$intervalDelay;
  var prefersReducedMotion = typeof window === 'undefined' ? true : window.matchMedia('(prefers-reduced-motion: no-preference)');
  var isEnabled = hasBrowserSupport && prefersReducedMotion.matches;

  var _React$useRef = _react["default"].useRef((0, _utils.generateId)()),
      uniqueId = _React$useRef.current; // Register all custom properties


  _react["default"].useEffect(function () {
    if (!isEnabled) {
      return;
    }

    (0, _utils.range)(0, WINDOW_SIZE).map(function (index) {
      var name = getColorPropName(uniqueId, index);
      var initialValue = rainbowColors[index];
      CSS.registerProperty({
        name: name,
        initialValue: initialValue,
        syntax: '<color>',
        inherits: false
      });
    });
  }, [WINDOW_SIZE, isEnabled]);

  var intervalCount = (0, _useIncrementingNumber["default"])(intervalDelay);
  return (0, _utils.range)(0, WINDOW_SIZE).reduce(function (acc, index) {
    var effectiveIntervalCount = isEnabled ? intervalCount : 0;
    var name = getColorPropName(uniqueId, index);
    var value = rainbowColors[(effectiveIntervalCount + index) % paletteSize];
    return _objectSpread({}, acc, _defineProperty({}, name, value));
  }, {});
};

var _default = useRainbow;
exports["default"] = _default;