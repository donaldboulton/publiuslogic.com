"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Grid = exports.GridContainer = exports.Flex = exports.Container = exports.Div = exports.Heading = exports.Typography = exports.Box = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactSpring = require("react-spring");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\ndisplay: grid;\ngrid-template-columns: repeat(\n  auto-fit,\n  minmax(", ", 1fr)\n);\ngrid-gap: ", ";\ntext-align: ", ";\nmax-width: ", ";\ngrid-auto-rows: ", ";\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  margin: 0 auto;\n  width: 100%;\n  min-width: 288;\n  max-width: 1440;\n  py: 0;\n  px: ;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n font-size: 22px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n font-size: 18px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n  > * {\n    /* for vertical centering */\n    display: flex;\n    grid-area: 1/1;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Box = _styledComponents["default"].div(_templateObject());

exports.Box = Box;

var Typography = _styledComponents["default"].div(_templateObject2());

exports.Typography = Typography;

var Heading = _styledComponents["default"].h1(_templateObject3());

exports.Heading = Heading;
var Div = (0, _styledComponents["default"])(_reactSpring.animated.div)(_templateObject4());
exports.Div = Div;
var Container = (0, _styledComponents["default"])(Box)(_templateObject5());
exports.Container = Container;
var Flex = (0, _styledComponents["default"])(Box)(_templateObject6());
exports.Flex = Flex;
var GridContainer = (0, _styledComponents["default"])(Grid)(_templateObject7(), function (props) {
  return props.minWidth || "5em";
}, function (props) {
  return props.gap || "calc(1em + 1vw)";
}, function (props) {
  return props.align;
}, function (props) {
  return props.children.length === 1 && props.maxWidth;
}, function (props) {
  return props.height;
});
exports.GridContainer = GridContainer;
var Grid = (0, _styledComponents["default"])(Box)(_templateObject8());
exports.Grid = Grid;