import React from 'react'
import { animated } from 'react-spring'
import styled from 'styled-components'

export const Box = styled.div`
  display: grid;
  > * {
    /* for vertical centering */
    display: flex;
    grid-area: 1/1;
  }
`
export const Typography = styled.div`
 font-size: 18px;
`
export const Heading = styled.h1`
 font-size: 22px;
`
export const Div = styled(animated.div)``

export const Container = styled(Box)`
  margin: 0 auto;
  width: 100%;
  min-width: 288;
  max-width: 1440;
  py: 0;
  px: ;
`
export const Flex = styled(Box)`
  display: flex;
`
export const GridContainer = styled(Grid)`
display: grid;
grid-template-columns: repeat(
  auto-fit,
  minmax(${props => props.minWidth || `5em`}, 1fr)
);
grid-gap: ${props => props.gap || `calc(1em + 1vw)`};
text-align: ${props => props.align};
max-width: ${props => props.children.length === 1 && props.maxWidth};
grid-auto-rows: ${props => props.height};
`

export const Grid = styled(Box)`
`;

