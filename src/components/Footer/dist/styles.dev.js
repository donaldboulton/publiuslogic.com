"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PoweredBy = exports.FooterContainer = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _mediaQueries = _interopRequireDefault(require("./../../utils/mediaQueries"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  grid-area: poweredBy;\n  display: grid;\n  grid-auto-flow: column;\n  a {\n    height: 1.5em;\n    width: 1.5em;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  padding: 5vh 5vw;\n  color: ", ";\n  display: grid;\n  justify-items: center;\n  align-items: center;\n  grid-template-areas:\n    'copyright'\n    'source'\n    'poweredBy';\n  grid-gap: 4vh 6vw;\n  ", " {\n    grid-template-areas:\n      'copyright source'\n      'poweredBy poweredBy';\n  }\n  ", " {\n    grid-template-areas: 'copyright source poweredBy';\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FooterContainer = _styledComponents["default"].div(_templateObject(), function (props) {
  return props.theme.white;
}, _mediaQueries["default"].minPhone, _mediaQueries["default"].minTablet);

exports.FooterContainer = FooterContainer;

var PoweredBy = _styledComponents["default"].div(_templateObject2());

exports.PoweredBy = PoweredBy;