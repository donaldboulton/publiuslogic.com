import React from 'react'
import { OutboundLink } from "gatsby-plugin-google-gtag"
import ouSoftball from '../../../static/img/ou-softball-164.webp'

const CssTricksAds = () => {
  return (
    <>
      <div className='card card1'>
        <div className='message-body'>
          <div>
            <OutboundLink title='Sooner Girls' href='https://soonersports.com/splash.aspx?id=splash_3&path=softball' target='_blank' rel='noopener noreferrer'>
              <img
                className='icon-logo-star'
                src={ouSoftball}
                alt='Sooner Girls'
              />
            </OutboundLink>
          </div>
          <div>
            <OutboundLink title='Go Sooners SoftBall!' href='https://soonersports.com/splash.aspx?id=splash_3&path=softball' target='_blank' rel='noopener noreferrer'>
              <strong>2021 National Champs!</strong>
            </OutboundLink>
          </div>
          <p><h3 className='a'>National Champions For the Fifth Time, Go Sooners!</h3></p>
        </div>
      </div>
    </>
  )
}

export default CssTricksAds

