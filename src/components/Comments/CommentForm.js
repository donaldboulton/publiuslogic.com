import React from 'react'
import { NetlifyForm, Honeypot, Recaptcha } from 'react-netlify-forms'
import styled from 'styled-components'

const FormInput = styled.input`
  font-family: Kaushan Script,cursive;
  font-size: 1.1em;
  line-height: 1.5em;
  text-size-adjust: 100%;
  width: 45%;
`

const FormTextarea = styled.textarea`
  font-family: Kaushan Script,cursive;
  font-size: 1.1em;
  line-height: 1.5em;
  text-size-adjust: 100%;
  width: 95%;
`

function CommentForm() {
  return (
    <NetlifyForm
      name='comments'
      honeypotName='bot-field'
      enableRecaptcha
      onSuccess={(response, context) => {
        console.log('Successfully sent form data to Netlify Server')
        context.formRef.current.reset()
      }}
    >
      {({ handleChange, success, error }) => (
        <>
          <div 
            className='box'
          >
            <Honeypot />
            
            {success && (
              <p 
                style={{ 
                  color: '#00FF00', 
                  marginLeft: '2em'
                }}
              >
                Thanks for contacting us!
              </p>
            )}
            {error && (
              <p 
                style={{ 
                  color: '#0000FF',
                  marginLeft: '2em'
                }}
              >
                Sorry, we could not reach servers.
              </p>
            )}
            <div
              style={{
                display: `inline`,
                marginTop: '2em',
                verticalAlign: `middle`,
                justifyItems: `center`,
                justifyContent: `center`,
                alignItems: `center`,
              }}
            >
                <FormInput
                  type='text'
                  name='name'
                  id='name'
                  onChange={handleChange}
                  placeholder='Your Name'
                  className='input'
                  required
                  style={{ 
                    marginTop: '2em',
                  }}
                />                
                  <FormInput
                    type='email'
                    name='email'
                    id='email'
                    onChange={handleChange}
                    className='input'
                    placeholder='Email Address'
                    required
                    style={{ 
                      marginTop: '2em',
                    }}              
                  />
            </div>
            <div 
              style={{ 
                marginTop: '2em',
                marginRight: '2em',
                marginLeft: '1.6em',
                justifyItems: `center`,
                justifyContent: `center`,
                alignItems: `center`,
                width: '98%'
              }}
            >
              <FormTextarea
                type='text'
                name='message'
                id='message'
                placeholder='Message'
                onChange={handleChange}
                className='textarea'
                style={{
                  height: '200px',
                  padding: '0.9em',
                  marginBottom: '1.5em'
                }}
                required
              />
            </div>
            <div 
              style={{ 
                marginBottom: '1em'  
              }}
            >
              <Recaptcha siteKey={ process.env.GATSBY_SITE_RECAPTCHA_KEY } invisible />
              <div 
                style={{ 
                  marginRight: '1em',
                  marginLeft: '2em'  
                }}
              >
                <button type='submit' className='button'>
                  Submit
                </button>
                <button type='reset' className='button'>
                  Reset
                </button>
              </div>
            </div>
          </div>          
        </>
      )}
    </NetlifyForm>
  )
}

export default CommentForm