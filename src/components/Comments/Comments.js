import React from 'react'
import { graphql, StaticQuery } from 'gatsby'
import { MessageDetail } from '@styled-icons/boxicons-solid/';
import styled from 'styled-components'

const CommentsTitle = styled.h3`
  margin: 1em;
  display: grid;
  grid-auto-flow: column;
  align-items: center;
  grid-template-columns: auto auto 1fr;
  color: ${props => props.theme.white};
  border-bottom: thin ${props => props.theme.lightGray};
`
const Message = styled(MessageDetail)`
  width: 1.4em;
  height: 1.4em;
  margin-right: 1em;
`
const CommentSpan = styled.span`
  font-family: Kaushan Script,cursive;
  font-size: 0.9em;
  color: ${props => props.theme.white};
  border-bottom: thin #d3d3d3;
`
const CommentDiv = styled.div`
  font-family: Kaushan Script,cursive;
  font-size: 0.9em;
  color: ${props => props.theme.white};
  border-bottom: thin #d3d3d3;
`

const Comments = ({ data }) => (
  <>
    <section>
      <h2 className='h2'>Visitor Comments</h2>
      <div>
        {data.allCommentsJson.edges.map(edge =>
          <div>
              <div>
                <CommentsTitle>
                  <Message role='img' />
                  Title: &nbsp;<CommentSpan>{edge.node.name}</CommentSpan>
                </CommentsTitle>
              </div>
              <div>
                <CommentsTitle>
                  Date: &nbsp;<CommentSpan>{edge.node.date}</CommentSpan>
                </CommentsTitle>
              </div>
              <div>
                <CommentsTitle>
                  Message: &nbsp;<CommentDiv>{edge.node.message}</CommentDiv> 
                </CommentsTitle>
              </div>
              <div hidden>
                <div><h3>Email</h3></div>
                <div>{edge.node.email}</div>
              </div>  
          </div>
        )}
      </div>
    </section>
  </>
)

export default props => (
  <StaticQuery
    query={graphql`
      query Comments {
        allCommentsJson(
          limit: 1000
          sort: { fields: date, order: DESC }
        ) {
          edges {
            node {
              date
              name
              email
              message
              id
              slug
            }
          }
        }
      }
    `}
    render={data => <Comments data={data} {...props} />}
  />
)

