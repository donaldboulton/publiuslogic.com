import React from 'react'
import CommentForm from './CommentForm'
import Comments from './Comments'
import styled from 'styled-components'

const Comment = ({ html }) => {
  return (
    <>
      <section>
        <div>
          <h2>Comments Are Moderated</h2>
        </div>
        <CommentForm />
      </section>
      <section>
        <CommentWrapper>
          <Comments /> 
          <CommentCopy dangerouslySetInnerHTML={{ __html: html }} />                 
        </CommentWrapper>
      </section>
    </>
  )
}

export default Comment

const CommentWrapper = styled.div`
  display: flex;
  align-items: left;
  margin-top: 1em;
  padding-bottom: 1em;

  & > * {
    flex: 1;
  }

  @media screen and (max-width: 1000px) {
    & {
      flex-direction: column;
      justify-content: flex-start;
    }

    & > * {
      margin-top: 2rem;
      flex: 0;
      width: 100%;
    }
  }
`;

const CommentCopy = styled.div`
  max-width: 80vw;

  & p {
    font-size: 16px;
  }
`;