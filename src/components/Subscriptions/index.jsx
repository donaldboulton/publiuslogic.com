import React from 'react'
import { NetlifyForm, Honeypot } from 'react-netlify-forms'
import styled from 'styled-components'
import MagicRainbowButton from '../MagicRainbowButton'
import { OutboundLink } from "gatsby-plugin-google-gtag"

const EmailButton = styled(MagicRainbowButton)`
  padding: calc(1.2em - 1px) 1em;
  border-radius: 0px 3px 3px 0px;
`
const EmailInput = styled.input`
  padding: calc(1em - 1px) 1em;
`
const NeonDiv = styled.div`
  font-size: 16px;
  font-family: 'Sacramento', cursive;
  display: flex;
  justify-content: center;
  align-items: center; 
`
const H1Neon = styled.h2`
  font-size: 3.2rem;
  font-family: 'Sacramento', cursive;
  animation: pulsate 1.5s infinite alternate;  
  border: 0.2rem solid rgb(234 229 229);
  border-radius: 2rem;
  padding: 0.4em;
  box-shadow: 0 0 .2rem #fff,
              0 0 .2rem #fff,
              0 0 2rem #da1b60,
              0 0 0.8rem #da1b60,
              0 0 2.8rem #da1b60,
              inset 0 0 1.3rem #da1b60;
  }

  @keyframes pulsate {
    
    100% {
  
        text-shadow:
        0 0 4px #fff,
        0 0 11px #fff,
        0 0 19px #fff,
        0 0 40px #da1b60,
        0 0 80px #da1b60,
        0 0 90px #da1b60,
        0 0 100px #da1b60,
        0 0 150px #da1b60;
    
    }
    
    0% {
  
      text-shadow:
      0 0 2px #fff,
      0 0 4px #fff,
      0 0 6px #fff,
      0 0 10px #da1b60,
      0 0 45px #da1b60,
      0 0 55px #da1b60,
      0 0 70px #da1b60,
      0 0 80px #da1b60;
  
  }
`

function Subscriptions() {
  return (
    <>
      <OutboundLink title='MailChimp Subscriptions' href='https://publiuslogic-old.netlify.app/privacy#Subscriptions' target='_blank' rel='noopener noreferrer'>
        <NeonDiv>
          <H1Neon className='neonText'>
            Subscriptions
          </H1Neon>
        </NeonDiv>
      </OutboundLink>
        <NeonDiv>
          <NetlifyForm
            name='subscriptions'
            honeypotName='bot-field'
            onSuccess={(response, context) => {
              console.log('Successfully sent form data to Netlify Server')
              context.formRef.current.reset()
            }}
          >
            {({ handleChange, success, error }) => (
            <>
            <div>
              <Honeypot />
            
              {success && (
                <p 
                  style={{ 
                    color: '#00FF00', 
                    marginLeft: '2em'
                  }}
                >
                  Thanks for contacting us!
               </p>
              )}
              {error && (
              <p 
                style={{ 
                  color: '#0000FF',
                  marginLeft: '2em'
                }}
              >
                Sorry, we could not reach servers.
              </p>
              )}
              <input type="hidden" name="form-name" value="subscriptions" />
              <div
                style={{
                  display: `inline`,
                  marginTop: '2em',
                  verticalAlign: `middle`,
                  justifyItems: `center`,
                  justifyContent: `center`,
                  alignItems: `center`,
                }}
              >
                <div
                  className='field has-addons'
                >
                  <span className='control'>
                    <EmailInput
                      className='input'
                      type='text'
                      id='email'
                      onChange={handleChange}
                      aria-label='Your Email'
                      aria-required='true'
                      placeholder='Email *'
                      required
                    />
                  </span>
                  <span className='control'>
                    <EmailButton
                      type='submit'
                      aria-label='Submit Subscription'               
                    >
                      SignUp
                    </EmailButton>
                  </span>                          
                  </div>                          
                </div>
              </div>
            </>
          )}
        </NetlifyForm>
      </NeonDiv>
    </>
  )
}

export default Subscriptions