"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GlobalStyle = void 0;

var _styledComponents = require("styled-components");

var _mediaQueries = _interopRequireWildcard(require("../../utils/mediaQueries"));

var _typography = _interopRequireDefault(require("../../utils/typography"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  html {\n    margin: 0;\n    padding: 0;\n    overflow-x: hidden;\n    width: 100%;\n    -webkit-box-sizing: border-box;\n    -moz-box-sizing: border-box;\n    box-sizing: border-box;\n  }\n  *, *:before, *:after {\n    -webkit-box-sizing: inherit;\n    -moz-box-sizing: inherit;\n    box-sizing: inherit;\n  }\n  body {\n    margin: 0;\n    z-index: 0;\n    hyphens: auto;\n    overflow-x: hidden;\n    font-family: ", ";\n    font-size: ", "em;\n    line-height: ", "em;\n    /* Fix very large font size in code blocks in iOS Safari (https://stackoverflow.com/a/3428477). */\n    -webkit-text-size-adjust: 100%;\n    ", " {\n      font-size: calc(", "em + (", " - ", ") * ((100vw - ", "em) / (", " - ", ")));\n      line-height: calc(", "em + (", " - ", ") * ((100vw - ", "em) / (", " - ", ")));\n    }\n    ", " {\n      font-size: ", "em;\n      line-height: ", "em;\n    }\n    /* Ensure full height page even if unsufficient content. */\n    div[role=\"group\"][tabindex] {\n      min-height: 100vh;\n      display: flex;\n      flex-direction: column;\n      main {\n        flex: 1;\n      }\n    }\n  }\n  h1, h2, h3, h4, h5, h6 {\n    line-height: initial;\n  }\n  header,\n  footer {\n    grid-column: 1 / span 2;\n  }\n  .button {\n    border-radius: 4px;\n    padding: calc(.5em - 1px) 1em;\n  }\n  .input {\n    padding: calc(.5em - 1px) 1em;\n  }\n  .content figure {\n    margin-left: 2em;\n    margin-right: 2em;\n    text-align: center;\n  }\n  .field.has-addons {\n    display: flex;\n    justify-content: flex-start;\n  }\n  .tags:last-child {\n    margin-bottom: -.5rem;\n  }\n  .tags {\n    align-items: center;\n    display: flex;\n    flex-wrap: wrap;\n    justify-content: flex-start;\n  }\n  .tags.has-addons .tag:not(:last-child) {\n    border-bottom-right-radius: 0;\n    border-top-right-radius: 0;\n  }\n  .tags.has-addons .tag {\n    margin-right: -1px !important;\n  }\n  .tags.has-addons .tag:not(:first-child) {\n    margin-left: 0;\n    border-bottom-left-radius: 0;\n    border-top-left-radius: 0;\n  }\n  .tags .tag:not(:last-child) {\n    margin-right: .5rem;\n  }\n  .tag:not(body) {\n    align-items: center;\n    border-radius: 4px;\n    display: inline-flex;\n    font-size: .75rem;\n    height: 2em;\n    justify-content: center;\n    line-height: 1.5;\n    padding-left: .75em;\n    padding-right: .75em;\n    white-space: nowrap;\n  }\n  .tag:not(body).is-dark {\n    background-color: #363636;\n    color: #fff;\n  }\n  .field.is-grouped {\n    display: flex;\n    justify-content: flex-start;\n  }\n  .field.is-grouped.is-grouped-multiline>.control:last-child, .field.is-grouped.is-grouped-multiline>.control:not(:last-child) {\n    margin-bottom: .75rem;\n  }\n  .field.is-grouped.is-grouped-multiline {\n    flex-wrap: wrap;\n  }\n  .field.is-grouped>.control:not(:last-child) {\n    margin-bottom: 0;\n    margin-right: .75rem;\n  }\n  .field.is-grouped>.control {\n    flex-shrink: 0;\n  }\n  .card, .control {\n    position: relative;\n  }\n  .control {\n    box-sizing: border-box;\n    clear: both;\n    font-size: 1rem;\n    text-align: left;\n  }\n  .footer-icon {\n    height: 1.5rem;\n    width: 1.5rem;\n  }\n  .fade-in-section {\n    opacity: 0;\n    transform: translateY(20vh);\n    visibility: hidden;\n    transition: opacity 0.6s ease-out, transform 1.2s ease-out;\n     will-change: opacity, visibility;\n  }\n  .fade-in-section.is-visible {\n    opacity: 1;\n    transform: none;\n    visibility: visible;\n  }\n  .grid {\n    display: grid;\n    grid-template-columns: repeat(auto-fill, minmax(8em, 1fr));\n    grid-gap: 1em 2em;\n    text-align: center;\n    &.div {\n    grid-gap: 1em;\n      p {\n        margin: 0;\n      }\n      img {\n        border: thin solid darkgray;\n        border-radius: 5px;\n        overflow: hidden;\n      }\n    }\n  }\n  /* center image captions */\n  .gatsby-resp-image-wrapper + em, img + em {\n    margin-top: 0.3em;\n    display: block;\n    text-align: center;\n    margin-left: auto;\n    margin-right: auto;\n    font-size: 0.95em;\n    background: transparent;\n  }\n  blockquote {\n    margin: 0;\n  }\n  table {\n    border-collapse: collapse;\n    width: 100%;\n  }\n  table td, table th {\n    border: thin solid ", ";\n    padding: 0.2em 0.6em;\n  }\n  tbody tr:nth-child(odd) {\n    background: ", ";\n    color: ", ";\n  }\n  div.scroll {\n    overflow: scroll;\n    margin: 1em auto;\n    border: 1px solid ", ";\n    border-width: 0 1px;\n    white-space: nowrap;\n    table td, table th {\n      :first-child {\n        border-left: none;\n      }\n      :last-child {\n        border-right: none;\n      }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var phone = _mediaQueries.screens.phone,
    desktop = _mediaQueries.screens.desktop;
var fonts = _typography["default"].fonts,
    minFontSize = _typography["default"].minFontSize,
    maxFontSize = _typography["default"].maxFontSize,
    minLineHeight = _typography["default"].minLineHeight,
    maxLineHeight = _typography["default"].maxLineHeight;
var GlobalStyle = (0, _styledComponents.createGlobalStyle)(_templateObject(), fonts, minFontSize, minLineHeight, _mediaQueries["default"].minPhone, minFontSize, maxFontSize, minFontSize, phone, desktop, phone, minLineHeight, maxLineHeight, minLineHeight, phone, desktop, phone, _mediaQueries["default"].minDesktop, maxFontSize, maxLineHeight, function (props) {
  return props.theme.darkGray;
}, function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
}, function (props) {
  return props.theme.lightGray;
});
exports.GlobalStyle = GlobalStyle;