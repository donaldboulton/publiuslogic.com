import React, { useState } from "react";
import Nav from '../Nav'
import { HeaderContainer, Logo } from './styles'
import Brand from './Brand'
import Toggle from '../DarkMode/DarkModeToggle'
import NavLogin from '../NavLogin'
import Search from '../Search'
import popping from '../../../static/audio/popping.mp3'
import useInterval from "../../hooks/useInterval"
import useSound from 'use-sound'

const searchIndices = [
  { name: `Posts`, title: `Blog Posts`, type: `postHit` },
]

const Header = () => {
  const [isRunning, setIsRunning] = useState(true);
  const [checked, setChecked] = useState(false);
  const [count, setCount] = useState(0);
  const [play] = useSound(popping, { volume: 0.5 });

  useInterval(
    () => {
      setCount(count + 1);
      if (checked) {
        play();
      }
    },
    isRunning ? 3000 : null
  );

  const reset = () => {
    setIsRunning(false);
    setCount(0);
  };

  const toggle = () => {
    setChecked(!checked);
  };
  return (
    <>
      <HeaderContainer id='header' className='footer-background'>
        <Logo to='/' title='Publius Logic' rel='home'>
          <Brand
            css='grid-area: title;'
          />
        </Logo>
        <Nav
          css='grid-area: nav;'
        />
        <NavLogin
          css='grid-area: login;'
        />
        <Toggle
          css='grid-area: toggle;'
          handleChange={toggle} 
          checked={checked}
        />
        <Search collapse indices={searchIndices} css='grid-area: search;' />
      </HeaderContainer>
    </>
  )
}

export default Header
