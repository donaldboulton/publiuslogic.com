import React, { useRef, useState, useLayoutEffect } from 'react'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { VideoTitle, VideoDiv, VideoIcon, VideoToggle } from './styles'
import ReactPlayer from 'react-player/lazy'
import useVideoQuery from '../../hooks/useVideoQuery'

function useLockBodyScroll () {
  useLayoutEffect(() => {
    // Get original body overflow
    const originalStyle = window.getComputedStyle(document.body).overflow
    // Prevent scrolling on mount
    document.body.style.overflow = 'hidden'
    // Re-enable scrolling when component unmounts
    return () => document.body.style.overflow = originalStyle
  }, []) // Empty array ensures effect is only run on mount and unmount
}

const Player = () => {
  const { videoData, videoTitle, videoUrl } = useVideoQuery()
  const [open, setOpen] = useState(false)
  const ref = useRef()
  useOnClickOutside(ref, () => setOpen(false))
  useLockBodyScroll()

  return (
    <>
      <VideoToggle opener open={open} onClick={() => setOpen(true)} />
      <VideoDiv ref={ref} open={open} className='nav-background' onScroll={e => e.preventDefault()}>
        <VideoTitle>
          <VideoIcon />
          {videoTitle}
          <VideoToggle onClick={() => setOpen(false)} />
        </VideoTitle>
          <div className='center react-player-wrapper'>
            <ReactPlayer
              ref={ref}
              url={videoUrl}
              muted='true'
              controls='true'
              width='100%'
              height='100%'
              pip='true'
              config={{
                file: {
                  attributes: {
                    style: { height: "100%", objectFit: "cover" },
                  },                
                  tracks: [
                    {kind: 'subtitles', src: 'subs/subtitles.en.vtt', srcLang: 'en', default: true},
                    {kind: 'subtitles', src: 'subs/subtitles.ja.vtt', srcLang: 'ja'},
                    {kind: 'subtitles', src: 'subs/subtitles.de.vtt', srcLang: 'de'}
                  ]
                },
              }}
            />
          </div>
        </VideoDiv>
    </>
  )
}

export default Player