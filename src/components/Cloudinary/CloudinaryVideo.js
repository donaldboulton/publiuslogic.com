import React from 'react'
import {Video, CloudinaryContext} from 'cloudinary-react'

const Player = () => {
  return (
    <div className='center'>
      <CloudinaryContext cloudName="mansbooks">
        {/* Cloudinary Video Player embed code */}
        <Video
          id='player'
          cloudName="mansbooks"
          publicId="videos/Jefferson_Airplane_White_Rabbit_Grace_Slick_Woodstock_aug_17_1969"
          controls
          class="cld-video-player cld-video-player-skin-dark cld-fluid"
          poster={{publicId: "videos/Jefferson_Airplane_White_Rabbit_Grace_Slick_Woodstock_aug_17_1969"}}
          fallbackContent="Your browser does not support HTML5 video tags."  
          loop="false"
          allow="fullscreen; encrypted-media; picture-in-picture"
          allowfullscreen
          >
        </Video>
      </CloudinaryContext>
    </div>
  )
}

export default Player
