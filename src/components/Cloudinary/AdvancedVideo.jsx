import React from 'react'
import {AdvancedVideo} from '@cloudinary/react';
import {Cloudinary} from '@cloudinary/base'
import {Transformation} from '@cloudinary/base'
import {accelerate} from '@cloudinary/base/actions/effect'
import {brightness} from '@cloudinary/base/actions/adjust'
import {max} from '@cloudinary/base/actions/roundCorners'
import {concatenate} from '@cloudinary/base/actions/videoEdit'
import {Concatenate} from '@cloudinary/base/qualifiers/concatenate'

const CloudinaryVideo = () => {
// Create and configure your Cloudinary instance.
const cld = new Cloudinary({
  cloud: {
    cloudName: 'mansbooks',
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
  }
}); 
// Use the video with public ID, 'white_rabbit'.
const myVideo = cld.video('https://res.cloudinary.com/mansbooks/video/upload/v1599077116/fl_splice,l_video:white_rabbit/fl_layer_apply/fl_splice,l_video:white_rabbit/e_accelerate:-1/fl_layer_apply/e_brightness:10/r_max/w_400/white_rabbit')
myVideo
// Apply the transformation.
.videoEdit(concatenate(Concatenate.videoSource('white_rabbit').transformation(new Transformation().effect(accelerate(-1)))))
  .adjust(brightness(10))
  .roundCorners(max());
// Get the URL of the video.
const myURL = myVideo.toURL();

  return (
    <div className='center video_holder'>
        <AdvancedVideo
          width='100%'
          id='example-player'
          class='cloudinary-video cld-video-player cld-video-player-skin-dark cld-fluid controls'
          secure
          controls
          cldVideo='https://res.cloudinary.com/mansbooks/video/upload/v1599077116/fl_splice,l_video:white_rabbit/fl_layer_apply/fl_splice,l_video:white_rabbit/e_accelerate:-1/fl_layer_apply/e_brightness:10/r_max/w_400/white_rabbit.mp4'
          publicId={myVideo}
          poster='https://res.cloudinary.com/mansbooks/video/upload/v1599077116/white_rabbit'
        >            
        </AdvancedVideo>
    </div>
  )
}

export default CloudinaryVideo