import React from 'react'
import popping from '../../../static/audio/popping.mp3'
import useSound from 'use-sound'

const Toggle = ({ checked, onChange, handleChange }) => {
  const [play] = useSound(popping, { volume: 0.7 })
  return (
  <span className='toggle-control'>
    <input
      aria-label='switch'
      className='dmcheck'
      type='checkbox'
      checked={checked}
      onChange={onChange, handleChange}
      id='dmcheck'
    />
    <label htmlFor='dmcheck' 
      onClick={() => {
        play();
      }}
    />
  </span>
  )
}

export default Toggle
