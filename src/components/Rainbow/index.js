import React from 'react'
import styled from 'styled-components'
import useRainbow from '../../hooks/useRainbow'

const Rainbow = ({
  children,
  intervalDelay = 1300,
  ...delegated
}) => {
  const transitionDelay = intervalDelay * 1.25

  const colors = useRainbow({ intervalDelay })

  const colorKeys = Object.keys(colors)

  return (
    <SpanElem
      {...delegated}
      style={{
        ...colors,
        transition: `
          ${colorKeys[0]} ${transitionDelay}ms linear,
          ${colorKeys[1]} ${transitionDelay}ms linear,
          ${colorKeys[2]} ${transitionDelay}ms linear
        `,
        background: `
          radial-gradient(
            circle at top left,
            var(${colorKeys[2]}),
            var(${colorKeys[1]}),
            var(${colorKeys[0]})
          )
        `,
      }}
    >
      {children}
    </SpanElem>
  )
}

const SpanElem = styled.span`
  position: relative;
  min-width: 52px;
  max-width: 56px;
  margin-right: 1em;
  border-radius: 50%;
`

export default Rainbow