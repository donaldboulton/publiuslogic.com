import styled from 'styled-components'

export const AccordionSection = styled.div`
  display: flex;
  flex-direction: column;
`
export const AccordionSectionButton = styled.button`
  background-color: ${props => props.theme.white};
  color: ${props => props.theme.textColor};
  cursor: pointer;
  padding: 18px;
  display: flex;
  align-items: center;
  border: none;
  outline: none;
  transition: background-color 0.6s ease;
`
export const AccordionContent = styled.div`
  background-color: ${props => props.theme.white};
  overflow: auto;
  transition: max-height 0.6s ease;
`
export const AccordionText= styled.div`
  background-color: ${props => props.theme.white};
  overflow: auto;
  transition: max-height 0.6s ease;
`
