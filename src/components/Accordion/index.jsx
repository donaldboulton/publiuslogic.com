import React, { useState, useRef } from 'react'
import Chevron from './Chevron'
import { AccordionSection, AccordionSectionButton, AccordionText, AccordionContent } from './styles'

function Accordion(props) {
  const [setActive, setActiveState] = useState("");
  const [setHeight, setHeightState] = useState("0px");
  const [setRotate, setRotateState] = useState("accordion-icon");

  const content = useRef(null);

  function toggleAccordion() {
    setActiveState(setActive === "" ? "active" : "");
    setHeightState(
      setActive === "active" ? "0px" : `${content.current.scrollHeight}px`
    );
    setRotateState(
      setActive === "active" ? "accordion-icon" : "accordion-icon rotate"
    );
  }

  return (
    <AccordionSection>
      <AccordionSectionButton className={`accordion ${setActive}`} onClick={toggleAccordion}>
        <p className="accordion-title">{props.title}</p>
        <Chevron className={`${setRotate}`} width={10} fill={"#777"} />
      </AccordionSectionButton>
      <AccordionContent
        ref={content}
        style={{ maxHeight: `${setHeight}` }}
        className="accordion-content"
      >
        <AccordionText
          className="accordion-text"
          dangerouslySetInnerHTML={{ __html: props.content }}
        />
      </AccordionContent>
    </AccordionSection>
  )
}

export default Accordion
