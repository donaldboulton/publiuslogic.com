import ReactDOM from 'react-dom'
import useHasMounted from '../../hooks/useHasMounted'

export default function InPortal({ id, children }) {
  const hasMounted = useHasMounted()
  if (!hasMounted) {
    return null;
  }
  return ReactDOM.createPortal(
    children,
    document.querySelector(`#${id}`)
  )
}