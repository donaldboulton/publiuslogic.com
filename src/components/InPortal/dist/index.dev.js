"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = InPortal;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _useHasMounted = _interopRequireDefault(require("../../hooks/useHasMounted"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function InPortal(_ref) {
  var id = _ref.id,
      children = _ref.children;
  var hasMounted = (0, _useHasMounted["default"])();

  if (!hasMounted) {
    return null;
  }

  return _reactDom["default"].createPortal(children, document.querySelector("#".concat(id)));
}