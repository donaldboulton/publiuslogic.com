import styled from 'styled-components'
import mediaQuery from './../../utils/mediaQuery'

export const PostLinkWrapper = styled.div`
  padding: 2vh 3vw;
  grid-auto-flow: column;
  justify-items: center;
  justify-content: center;
  align-content: center;
  align-items: center;
  grid-template-areas:
    'pages'
    'posts'
    'admin'
  grid-gap: 2vh 1vw;
  ${mediaQuery.minPhone} {
    grid-template-areas:
      'pages posts admin';
  }
  ${mediaQuery.minDesktopArea} {
    grid-template-areas: 
      'pages posts admin';
  }
  ${mediaQuery.minDesktop} {
    grid-template-areas: 
      'pages posts admin';
  }
`

