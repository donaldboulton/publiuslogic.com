"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PostLinkWrapper = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _mediaQueries = _interopRequireDefault(require("./../../utils/mediaQueries"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  padding: 2vh 3vw;\n  grid-auto-flow: column;\n  justify-items: center;\n  justify-content: center;\n  align-content: center;\n  align-items: center;\n  grid-template-areas:\n    'pages'\n    'posts'\n    'admin'\n  grid-gap: 2vh 1vw;\n  ", " {\n    grid-template-areas:\n      'pages posts admin';\n  }\n  ", " {\n    grid-template-areas: \n      'pages posts admin';\n  }\n  ", " {\n    grid-template-areas: \n      'pages posts admin';\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var PostLinkWrapper = _styledComponents["default"].div(_templateObject(), _mediaQueries["default"].minPhone, _mediaQueries["default"].minDesktopArea, _mediaQueries["default"].minDesktop);

exports.PostLinkWrapper = PostLinkWrapper;