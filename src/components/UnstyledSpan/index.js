import styled from 'styled-components'

export const UnstyledSpan = styled.span`
  display: block;
  margin: 0;
  padding: 0;
  border: none;
  background: transparent;
`;

export const UnstyledSpanContents = styled.span`
  margin: 0;
  padding: 0;
  border: none;
  background: transparent;
  cursor: pointer;
  
`;