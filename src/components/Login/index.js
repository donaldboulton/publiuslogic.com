import React, { useState } from 'react'
import { useMutation, gql } from '@apollo/client'
import { navigate } from 'gatsby'
import { createMemorySource, createHistory } from '@reach/router'
import {
  isLoggedIn,
  setToken,
  setUser,
  getPrivateRoute,
  tokenKey,
} from "../../networking/auth"

function Login() {
  const CREATE_MUTATION = gql`
    mutation createMutation(
      $email: String!
      $password: String!
      $name: String!
    ) {
      create(
        email: $email
        password: $password
        name: $name
      ) {
        token
      }
    }
  `;

  const LOGIN_MUTATION = gql`
    mutation LoginMutation(
      $email: String!
      $password: String!
    ) {
      login(email: $email, password: $password) {
        token
      }
    }
  `;

  // Logged-in users should not see the login page
  if (isLoggedIn()) navigate(getPrivateRoute())
  
  // for some types of tests you want a memory source
  let source = createMemorySource("/app/login")
  let history = createHistory(source)

  const [formState, setFormState] = useState({
    login: true,
    email: '',
    password: '',
    name: ''
  });
  const [login, { data, error, user }] = useMutation(LOGIN_MUTATION, {
    variables: {
      email: formState.email,
      password: formState.password
    },
    onCompleted: ({ login }) => {
      setToken(tokenKey, login.token)
      setUser(user, data.login.user)
      history.push('/app/');
    }
  });
  
  const [create] = useMutation(CREATE_MUTATION, {
    variables: {
      name: formState.name,
      email: formState.email,
      password: formState.password
    },
    onCompleted: ({ create }) => {
      setToken(tokenKey, create.token);
      setUser(user, data.login.user)
      history.push('/app/');
    }
  });

  return (
    <div>
      <h3 className="h3"
        style={{
          marginLeft: '2em'
        }}
      >
        {formState.login ? 'Login with the form Below' : 'Sign Up with the form below'}
      </h3>
      <div
        className='box'
        style={{
          margin: `2em`,
        }}
      >
        {error && (
          <div
            style={{
              padding: '1em',
              borderRadius: '3px',
              backgroundColor: '#8e0b30',
              color: '#fe7299',
            }}
          >
            {error.message}
          </div>
        )}
        {!formState.login && (
          <div
            style={{
              marginBottom: '1em',
              marginTop: '1em',
            }}
          >
          <input
            className='input'
            value={formState.name}
            onChange={(e) =>
              setFormState({
                ...formState,
                name: e.target.value
              })
            }
            type="text"
            placeholder="Your name"
            required
          />
          </div>
        )}
        <div
          style={{
            marginBottom: '1em',
            marginTop: '1em',
          }}
        >
        <input
          className='input'
          value={formState.email}
          onChange={(e) =>
            setFormState({
              ...formState,
              email: e.target.value
            })
          }
          type="text"
          placeholder="Your email address"
          required
        />
        </div>
        <div
          style={{
            marginBottom: '1em',
          }}
         >
        <input
          className='input'
          value={formState.password}
          onChange={(e) =>
            setFormState({
              ...formState,
              password: e.target.value
            })
          }
          type="password"
          placeholder="Choose a Safe Password"
          required
        />
        </div>      
        <div
          style={{
            marginBottom: '1em',
            marginLeft: '2em',
            marginRight: '1em',
          }}
        >
          <button
            className="pointer button"
            onClick={formState.login ? login : create}
          >
            {formState.login ? 'login' : 'create account'}
          </button>
          <button
            className="pointer button"
            onClick={(e) =>
              setFormState({
               ...formState,
                 login: !formState.login
              })
            }
          >
            {formState.login
              ? 'Need to create an account?'
              : 'Already have an account?'}
          </button>
        </div>
      </div>
    </div>
  )
}

export default Login
