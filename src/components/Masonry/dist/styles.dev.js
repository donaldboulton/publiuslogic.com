"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Col = exports.MasonryDiv = exports.Child = exports.Parent = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n  grid-gap: ", ";\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n  grid-auto-flow: column;\n  grid-gap: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  grid-row: span ", ";\n  height: max-content;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n  grid-template-columns: repeat(auto-fit, minmax(", ", 1fr));\n  grid-auto-rows: calc(", "px - 2em);\n  grid-gap: 2em;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Parent = _styledComponents["default"].div(_templateObject(), function (props) {
  return props.colWidth;
}, function (props) {
  return props.rowHeight;
});

exports.Parent = Parent;

var Child = _styledComponents["default"].div(_templateObject2(), function (props) {
  return props.span;
});

exports.Child = Child;

var MasonryDiv = _styledComponents["default"].div(_templateObject3(), function (props) {
  return props.gap || "1em";
});

exports.MasonryDiv = MasonryDiv;

var Col = _styledComponents["default"].div(_templateObject4(), function (props) {
  return props.gap || "1em";
});

exports.Col = Col;