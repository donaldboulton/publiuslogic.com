import React, { useRef, useState, useLayoutEffect } from 'react'
import hotjar from '../../../static/img/HotJar-250x100_logo.png'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { HotJarDiv, HotJarTitle, HotJarToggle, HotJarIcon, Box } from './styles'
import { TableOfContents } from '../styles/ArticleStyles'

function useLockBodyScroll () {
  useLayoutEffect(() => {
    // Get original body overflow
    const originalStyle = window.getComputedStyle(document.body).overflow
    // Prevent scrolling on mount
    document.body.style.overflow = 'hidden'
    // Re-enable scrolling when component unmounts
    return () => document.body.style.overflow = originalStyle
  }, []) // Empty array ensures effect is only run on mount and unmount
}

const HotJar = ({ group, ...rest }) => {
  const { hotJarTitle = `HotJar` } = rest
  const [open, setOpen] = useState(false)
  const ref = useRef()
  useOnClickOutside(ref, () => setOpen(false))
  useLockBodyScroll()

  return (
    <>
      <HotJarToggle opener open={open} onClick={() => setOpen(true)} />
      <HotJarDiv ref={ref} open={open} className='nav-background' onScroll={e => e.preventDefault()}>
        <HotJarTitle>
          <HotJarIcon />
          {hotJarTitle}
          <HotJarToggle onClick={() => setOpen(false)} />
        </HotJarTitle>
        <TableOfContents>
        <nav className='nav-scroll'>
          <div className='center'>
            <a href='https://www.hotjar.com/r/r543e6a' target='_blank' rel='noopener noreferrer'>
              <img
                src={hotjar}
                alt='Hotjar'
                style={{ width: '140px', height: '79px' }}
              />
            </a>
          </div>
          <span>
            <h3 className='center'>Heatmaps Visualize behavior.</h3>
            <h4 className='center'>Hotjar is the fast and visual way To...</h4>
            <Box>
              Understand what users want, care about and do on your site by visually representing their clicks, taps and scrolling behavior.
            </Box>
          </span>
        </nav>
      </TableOfContents>
    </HotJarDiv>
    </>
  )
}

export default HotJar

