import styled, { css } from 'styled-components'
import { Close as Cross } from '@styled-icons/material/Close'
import { Hotjar } from '@styled-icons/simple-icons/Hotjar'
import mediaQuery from '../../utils/mediaQuery'

export const HotJarDiv = styled.div`
  grid-column-start: aside-start;
  grid-column-end: aside-end;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
  padding: 1vmin;
  height: max-content;
  max-height: 85vh;
  border-radius: 4px;
  z-index: 3;
  line-height: 2em;
  margin: .2em;
  min-width: 20em;
  width: 165px;
  overscroll-behavior: none;
  box-shadow: 0 0 1em rgba(0, 0, 0, 0.5);
  border: thin ${props => props.theme.darkerGray};
  overflow-x: hidden;
  overflow-y: hidden;
  ${mediaQuery.maxLaptop} {
    position: fixed;
    bottom: 1em;
    background: ${props => props.theme.black};
    color: ${props => props.theme.white};
    left: 1em;
    ${props => !props.open && `height: 0;`};
    visibility: ${props => (props.open ? `visible` : `hidden`)};
    opacity: ${props => (props.open ? 1 : 0)};
    transition: 0.3s;
  }
  ${mediaQuery.minLaptop} {
    font-size: 0.85em;
    grid-column: 4 / -1;
    position: -webkit-sticky;
    position: sticky;
    top: 2em;
    background: ${props => props.theme.black};
    color: ${props => props.theme.white};
  }
  nav {
    max-height: 70vh;
    overflow-x: hidden;
  }
`

export const HotJarTitle = styled.h3`
  margin: 1em;
  display: grid;
  grid-auto-flow: column;
  align-items: center;
  grid-template-columns: auto auto 1fr;
  color: ${props => props.theme.white}; 
  border-bottom: thin ${props => props.theme.darkerGray};
`
export const BookIcon = styled(Hotjar)`
  font-size: 2em;
  width: 2em;
  height: 2em;
  margin-right: 0.2em;
  color: ${props => props.theme.white};
`
export const HotJarIcon = styled(Hotjar)`
  font-size: 1.1em;
  width: 1.1em;
  height: 1.1em;
  margin-right: 1em;
  color: ${props => props.theme.white};
`

export const Box = styled.div`
  padding: 1em;
  margin: 1.1em;
  border-radius: ${props => props.theme.mediumBorderRadius};
  border: 1px solid ${props => props.theme.darkerGray};
  background: radial-gradient(
    circle closest-corner at center 75px,
    ${props => props.theme.darkestGray},
    ${props => props.theme.black} 40%,
  ) no-repeat;
  text-align: center;
`
const openedCssH = css`
  position: fixed;
  z-index: 25;
  top: calc(3.5vh + 5em);
  ${mediaQuery.minLaptop} {
    top: calc(2vh + 5em);
  }
  ${mediaQuery.minPhone} {
    display: none;
  }
  left: 0;
  padding: 0.4em 0.5em 0.4em 0.2em;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
  border: 2px solid gray;
  border-radius: 0 50% 50% 0;
  transform: translate(${props => (props.open ? `-100%` : 0)});
`

const closedCssH = css`
  font-size: 1em;
  margin-left: 1em;
  border: 1px solid gray;
  border-radius: 50%;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
`

export const HotJarToggle = styled(Cross).attrs(props => ({
  as: props.opener && Hotjar,
  size: props.opener ? `2em` : `2.4em`,
}))`
  z-index: 2;
  transition: 0.3s;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
  justify-self: end;
  :hover {
    transform: scale(1.1);
  }
  ${mediaQuery.minLaptop} {
    display: none;
  }
  ${props => (props.opener ? openedCssH : closedCssH)};
`