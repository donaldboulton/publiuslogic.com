import React, { useState } from 'react'
import { useTransition, animated, useSpring } from 'react-spring/renderprops'

const PoppingArrows = () => {
    const [isDisplay, setIsDisplay] = useState(true)
    const [hover, setHover] = useState(false)
    const hoverAnimation = useSpring({
      transform: hover ? 'rotate(180deg)' : 'rotate(0deg)',
  
    })
    const transitions = useTransition(isDisplay, null, {
      from: { transform: `translateX(50px)`, opacity: 0 },
      enter: { transform: `translateX(0px)`, opacity: 1 },
      leave: { transform: `translateX(50px)`, opacity: 0 },
    })
    return (        
      <div className="popping-arrows"
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        style={{
            position: relative,
            display: inline-flex,
            alignItems: center,
            fontSize: `16px`,
            fontWeight: `700`,
        }}
      >
        {transitions.map(
            ({ item }) =>
              item && (
                <animated.svg width="36" height="12" viewBox="0 0 36 12" style={hoverAnimation} fill="none" className="arrow-svg">
                  <path d="M0.75 6H11.25 M6 0.75L11.25 6L6 11.25" stroke="#d64000" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0; transition: opacity 125ms ease 0s;"></path>
                  <path d="M15 10L19.5 5.5L15 1" stroke="#d64000" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0; transition: opacity 125ms ease 0s;"></path>
                  <path d="M23 10L27.5 5.5L23 1" stroke="#d64000" strokeOpacity="0.66" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0; transition: opacity 125ms ease 125ms;"></path>
                  <path d="M31 10L35.5 5.5L31 1" stroke="v#d64000" strokeOpacity="0.35" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0; transition: opacity 125ms ease 250ms;"></path>
                </animated.svg>
            )
        )}
      </div>        
    )
}

export default PoppingArrows