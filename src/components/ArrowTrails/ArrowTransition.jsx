import React, { useState, useCallback } from 'react'
import { useTransition, animated, useSpring } from 'react-spring'
import { ArrowTrails } from './styles'

const pages = [
  ({ style }) => <animated.div style={{ ...style, background: 'transparent' }}>👉</animated.div>,
  ({ style }) => <animated.div style={{ ...style, background: 'lightblue' }}>👉</animated.div>,
  ({ style }) => <animated.div style={{ ...style, background: 'lightgreen' }}>👉</animated.div>,
]

function ArrowTransition() {
    const [isDisplay, setIsDisplay] = useState(true)
    const [hover, setHover] = useState(false)
    const hoverAnimation = useSpring({
      transform: hover ? 'rotate(180deg)' : 'rotate(0deg)',
  
    })
  const [index, set] = useState(0)
  const onClick = useCallback(() => set(state => (state + 1) % 3), [])
  const transitions = useTransition(index, p => p, {
    from: { opacity: 0, transform: 'translate3d(100%,0,0)' },
    enter: { opacity: 1, transform: 'translate3d(0%,0,0)' },
    leave: { opacity: 0, transform: 'translate3d(-50%,0,0)' },
  })
  return (
    <ArrowTrails onClick={onClick}>
      {transitions.map(({ item, props, key }) => {
        const Page = pages[item]
        return <Page key={key} style={props} />
      })}
    </ArrowTrails>
  )
}

export default ArrowTransition