import React, { useState } from 'react'
import { useTransition, animated, useSpring } from 'react-spring/renderprops'

const ArrowTrails = () => {
  const [isDisplay, setIsDisplay] = useState(true)
  const [hover, setHover] = useState(false)
  const hoverAnimation = useSpring({
    transform: hover ? 'rotate(180deg)' : 'rotate(0deg)',

  })
  const transitions = useTransition(isDisplay, null, {
    from: { transform: `translateX(50px)`, opacity: 0 },
    enter: { transform: `translateX(0px)`, opacity: 1 },
    leave: { transform: `translateX(50px)`, opacity: 0 },
  })

  return (
    <>
      <div 
        style={{ 
          width: `50`, 
          height: `20`, 
        }}
      >
        {transitions.map(
          ({ item, key, props }) =>
            item && (
              <animated.svg key={key} style={{ ...props, display: 'inline-block' }}>
                <svg width="36" height="12" viewBox="0 0 36 12" fill="none" className="PoppingArrows" xmlns="http://www.w3.org/2000/svg">
                    <animated.path style={hoverAnimation} d="M0.75 6H11.25 M6 0.75L11.25 6L6 11.25" stroke="#d64000" stroke-linecap="round" stroke-linejoin="round" style="opacity: 1 transition: opacity 125ms ease 0s"></animated.path>
                    <path d="M15 10L19.5 5.5L15 1" stroke="#d64000" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0 transition: opacity 125ms ease 0s"></path>
                    <path d="M23 10L27.5 5.5L23 1" stroke="#d64000" strokeOpacity="0.66" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0 transition: opacity 125ms ease 125ms"></path>
                    <path d="M31 10L35.5 5.5L31 1" stroke="#d64000" strokeOpacity="0.35" strokeLinecap="round" strokeLinejoin="round" style="opacity: 0 transition: opacity 125ms ease 250ms"></path>
                </svg>
              </animated.svg>
            )
        )}
      </div>
      <button
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        Hover
      </button>
    </>
  )
}

export default ArrowTrails
