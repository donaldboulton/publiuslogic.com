import React from 'react'
import { useSprings, animated } from 'react-spring'

const arrows = [
  {
    key: 'arrow1',
    color: 'red',
    from: {
      width: '0px',
    },
    to: {
      width: '10px',
    },
    config: {
      mass: 20,
    },
  },
  {
    key: 'arrow2',
    color: 'pink',
    from: {
      width: '0px',
    },
    to: {
      width: '12px',
    },
    config: {
      mass: 30,
    },
  },
  {
    key: 'arrow3',
    color: 'pink',
    from: {
      width: '0px',
    },
    to: {
      width: '15px',
    },
    config: {
      mass: 13,
    },
  },
];

const ArrowGraph = () => {
  const springs = useSprings(
    arrows.length,
    arrows.map(({ color, key, ...config }) => config)
  );

  return springs.map((spring, index) => (
    <animated.div
      style={{
        ...spring,
        height: '20px',
        display: 'inline-block',
        backgroundColor: arrows[index].color,
      }}
    />
  ))
}

export default ArrowGraph
