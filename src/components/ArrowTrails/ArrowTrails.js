import React, { useState } from 'react'
import { useTrail, animated } from 'react-spring'

const items = [">", ">", ">",]
const config = { mass: 5, tension: 2000, friction: 200 }

function ArrowTrails() {
  const [state, setState] = useState(false)
  const trail = useTrail(items.length, {
    config,
      from: { opacity: 0, y: 20 },
      to: { opacity: state ? 1 : 0, y: state ? 20 : 10 }
    })
  return (
     <span
       className="trails-span"
       onMouseEnter={() => setState((state) => !state)}      
       onMouseLeave={() => setState((state) => !state)}       
     >
      <span className="arrow" role="img" area-label="next">
        {'>'}
      </span>
      {trail.map(({ y, ...otherProps }, i) => (
        <animated.span
          key={items[i]}
          style={{
            ...otherProps,
            transform: y.to(y => `translate3d(${y}px, 0, 0)`)
          }}
          className="trails-text"
        >
          <animated.span>{items[i]}</animated.span>
        </animated.span>
      ))}
    </span>
  )
}

export default ArrowTrails
