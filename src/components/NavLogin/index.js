import React, { useState, useRef } from 'react'
import { Link } from 'gatsby'
import Modali, { useModali } from 'modali'
import UserAvatar from 'react-user-avatar'
import { User } from './styles'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import Login from '../Login'
import { NavEntryLogin, SubNavLogin } from '../Nav/Desktop/styles'
import {
isLoggedIn,
getUser,
} from '../../networking/auth'

const NavLogin = () => {
  const ref = useRef()
  const [isOpen, setOpen] = useState(false)
  useOnClickOutside(ref, () => setOpen(false))
  const avatar_url = 'https://avatars1.githubusercontent.com/u/10813159?v=4'
  const user = 'Donald Boulton'
  const email = 'donaldboulton@gmail.com'
  const [completeExample, toggleCompleteModal] = useModali({
    animated: true,
    title: 'Login 💘',
    message: 'Login To Backend.',
  })

  return (
    <>
      <div>
        {isLoggedIn() ? (
          <>
            <NavEntryLogin key={avatar_url} aria-haspopup='true'>
              <button className='identity-logout button-transparent' onClick={() => setOpen(true)}>
                {avatar_url &&
                  <UserAvatar
                    size='24'
                    style={{
                      height: `1.2em`,
                      width: `1.2em`,
                      borderRadius: `50%`,
                      cursor: `pointer`,
                    }}
                    className='user-icon'
                    name={email} src={avatar_url}
                  />}
              </button>
              {isOpen && (
                <SubNavLogin ref={ref} className='sub-nav'>
                  <div className='menu-item'><span className='h3'>Welcome!</span></div>
                  <div className='menu-item'><span role='img' aria-labelledby='User Name'>😀</span> {user}</div>
                  <div className='menu-item'><span role='img' aria-labelledby='Email'> 📧</span> {email}</div>                  
                  <Link className='menu-item' to='/sitemap.xml'><span role='img' aria-labelledby='Sitemap'> 🗺️</span> Sitemap Index</Link>
                  <Link className='menu-item' to='/contact'><span role='img' aria-labelledby='Locked'> 📇</span> Contact Form</Link>
                  <Link className='menu-item' to='/gallery'><span role='img' aria-labelledby='Cat Gallery'> 🐱</span> Cat Gallery</Link>
                  <Link className='menu-item' to='/privacy'><span role='img' aria-labelledby='Scales'> ⚖️</span> Privacy and Terms</Link>                  
                  <hr className='navbar-divider' />
                  <Link className='menu-item' to='/admin/#/'><span role='img' aria-labelledby='Admin CMS'> 🔓</span> Admin CMS</Link> 
                  <Link className='menu-item' to='/app/'><span role='img' aria-labelledby='Admin Users'> ✨</span> Admin Users</Link>
                </SubNavLogin>
              )}
            </NavEntryLogin>
          </>
        ) : (
          <>
            <NavEntryLogin key={User}>
              <button aria-label='Login' className='identity-login menu-item button-transparent' onClick={() => setOpen(true)}>
                <User className='user-icon' />
              </button>
              {isOpen && (
                <SubNavLogin ref={ref} className='sub-nav'>
                  <div className='menu-item'><span role='img' aria-labelledby='Heart'>💘</span>&nbsp;<button className='button' onClick={toggleCompleteModal}>Login</button></div>
                  <Link className='menu-item' to='/contact'><span role='img' aria-labelledby='Contact Page'> 📇</span> Contact</Link> 
                  <Link className='menu-item' to='/gallery'><span role='img' aria-labelledby='Send Mail'> 🖼️</span> Gallery</Link> 
                  <Link className='menu-item' to='/privacy'><span role='img'>⚖️</span> Privacy and Terms</Link> 
                  <Link className='menu-item' to='/sitemap.xml'><span role='img'> 🗺️</span> Sitemap Index</Link>                                   
                  <hr className='navbar-divider' />                  
                  <Link className='menu-item' to='/admin/#/'><span role='img'> 🔐</span> Admin CMS</Link>                  
                  <Link className='menu-item' to='/app/'><span role='img'> 💕</span> Admin Users</Link>
                </SubNavLogin>
              )}
            </NavEntryLogin>              
          </>
        )}
      </div>
      <Modali.Modal {...completeExample} className='modali-size-large'>
        <div>
          <p>
            <div>
              <Login />
            </div>
          </p>
        </div>
      </Modali.Modal>
    </>
  )
}

export default NavLogin
