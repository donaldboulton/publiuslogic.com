import styled from 'styled-components'
import { UserCircle } from '@styled-icons/fa-solid/UserCircle'

export const User = styled(UserCircle)`
  width: 1.2em;
  top: calc(1.3vh + 1.5em);
  color: ${props => props.theme.white};
  z-index: 120;
  cursor: pointer;
`
