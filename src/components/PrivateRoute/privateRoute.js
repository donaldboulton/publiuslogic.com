import React, { useEffect } from 'react'
import { navigate } from 'gatsby'
import { useIdentityContext } from 'react-netlify-identity-widget'

function PrivateRoute (props) {
  const identity = useIdentityContext()
  const isLoggedIn = identity && identity.isLoggedIn
  const { component: Component, location, ...rest } = props

  useEffect(
    () => {
      if (!isLoggedIn && location.pathname !== `/app/main`) {
        // If the user is not logged in, redirect to the login page.
        navigate(`/app/`)
      }
    },
    [isLoggedIn, location],
  )
  return isLoggedIn ? <Component {...rest} /> : null
}

export default PrivateRoute
