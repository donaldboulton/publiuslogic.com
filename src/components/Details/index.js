import React from 'react'
import { StyledH1 } from '../styles/ArticleStyles'
import { FaunaContext, UserContext } from '../../contexts'
import UserAvatar from 'react-user-avatar'
import { useIdentityContext } from 'react-netlify-identity'
import { getCurrentUser } from "../../networking/auth"

const Details = () => {
  const { email } = getCurrentUser()
  const identity = useIdentityContext()
  const name =
  (identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.full_name) || 'NoName'
  const avatar_url = identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.avatar_url
  console.log(JSON.stringify(identity))
  const isLoggedIn = identity && identity.isLoggedIn
  return (
    <>
      <div>
        <StyledH1>
          Your Details
        </StyledH1>
          <div
            style={{
              marginBottom: 2,
            }}
          >            
          <> 
            <FaunaContext.Provider value={fauna}>
              <UserContext.Provider value={identity}>                                          
                <span>GitHub Avatar{avatar_url && <img alt={name} src={avatar_url} className='user-icon' />}</span>
                <span>&nbsp;Global Avatar <UserAvatar size='24' className='user-icon' name={name} src={avatar_url} />
                  <h3>&nbsp; Hello {name}!</h3>
                  <span>{email}</span>
                </span> 
              </UserContext.Provider> 
            </FaunaContext.Provider>                            
          </>            
          </div>
          <hr
            style={{
              marginBottom: 2,
            }}
          />        
      </div>
    </>
  )
}

export default Details
