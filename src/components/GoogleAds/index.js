import React, { Component  } from 'react'
import { AdsDiv } from './styles'

class GoogleAds extends Component {

    componentDidMount() {
      const installGoogleAds = () => {
        const elem = document.createElement("script");
        elem.src =
          "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";
        elem.async = true;
        elem.defer = true;
        document.body.insertBefore(elem, document.body.firstChild);
      };
      installGoogleAds();
      (adsbygoogle = window.adsbygoogle || []).push({});
    }

   render () {
    return(
      <AdsDiv role='note' className='add-block clear-both' itemScope='itemScope' itemType='https://schema.org/WPAdBlock'>
        <div class="center">
          <ins className = "adsbygoogle"
            style = { {display:"block"} }
            data-ad-client="ca-pub-7655495105068461"
            data-ad-slot="5601054893"
            data-ad-format="auto" />
        </div>
      </AdsDiv>
    )
  }
}

export default GoogleAds
