import styled from 'styled-components'
import mediaQuery from '../../utils/mediaQuery'

export const AdsDiv = styled.div`
z-index: 3;
margin: .2em;
min-width: 50vw;
overscroll-behavior: none;
${mediaQuery.maxPhablet} {
    bottom: 1em;
    maxWidth: 80vw;
    transition: 0.3s;
  }
  ${mediaQuery.minPhablet} {
    top: 2em;
    width: 60vw;
  }
`
