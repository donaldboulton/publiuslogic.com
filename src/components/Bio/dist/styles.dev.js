"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetaBio = exports.ImageWrapper = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Rainbow = _interopRequireDefault(require("../Rainbow"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-wrap: wrap;\n  > * {\n    display: flex;\n    align-items: left;\n  }\n  > :not(:last-child) {\n    margin-right: 1em;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  marginRight: 1,\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ImageWrapper = (0, _styledComponents["default"])(_Rainbow["default"])(_templateObject());
exports.ImageWrapper = ImageWrapper;

var MetaBio = _styledComponents["default"].div(_templateObject2());

exports.MetaBio = MetaBio;