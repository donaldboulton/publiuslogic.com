import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Applause from '../ApplauseButton'
import Image from 'gatsby-image'
import { OutboundLink } from "gatsby-plugin-google-gtag"
import BioTypist from './BioTypist'
import { ImageWrapper, MetaBio } from './styles'

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      avatar: file(absolutePath: { regex: "/donald-boulton.jpg/" }) {
        childImageSharp {
          fixed(width: 50, height: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }      
      site {
        siteMetadata {
          author
          siteUrl
          gitHubEditMe
        }
      }
      markdownRemark {
        frontmatter {
          path
        }
      }
    }
  `)
  const { author } = data.site.siteMetadata
  
  return (
    <>
      <section
        style={{
          justifyItems: `center`,
          justifyContent: `center`,
          alignItems: `center`,
        }}
      >
        <div
          style={{
            display: `inline-block`,
            verticalAlign: `top`,
          }}
        >    
             <ImageWrapper>    
                <Image
                  fixed={data.avatar.childImageSharp.fixed}
                  alt={author}
                  style={{
                    minWidth: `50px`,
                    padding: `1em`,
                    borderRadius: `100%`,
                  }}
                />                
              </ImageWrapper>            
            </div> 
          <div
            style={{
              display: `inline-block`,
            }}
            >          
            <MetaBio>
              <span>
                <span>
                  Full Stack Developed by: <strong>{author}</strong>
                  {` `}
                </span>
              </span> 
              <span>                
                  Follow&nbsp;<OutboundLink className='a' href={`https://twitter.com/donboulton`} target='_blank' rel='noopener noreferrer'>Don</OutboundLink>&nbsp;on Twitter
              <span>
                <Applause />
              </span>
            </span>
          </MetaBio>
          <div>
            <BioTypist />            
          </div>
        </div>  
      </section>       
    </>
  )
}

export default Bio
