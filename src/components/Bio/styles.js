import styled from 'styled-components'
import Rainbow from '../Rainbow'

export const ImageWrapper = styled(Rainbow)`
  marginRight: 1,
`;

export const MetaBio = styled.div`
  display: flex;
  flex-wrap: wrap;
  > * {
    display: flex;
    align-items: left;
  }
  > :not(:last-child) {
    margin-right: 1em;
  }
`