export const SPRING_WIDTH = 40;
export const SPRING_HEIGHT = 220;
export const SPRING_STEM_HEIGHT = 10;
export const SPRING_STRETCH_LIMIT = 420;

export const ENVIRONMENT_HEIGHT = 540;
export const ENVIRONMENT_WIDTH = 245;

export const MASS_MIN = 0.5;
export const MASS_STEP = 1.25;
export const MASS_VALUES = [
  MASS_MIN,
  MASS_MIN + MASS_STEP,
  MASS_MIN + MASS_STEP * 2,
];
export const MASS_DEFAULT = MASS_VALUES[1];
export const MASS_MAX = MASS_VALUES[2];

export const TENSION_MIN = 30;
export const TENSION_MAX = 400;
export const TENSION_DEFAULT = 200;

export const FRICTION_MIN = 0;
export const FRICTION_MAX = 35;
export const FRICTION_DEFAULT = 12;
