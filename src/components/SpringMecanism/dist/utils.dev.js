"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.requestAnimationFramePromise = requestAnimationFramePromise;
exports.setTimeoutPromise = setTimeoutPromise;
exports.hash = hash;
exports.splitCommaSeparatedArray = exports.convertRadiansToDegrees = exports.getDistanceBetweenPoints = exports.normalize = exports.generateId = exports.getTimeOfDay = exports.delay = exports.hyphenate = exports.convertHexToRGBA = exports.deleteCookie = exports.capitalizeSentence = exports.capitalize = exports.findRight = exports.mergeUnique = exports.toggleInArray = exports.convertArrayToMap = exports.omit = exports.pick = exports.camelToDashCase = exports.getInterpolatedValue = exports.sortBy = exports.isEmpty = exports.slugify = exports.throttle = exports.debounce = exports.roundTo = exports.clamp = exports.mean = exports.sum = exports.random = exports.sample = exports.sampleOne = exports.range = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// TODO: Modernize

/* eslint-disable */
var range = function range(start, end) {
  var step = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  var output = [];

  if (typeof end === 'undefined') {
    end = start;
    start = 0;
  }

  for (var i = start; i < end; i += step) {
    output.push(i);
  }

  return output;
};
/* eslint-enable */


exports.range = range;

var sampleOne = function sampleOne(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
};

exports.sampleOne = sampleOne;

var sample = function sample(arr) {
  var len = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var output = [];

  for (var i = 0; i < len; i++) {
    output.push(sampleOne(arr));
  }

  return output;
};

exports.sample = sample;

var random = function random(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

exports.random = random;

var sum = function sum(values) {
  return values.reduce(function (sum, value) {
    return sum + value;
  }, 0);
};

exports.sum = sum;

var mean = function mean(values) {
  return sum(values) / values.length;
};

exports.mean = mean;

var clamp = function clamp(val) {
  var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  return Math.max(min, Math.min(max, val));
};

exports.clamp = clamp;

var roundTo = function roundTo(number) {
  var places = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return Math.round(number * Math.pow(10, places)) / Math.pow(10, places);
};

exports.roundTo = roundTo;

var debounce = function debounce(callback, wait) {
  var timeoutId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  return function () {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    window.clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
      callback.apply(null, args);
    }, wait);
  };
};

exports.debounce = debounce;

var throttle = function throttle(func, limit) {
  var lastFunc;
  var lastRan;
  return function () {
    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    if (!lastRan) {
      func.apply(null, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function () {
        if (Date.now() - lastRan >= limit) {
          func.apply(null, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
};

exports.throttle = throttle;

var slugify = function slugify() {
  var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var slug = str.toLowerCase().replace(/\s/g, '-').replace(/[^a-zA-Z0-9-]/g, ''); // If the value starts with a number, swap it out!
  // Doing this in a dumb way for now.

  if (slug.match(/^[\d]{1,2}-/)) {
    slug = slug.replace(/^[\d]{1,2}-/, 'digit');
  }

  return slug;
};

exports.slugify = slugify;

var isEmpty = function isEmpty(obj) {
  return Object.keys(obj).length === 0;
};

exports.isEmpty = isEmpty;

var sortBy = function sortBy(arr, key) {
  var direction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'asc';
  var comparator = arguments.length > 3 ? arguments[3] : undefined;

  var comparatorToUse = comparator || function (a, b) {
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    } else {
      return 0;
    }
  };

  return arr.sort(function (a, b) {
    return comparatorToUse(sortBy === 'asc' ? a[key] : b[key], sortBy === 'asc' ? b[key] : a[key]);
  });
};

exports.sortBy = sortBy;

var getInterpolatedValue = function getInterpolatedValue(y1, y2, ratio) {
  // We're assuming that `ratio` is a value between 0 and 1.
  // If this were a graph, it'd be our `x`, and we're trying to solve for `y`.
  // First, find the slope of our line.
  var slope = y2 - y1;
  return slope * ratio + y1;
};

exports.getInterpolatedValue = getInterpolatedValue;

var camelToDashCase = function camelToDashCase(str) {
  return str.replace(/[A-Z0-9]/g, function (letter) {
    return "-".concat(letter.toLowerCase());
  });
};

exports.camelToDashCase = camelToDashCase;

var pick = function pick(obj, keys) {
  var o = {};
  var i = 0;
  var key;
  keys = Array.isArray(keys) ? keys : [keys];

  while (key = keys[i++]) {
    if (typeof obj[key] !== 'undefined') {
      o[key] = obj[key];
    }
  }

  return o;
};

exports.pick = pick;

var omit = function omit(obj, key) {
  var newObj = {};

  for (var name in obj) {
    if (name !== key) {
      newObj[name] = obj[name];
    }
  }

  return newObj;
};

exports.omit = omit;

var convertArrayToMap = function convertArrayToMap(list) {
  return list.reduce(function (acc, item) {
    return _objectSpread({}, acc, _defineProperty({}, item.id, item));
  }, {});
}; // Either removes or adds an item to an array
// EXAMPLE: toggleInArray([1, 2], 3) -> [1, 2, 3]
// EXAMPLE: toggleInArray([1, 2], 2) -> [1]


exports.convertArrayToMap = convertArrayToMap;

var toggleInArray = function toggleInArray(arr, item) {
  return arr.includes(item) ? arr.filter(function (i) {
    return i !== item;
  }) : [].concat(_toConsumableArray(arr), [item]);
}; // Combines 2 arrays, removing duplicates.
// EXAMPLE: mergeUnique([1, 2], [2, 3]) -> [1, 2, 3]


exports.toggleInArray = toggleInArray;

var mergeUnique = function mergeUnique(arr1, arr2) {
  return arr1.concat(arr2.filter(function (item) {
    return arr1.indexOf(item) === -1;
  }));
};

exports.mergeUnique = mergeUnique;

var findRight = function findRight(arr, predicate) {
  return arr.slice().reverse().find(predicate);
};

exports.findRight = findRight;

function requestAnimationFramePromise() {
  return new Promise(function (resolve) {
    return window.requestAnimationFrame(resolve);
  });
}

function setTimeoutPromise(duration) {
  return new Promise(function (resolve) {
    return window.setTimeout(resolve, duration);
  });
}

var capitalize = function capitalize(str) {
  return str[0].toUpperCase() + str.slice(1);
};

exports.capitalize = capitalize;

var capitalizeSentence = function capitalizeSentence(str) {
  return str.split(' ').map(function (word) {
    return word[0].toUpperCase() + word.slice(1);
  }).join(' ');
};

exports.capitalizeSentence = capitalizeSentence;

var deleteCookie = function deleteCookie(key) {
  document.cookie = "".concat(encodeURIComponent(key), "=; expires=Thu, 01 Jan 1970 00:00:00 GMT");
};

exports.deleteCookie = deleteCookie;

var convertHexToRGBA = function convertHexToRGBA(hex) {
  var alpha = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var r = parseInt(hex.slice(1, 3), 16);
  var g = parseInt(hex.slice(3, 5), 16);
  var b = parseInt(hex.slice(5, 7), 16);
  return "rgba(".concat(r, ", ").concat(g, ", ").concat(b, ", ").concat(alpha, ")");
};

exports.convertHexToRGBA = convertHexToRGBA;

var hyphenate = function hyphenate(str) {
  return str.replace(/([A-Z])/g, '-$1').toLowerCase();
};

exports.hyphenate = hyphenate;

var delay = function delay(duration) {
  return new Promise(function (resolve) {
    return window.setTimeout(resolve, duration);
  });
};

exports.delay = delay;

var getTimeOfDay = function getTimeOfDay() {
  var now = new Date();
  var hourOfDay = now.getHours();

  if (hourOfDay <= 4) {
    return 'night';
  } else if (hourOfDay <= 11) {
    return 'morning';
  } else if (hourOfDay <= 17) {
    return 'afternoon';
  } else if (hourOfDay <= 21) {
    return 'evening';
  } else {
    return 'night';
  }
};

exports.getTimeOfDay = getTimeOfDay;

var generateId = function generateId() {
  var len = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 4;
  // prettier-ignore
  var characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  return sample(characters, len).join('');
};

exports.generateId = generateId;

var normalize = function normalize(number, currentScaleMin, currentScaleMax) {
  var newScaleMin = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
  var newScaleMax = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1;
  // FIrst, normalize the value between 0 and 1.
  var standardNormalization = (number - currentScaleMin) / (currentScaleMax - currentScaleMin); // Next, transpose that value to our desired scale.

  return (newScaleMax - newScaleMin) * standardNormalization + newScaleMin;
};

exports.normalize = normalize;

var getDistanceBetweenPoints = function getDistanceBetweenPoints(p1, p2) {
  var deltaX = Math.abs(p2.x - p1.x);
  var deltaY = Math.abs(p2.y - p1.y);
  return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
};

exports.getDistanceBetweenPoints = getDistanceBetweenPoints;

var convertRadiansToDegrees = function convertRadiansToDegrees(angle) {
  return angle * 180 / Math.PI;
};
/**
 * input:  "js,cat cat,  bee, dog"
 * output: ['js', 'cat cat', 'bee', 'dog']
 */


exports.convertRadiansToDegrees = convertRadiansToDegrees;

var splitCommaSeparatedArray = function splitCommaSeparatedArray(str) {
  return str.replace(/,\s+/g, ',').split(',');
};

exports.splitCommaSeparatedArray = splitCommaSeparatedArray;

function hash(val) {
  var hash = 0,
      i,
      chr;
  if (val.length === 0) return hash;

  for (i = 0; i < val.length; i++) {
    chr = val.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }

  return hash;
}