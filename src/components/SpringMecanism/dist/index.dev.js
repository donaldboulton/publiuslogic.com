"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calculatePoints = calculatePoints;
exports.renderToCanvas = renderToCanvas;

var _utils = require("./utils");

var _constants = require("./constants");

var SIN_CYCLE_LENGTH = Math.PI * 2;

var getNumOfWraps = function getNumOfWraps(tension) {
  return Math.round((0, _utils.normalize)(tension, 30, 400, 2, 18));
}; // const loadedCharacterImages = {};


function calculatePoints(_ref) {
  var width = _ref.width,
      defaultHeight = _ref.defaultHeight,
      tension = _ref.tension,
      dragY = _ref.dragY;
  var effectiveHeight = _constants.SPRING_HEIGHT - _constants.SPRING_STEM_HEIGHT + dragY;
  var numOfWraps = getNumOfWraps(tension);
  return (0, _utils.range)(effectiveHeight).map(function (i) {
    var ratio = i / effectiveHeight;
    var totalLength = SIN_CYCLE_LENGTH * numOfWraps; // the progress is through the given cycle, but we may be rendering
    // multiple cycles.

    var progressThroughDrawableArea = ratio * (1 / numOfWraps); // Right now, `progress` ranges from 0 to 100.
    // Normalize this value to fit between 0 and `totalLength`.
    // Just cross-multiplying to get the normalized value:
    //
    // progress         positionInRads
    // --------  =      --------------
    //   1             totalLength
    //
    // prettier-ignore

    var positionInRads = ratio * totalLength; // `sinValue` is a number from -1 to 1 representing where on the sin
    // wave this point is.
    // We need to transform that to a value that fits our given canvas.

    var sinValue = Math.sin(positionInRads);
    var x = (0, _utils.normalize)(sinValue, -1, 1, 2, _constants.SPRING_WIDTH - 2);
    return {
      x: x,
      // We want to draw 1 point per Y-axis point, so our Y position is
      // equal to the array index (since we're iterating from 0-height).
      y: i
    };
  });
}

function renderToCanvas(_ref2) {
  var ctx = _ref2.ctx,
      points = _ref2.points,
      springColor = _ref2.springColor;
  var _ctx$canvas = ctx.canvas,
      width = _ctx$canvas.width,
      height = _ctx$canvas.height;
  ctx.clearRect(0, 0, width, height);
  ctx.beginPath();
  points.forEach(function (point, index) {
    // Tackle the initial 10px stem at the top
    if (index === 0) {
      ctx.moveTo(point.x, 0);
      ctx.lineTo(point.x, _constants.SPRING_STEM_HEIGHT);
    }

    ctx.lineTo(point.x, point.y + _constants.SPRING_STEM_HEIGHT); // Tackle the bottom 10px stem

    if (index === points.length - 1) {
      ctx.lineTo(points[0].x, point.y + _constants.SPRING_STEM_HEIGHT);
      ctx.lineTo(points[0].x, point.y + _constants.SPRING_STEM_HEIGHT * 2);
    }
  });
  ctx.strokeStyle = springColor;
  ctx.lineWidth = 2;
  ctx.stroke();
  ctx.closePath();
}