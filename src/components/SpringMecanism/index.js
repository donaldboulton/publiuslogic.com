import { range, normalize } from './utils';

import { SPRING_WIDTH, SPRING_HEIGHT, SPRING_STEM_HEIGHT } from './constants';

const SIN_CYCLE_LENGTH = Math.PI * 2;

const getNumOfWraps = tension => Math.round(normalize(tension, 30, 400, 2, 18));

// const loadedCharacterImages = {};

export function calculatePoints({ width, defaultHeight, tension, dragY }) {
  const effectiveHeight = SPRING_HEIGHT - SPRING_STEM_HEIGHT + dragY;

  const numOfWraps = getNumOfWraps(tension);

  return range(effectiveHeight).map(i => {
    const ratio = i / effectiveHeight;

    const totalLength = SIN_CYCLE_LENGTH * numOfWraps;

    // the progress is through the given cycle, but we may be rendering
    // multiple cycles.
    const progressThroughDrawableArea = ratio * (1 / numOfWraps);

    // Right now, `progress` ranges from 0 to 100.
    // Normalize this value to fit between 0 and `totalLength`.
    // Just cross-multiplying to get the normalized value:
    //
    // progress         positionInRads
    // --------  =      --------------
    //   1             totalLength
    //
    // prettier-ignore
    const positionInRads = (ratio * totalLength);

    // `sinValue` is a number from -1 to 1 representing where on the sin
    // wave this point is.
    // We need to transform that to a value that fits our given canvas.
    const sinValue = Math.sin(positionInRads);
    const x = normalize(sinValue, -1, 1, 2, SPRING_WIDTH - 2);

    return {
      x,
      // We want to draw 1 point per Y-axis point, so our Y position is
      // equal to the array index (since we're iterating from 0-height).
      y: i,
    };
  });
}

export function renderToCanvas({ ctx, points, springColor }) {
  const { width, height } = ctx.canvas;
  ctx.clearRect(0, 0, width, height);

  ctx.beginPath();
  points.forEach((point, index) => {
    // Tackle the initial 10px stem at the top
    if (index === 0) {
      ctx.moveTo(point.x, 0);
      ctx.lineTo(point.x, SPRING_STEM_HEIGHT);
    }

    ctx.lineTo(point.x, point.y + SPRING_STEM_HEIGHT);

    // Tackle the bottom 10px stem
    if (index === points.length - 1) {
      ctx.lineTo(points[0].x, point.y + SPRING_STEM_HEIGHT);
      ctx.lineTo(points[0].x, point.y + SPRING_STEM_HEIGHT * 2);
    }
  });

  ctx.strokeStyle = springColor;
  ctx.lineWidth = 2;

  ctx.stroke();
  ctx.closePath();
}

const Springy = () => {
    return (
      <>
        <div style="min-height: 540px;">
            <div className="Environment__Wrapper-y92rgf-0 gipxex" style="width: 245px; height: 540px;">
                <div className="Environment__PlankWrapper-y92rgf-3 iiKBmH">
                    <img src="/img/plank-and-clamp.png" className="Environment__PlankAndClampImg-y92rgf-1 dQvuWk" />
                </div>
                <div className="Environment__Children-y92rgf-5 dNPUwm">
                    <div className="SpringMechanism__Wrapper-r7esla-0 eaHkvr">
                        <div className="SpringCanvas__Wrapper-sc-1gyuei8-0 fIGfFH">
                          <canvas width="80.00000238418579" height="840.0000250339508" className="SpringCanvas__Canvas-sc-1gyuei8-1 gYranv" style="width: 40px; height: 420px; display: block;"></canvas>
                          <Spacer size="220" className="spacer" />
                        </div>
                        <Spacer size='8' className="spacer" />
                        <button className="UnstyledButton-sc-1qlpyz4-0 SpringMechanism__Weight-r7esla-1 gmEakr" style="transform: translate(-2px, 0px);">
                            <img draggable="false" src="/img/midguy-default.png" alt="A normal-sized smiling weight" className="Character__CharacterImage-sc-1tmgd75-0 ioDDHd" style="width: 142px;" />
                        </button>
                    </div>
                </div>
                <div className="earth-background">
                    <img src="/img/cloud-1.png" className="EarthBackground__CloudImg-sc-4lrdie-1 gpooao" style="width: 150px; transform: translate(286.983px, 189px);" />
                    <img src="/img/cloud-2.png" className="EarthBackground__CloudImg-sc-4lrdie-1 gpooao" style="width: 160px; transform: translate(-132.896px, 378px);" />
                    <img src="/img/birds.png" className="EarthBackground__Birds-sc-4lrdie-2 dafZnB" />
                </div>
            </div>
        </div>
      </>
    )
  }
  
  export default Springy
