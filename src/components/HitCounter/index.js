import React from 'react'
import RetroHitCounter from 'react-retro-hit-counter'
import fetch from 'isomorphic-fetch'

const { faunadb, q } = require('faunadb')

const apiKey = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET
}) 

const query = q.Get(q.Match(q.Index('article'), id));
// eslint-disable-next-line valid-typeof
const url = typeof window !== 'undefined' ? window.location.href : ''
const HEADERS = {
  headers: {
    Accept: "application/json",
    Authorization: `Bearer ${apiKey}`,
    "Content-Type": "application/json",
  }
}

const Wrapper = props => <span style={{ position: 'relative' }} {...props} />

const HitWrapper = props => (
  <span style={{ position: 'absolute', top: 0, right: 0 }} {...props} />
)

const HitCounter = ({ props }) => {
  const [hits, setHits] = React.useState(undefined)
  // eslint-disable-next-line valid-typeof
  React.useEffect(() => {
    // Don't count hits on localhost
    if (process.env.NODE_ENV !== 'production') {
      return;
    }
    // Invoke the function by making a request.
    // Update the URL to match the format of your platform.
    fetch(`/.netlify/functions/HitCounter/register-hit?slug=${slug}`)
    HEADERS  
    response => response.json()
      .then((json) => {
        if (typeof json.hits === 'number') {
          setHits(json.hits);
        }
      });
  }, [url]);
  if (typeof hits === 'undefined') {
    return null;
  }
  return  <Wrapper><span
  style={{
    cursor: 'pointer',
    padding: '1em',
  }}>
   <HitWrapper>
      <RetroHitCounter withBorder={false} hits={hits}
        {...props}
      />
    </HitWrapper>
    </span>
  </Wrapper>
}

export default HitCounter