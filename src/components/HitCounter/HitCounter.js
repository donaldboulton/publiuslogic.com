import React, { useState, useEffect } from 'react'
import RetroHitCounter from 'react-retro-hit-counter'
import styled from 'styled-components'
import fetch from 'isomorphic-fetch'

const Wrapper = styled.span`
  position: relative;
`

const HitWrapper = styled.span`
  position: absolute; 
  top: 0; 
  right: 0;
`

const url = typeof window !== 'undefined' ? window.location.href : ''

function HitCounter() {
  const [hits, setHits] = useState(undefined);
  useEffect(() => {
    // Don't count hits on localhost
    if (process.env.NODE_ENV !== 'production') {
      return;
    }
    // Invoke the function by making a request.
    // Update the URL to match the format of your platform.
    fetch(`/.netlify/functions/register-hit?url=${url}`)
      .then((res) => res.json())
      .then((json) => {
        if (typeof json.hits === 'number') {
          setHits(json.hits);
        }
      });
  }, [url]);
  if (typeof hits === 'undefined') {
    return null;
  }
  return <Wrapper><span
  style={{
    cursor: 'pointer',
    padding: '1em',
  }}>
   <HitWrapper>
      <RetroHitCounter withBorder={false} hits={hits}
      />
    </HitWrapper>
    </span>
  </Wrapper>
}

export default HitCounter