import React from 'react'
import styled from 'styled-components'
import MagicRainbowButton from '../MagicRainbowButton'
import useSound from 'use-sound'
import beepBoop from '../../../static/audio/beep-boop.mp3'

const EmailButton = styled(MagicRainbowButton)`
  padding: 1em;
  margin: 1em 1em 1em 2em;
  border-radius: 6px;
`

const FanfareButton = () => {
  const [play, { stop }] = useSound(beepBoop)

  return (
    <EmailButton 
      id='request-email'   
      onMouseEnter={play} 
      onMouseLeave={stop}
    >
      <span role="button" aria-label="trumpet">
        <a href="mailto:donaldboulton@gmail.com" rel="noopener noreferrer" target="_blank"><span role="img" aria-labelledby='Send Mail'> 📧</span> Email Contact</a>
      </span>
    </EmailButton>
  )
}

export default FanfareButton