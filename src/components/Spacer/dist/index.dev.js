"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _Spacer["default"];
  }
});

var _Spacer = _interopRequireDefault(require("./Spacer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }