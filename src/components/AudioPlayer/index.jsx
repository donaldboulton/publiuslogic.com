import React from 'react'
import { Link } from 'gatsby'
import ReactAudioPlayer from "react-audio-player";
import moses from '../../../static/audio/book-of-moses.mp3'
import styled from 'styled-components'
import { FilePdf } from '@styled-icons/fa-regular/'
import { HeadphonesAlt } from '@styled-icons/fa-solid/'
import mediaQuery from './../../utils/mediaQuery'

const FooterContainer = styled.div`
  padding: 2vh 2vw;
  color: white;
  display: grid;
  justify-items: center;
  align-items: center;
  grid-template-areas:
    'copyright'
    'source'
    'poweredBy';
  grid-gap: 2vh 2vw;
  ${mediaQuery.minPhone} {
    grid-template-areas:
      'copyright source'
      'poweredBy poweredBy';
  }
  ${mediaQuery.minTablet} {
    grid-template-areas: 'copyright source poweredBy';
  }
`

export const PoweredBy = styled.div`
  grid-area: poweredBy;
  display: grid;
  grid-auto-flow: column;
  a {
    height: 1.5em;
    width: 1.5em;
  }
`

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

const alerting = () => {
  alert("test");
};

const PlayerPdf = styled(FilePdf)`
  width: 0.9em;
  margin-right: 0.2em;
  color: white;
`

const AudioIcon = styled(HeadphonesAlt)`
  width: 0.9em;
  margin-right: 0.2em;
  color: white;
`

function AudioPlayer() {    
    return (
      <div className='box'>
        <div style={styles}>
          <h2 onClick={alerting}>Book of Moses</h2>
          <ReactAudioPlayer
             src={moses}
             autoPlay
             controls
          />
        </div>
        <FooterContainer className='footer-background'>
          <span
            css='grid-area: copyright;'
          >
            <Link className='a' title='Download Audio' href='https://res.cloudinary.com/mansbooks/video/upload/v1625261530/audio/book-of-moses.mp3' rel='noopener noreferrer'>
              <AudioIcon size='1em' /> Download Audio
            </Link>
          </span>
          <span
            css='grid-area: source;'
          >
            React <a className='a' rel='me noopener noreferrer' target='_blank' href='https://github.com/justinmc/react-audio-player#readme'>Audio Player</a>
          </span> 
          <span
            css='grid-area: poweredBy;'
          >        
            <Link className='a' title='Download Audio' href='https://publiuslogic-old.netlify.app/static/pdf/book-of-moses-essays-001.pdf' rel='noopener noreferrer'>
              <PlayerPdf size='1em' /> Download Pdf
            </Link>
          </span>
        </FooterContainer>
      </div>
    );
  }

  export default AudioPlayer