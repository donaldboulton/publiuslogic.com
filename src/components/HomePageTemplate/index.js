import React from 'react'
import PropTypes from 'prop-types'

const HomePageTemplate = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <HomePageTemplate
      image={frontmatter.image}
      content={postNode.html}
      contentComponent={HTMLContent}
      date={post.frontmatter.date}
      title={post.frontmatter.title}
      cover={post.frontmatter.cover}
      meta_title={post.frontmatter.meta_title}
      meta_description={post.frontmatter.meta_description}
      videoUrl={post.frontmatter.videoUrl}
      videTitle={post.frontmatter.videoTitle}
      videoPoster={post.frontmatter.videoPoster}
      tags={post.frontmatter.tags}
      showToc={post.frontmatter.showToc}
      showCategory={post.frontmatter.showCategory}
      showTags={post.frontmatter.showTags}
      showHotJar={post.frontmatter.showHotJar}
      showStack={post.frontmatter.showStack}
      showVideo={post.frontmatter.showVideo}
      showLogRocket={post.frontmatter.showLogRocket}
    />
  )
}

HomePageTemplate.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default HomePageTemplate
