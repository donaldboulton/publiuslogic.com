import React from 'react'
import FoxNews from '../FoxNews'
import OuSoftball from '../OuSoftball'
import CssTricksAds from '../CssTricksAds'
import { AdsContainer, CssTricksDiv, SlackDiv, SubscriptionsDiv } from './styles'

const AdsGroup = () => {
  return (
    <AdsContainer css='grid-area: ads;'>
      <SlackDiv>
        <OuSoftball />
      </SlackDiv>
      <SubscriptionsDiv>
        <CssTricksAds />
      </SubscriptionsDiv>
      <CssTricksDiv>
        <FoxNews />
      </CssTricksDiv>
    </AdsContainer>
  )
}

export default AdsGroup

