import React from 'react'
import PropTypes from 'prop-types'
import Content from '../Content'

const PostsPageTemplate = ({
  content,
  contentComponent,
}) => {
  const PostContent = contentComponent || Content

  return (
    <div>      
      <PostContent content={content} />
    </div>
  )
}

PostsPageTemplate.propTypes = {
  content: PropTypes.string,
  contentComponent: PropTypes.func,
}

export default PostsPageTemplate

