import styled, { css } from 'styled-components'
import { Calendar } from '@styled-icons/octicons/'
import { Close as Cross } from '@styled-icons/material/Close'
import mediaQuery from '../../utils/mediaQuery'

const openSortDateDiv = css`
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
  padding: 0.5em 0.5em;
  border-radius: 0.5em;
  box-shadow: 0 0 1em rgba(0, 0, 0, 0.5);
  border: 1px solid ${props => props.theme.black};
`

export const SortDateDiv = styled.div`
  grid-column-start: aside-start;
  grid-column-end: aside-end;
  padding: 1vmin;
  height: max-content;
  max-height: 85vh;
  border-radius: 4px;
  z-index: 3;
  line-height: 2em;
  margin: .2em;
  min-width: 20em;
  width: 165px;
  overscroll-behavior: none;
  box-shadow: 0 0 1em rgba(0, 0, 0, 0.5);
  border: thin ${props => props.theme.black};
  overflow-x: hidden;
  overflow-y: hidden;
  ${mediaQuery.maxPhablet} {
    position: fixed;
    bottom: 1em;
    background: ${props => props.theme.black};
    color: ${props => props.theme.white};
    left: 1em;
    ${props => !props.open && `height: 0;`};
    ${props => props.open && openSortDateDiv};
    visibility: ${props => (props.open ? `visible` : `hidden`)};
    opacity: ${props => (props.open ? 1 : 0)};
    transition: 0.3s;
  }
  ${mediaQuery.minPhablet} {
    font-size: 0.85em;
    grid-column: 4 / -1;
    position: -webkit-sticky;
    position: sticky;
    top: 2em;
    background: ${props => props.theme.black};
    color: ${props => props.theme.white};
  }
  nav {
    max-height: 70vh;
    overflow-x: hidden;
  }
`

export const SortDateTitle = styled.h3`
  margin: 1em;
  display: grid;
  grid-auto-flow: column;
  align-items: center;
  grid-template-columns: auto auto 1fr;
  color: ${props => props.theme.white};
  border-bottom: thin ${props => props.theme.lightGray};
`

export const SortDateLink = styled.a`
  color: ${({ theme, active }) => (active ? theme.activeLinks : theme.activeLinks)};
  font-weight: ${props => props.active && `bold`};
  display: block;
  margin-left: ${props => props.depth + `em`};
  border-top: ${props =>
    props.depth === 0 && `1px solid ` + props.theme.white};
`

export const CalendarIcon = styled(Calendar)`
  width: 1.3em;
  height: 1.3em;
  margin-right: 1em;
  color: ${props => props.theme.white};
`

const openedCssD = css`
  position: fixed;
  z-index: 25;
  bottom: calc(19vh + 6em);
  ${mediaQuery.minLaptop} {
    bottom: calc(9vh + 6em);
  }
  left: 0;
  padding: 0.4em 0.5em 0.4em 0.2em;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
  border: 2px solid gray;
  border-radius: 0 50% 50% 0;
  transform: translate(${props => (props.open ? `-100%` : 0)});
`

const closedCssD = css`
  margin-left: 1em;
  border: 1px solid gray;
  border-radius: 50%;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
`

export const SortDateToggle = styled(Cross).attrs(props => ({
  as: props.opener && Calendar,
  size: props.opener ? `2em` : `2.4em`,
}))`
  z-index: 2;
  transition: 0.3s;
  background: ${props => props.theme.black};
  color: ${props => props.theme.white};
  justify-self: end;
  :hover {
    transform: scale(1.1);
  }
  ${mediaQuery.minLaptop} {
    display: none;
  }
  ${props => (props.opener ? openedCssD : closedCssD)};
`