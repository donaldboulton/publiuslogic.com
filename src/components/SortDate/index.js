import React, { useRef, useState } from 'react'
import { useStaticQuery, graphql, Link } from 'gatsby'
import { kebabCase } from 'lodash'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { SortDateTitle, SortDateDiv, SortDateToggle, CalendarIcon } from './styles'
import { TableOfContents } from '../styles/ArticleStyles'

const SortDate = ({ group, ...rest }) => {
  const { categoryTitle = `Posts Sort Date` } = rest
  const [open, setOpen] = useState(false)
  const ref = useRef()
  useOnClickOutside(ref, () => setOpen(false))

  const data = useStaticQuery(graphql`
    {
      allMarkdownRemark(sort: {fields: frontmatter___date, order: DESC}) {
        group(field: frontmatter___date) {
          fieldValue
          totalCount
          edges {
            node {
              frontmatter {
                slug
                title
                date(fromNow: true, difference: "MMM")
              }
            }
          }
        }
      }
    }
 `)
  return (
    <>
      <SortDateToggle opener open={open} onClick={() => setOpen(true)} />
      <SortDateDiv ref={ref} open={open} className='nav-background'>
        <SortDateTitle>
          <CalendarIcon />
          {categoryTitle}
          <SortDateToggle onClick={() => setOpen(false)} />
        </SortDateTitle>
        <TableOfContents>
          <nav className='nav-scroll'>
            <ul className='taglist field is-grouped is-grouped-multiline'>
              {data.allMarkdownRemark.group.map(date => (
                <li className='control menu-item' key={date.fieldValue}>
                  <Link to={`/sort-dates/${kebabCase(date.fieldValue)}/`}>
                    <div className='tags has-addons is-large'>
                      <span aria-label='Date' className='tag is-primary'>{date.fieldValue}</span>
                      <span aria-label='Date Count' className='tag is-dark'>{date.totalCount}</span>
                    </div>
                  </Link>
                </li>
              ))}
            </ul>
          </nav>
        </TableOfContents>
      </SortDateDiv>
    </>
  )
}

export default SortDate

