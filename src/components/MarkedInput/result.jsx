import React, { useContext } from "react"
import styled from "styled-components"
import ReactMarkdown from "react-markdown"
import rehypeRaw from 'rehype-raw'
import rehypeSanitize from 'rehype-sanitize'
import rehypeHighlight from 'rehype-highlight'
import editorContext from "../Context/EditorContext"

const unified = require('unified');
const remarkParse = require('remark-parse');
const remarkRehype = require('remark-rehype');
const rehypePrism = require('@mapbox/rehype-prism');

unified()
  .use(remarkParse)
  .use(remarkRehype)
  .use(rehypePrism)
  .process(/* some markdown */);

const Container = styled.div`
  width: 45%;
  height: 100%;
  padding: 13px;
  font-family: "Lato", sans-serif;
`
const Title = styled.div`
  font-family: Kaushan Script,cursive;
  margin-bottom: 1em;
  padding: 8px 0;
  border-bottom: thin ${props => props.theme.white};
`

const ResultArea = styled.div`
  width: 100%;
  height: 100%;
  border: none;
  font-size: 17px;
`

export function Result(props) {
  const { markdownText } = useContext(editorContext)

  return (
    <Container>
      <Title className='h2'>Converted Text</Title>
      <ResultArea>
        <ReactMarkdown
          className='textarea'
          name='message'
          id='message'
          rehypePlugins={[
            rehypeRaw, 
            rehypeSanitize, 
            rehypeHighlight, 
            rehypePrism
          ]} 
          children={markdownText} />
      </ResultArea>
    </Container>
  )
}
