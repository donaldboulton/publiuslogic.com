import React, { useContext } from "react"
import styled from "styled-components"
import editorContext from "../Context/EditorContext"

const Container = styled.div`
  width: 50%;
  height: 100%;
  padding: 13px;
  border-right: 1.5px solid rgba(15, 15, 15, 0.4);
  font-family: "Lato", sans-serif;
`

const Title = styled.div`
  font-family: Kaushan Script,cursive;
  margin-bottom: 1em;
  padding: 8px 0;
  border-bottom: thin ${props => props.theme.white};
`

const TextArea = styled.textarea`
  width: 95%;
  height: 100%;
  min-height: 200px;
  resize: none;
  border: none;
  outline: none;
  font-size: 17px;
`

export function MarkedInput(props) {
  const { setMarkdownText } = useContext(editorContext);

  const onInputChange = e => {
    const newValue = e.currentTarget.value;
    setMarkdownText(newValue);
  }

  return (
    <Container>
      <Title className='h2'>Markdown Text</Title>
      <TextArea
        onChange={onInputChange}
        className='textarea'
      />
    </Container>
  )
}
