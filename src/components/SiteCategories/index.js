import React, { useRef, useState, useLayoutEffect } from 'react'
import { useStaticQuery, graphql, Link } from 'gatsby'
import { kebabCase } from 'lodash'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { CategoriesTitle, CategoriesDiv, CategoriesToggle, CategoryIcon } from './styles'
import { TableOfContents } from '../styles/ArticleStyles'

function useLockBodyScroll () {
  useLayoutEffect(() => {
    // Get original body overflow
    const originalStyle = window.getComputedStyle(document.body).overflow
    // Prevent scrolling on mount
    document.body.style.overflow = 'hidden'
    // Re-enable scrolling when component unmounts
    return () => document.body.style.overflow = originalStyle
  }, []) // Empty array ensures effect is only run on mount and unmount
}

const SiteCategories = ({ group, ...rest }) => {
  const { categoryTitle = `Posts Categories` } = rest
  const [open, setOpen] = useState(false)
  const ref = useRef()
  useOnClickOutside(ref, () => setOpen(false))
  useLockBodyScroll()
  const data = useStaticQuery(graphql`
      {
        allMarkdownRemark {
          group(field: frontmatter___category) {
            fieldValue
            totalCount
          }
        }
      }
    `)
  return (
    <>
      <CategoriesToggle opener open={open} onClick={() => setOpen(true)} />
      <CategoriesDiv ref={ref} open={open} className='nav-background' onScroll={e => e.preventDefault()}>
        <CategoriesTitle>
          <CategoryIcon />
          {categoryTitle}
          <CategoriesToggle onClick={() => setOpen(false)} />
        </CategoriesTitle>
        <TableOfContents>
          <nav className='nav-scroll'>
            <ul className='taglist field is-grouped is-grouped-multiline'>
              {data.allMarkdownRemark.group.map(category => (
                <li className='control menu-item' key={category.fieldValue}>
                  <Link to={`/categories/${kebabCase(category.fieldValue)}/`}>
                    <div className='tags has-addons is-large'>
                      <span aria-label='Tag' className='tag is-primary'>{category.fieldValue}</span>
                      <span aria-label='Tag Count' className='tag is-dark'>{category.totalCount}</span>
                    </div>
                  </Link>
                </li>
              ))}
            </ul>
          </nav>
        </TableOfContents>
      </CategoriesDiv>
    </>
  )
}

export default SiteCategories

