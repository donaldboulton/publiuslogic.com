import React from 'react'
import styled from 'styled-components'

const CenterSpan = styled.span`
display: flex;
flex-direction: row;
flex-wrap: wrap;
justify-content: center;
align-content: center;
align-items: center;
margin-top: 0.3em;
font-size: 28px;
font-family: kaushan-script;
text-transform: uppercase;
z-index: 22;
background-position: 50% 50%;
text-align: center;
justify-content: center;
align-content: center;
align-items: center;
-webkit-box-pack: center;
background: radial-gradient(
  circle farthest-corner at center center,
  #8e0436,
  #d64000
) no-repeat;
-webkit-background-clip: text;
-webkit-text-fill-color: transparent;
`

const CenterStyledSpan = ({ children, ...delegated }) => {
  return (
    <>
      <CenterSpan {...delegated}>
        {children}
      </CenterSpan>
    </>
  )
}

export default CenterStyledSpan