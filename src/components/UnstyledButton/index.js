import styled from 'styled-components'

export const UnstyledButton = styled.span`
  display: block;
  margin: 0;
  padding: 0;
  border: none;
  background: transparent;
`;
