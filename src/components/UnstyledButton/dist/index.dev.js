"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UnstyledButton = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: block;\n  margin: 0;\n  padding: 0;\n  border: none;\n  background: transparent;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var UnstyledButton = _styledComponents["default"].span(_templateObject());

exports.UnstyledButton = UnstyledButton;