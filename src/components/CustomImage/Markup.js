function createMarkup() {
    return {__html: 'First · Second'};
  }

  function MarkupComponent() {
    return <div dangerouslySetInnerHTML={createMarkup()} />;
  }

  export default MarkupComponent