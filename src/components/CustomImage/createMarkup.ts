type CreateMarkup = (args: CreateMarkupArgs, options?: MarkupOptions) => string;

interface CreateMarkupArgs {
  sharpMethod: SharpMethod;
  originSrc: string;
  title?: string;
  alt?: string;

  aspectRatio: number;
  src: string;
  srcSet?: string;
  srcWebp?: string;
  srcSetWebp?: string;
  base64?: string;
  tracedSVG?: string;
  
  // fixed, resize
  width?: number;
  height?: number;

  // fluid
  presentationHeight?: number;
  presentationWidth?: number;
  sizes?: string;
  originalImg?: string;
}

interface MarkupOptions {
  loading: 'lazy' | 'eager' | 'auto';
  linkImagesToOriginal: boolean;
  showCaptions: boolean;
  wrapperStyle: string | Function;
  backgroundColor: string;
  tracedSVG: boolean | Object;
  blurUp: boolean;
}

export {}