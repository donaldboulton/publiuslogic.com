"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogRocketToggle = exports.BookIcon = exports.LogRocketIcon = exports.LogRocketLink = exports.LogRocketTitle = exports.Box = exports.LogRocketDiv = void 0;

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _Close = require("@styled-icons/material/Close");

var _Rocket = require("@styled-icons/octicons/Rocket");

var _mediaQueries = _interopRequireDefault(require("../../utils/mediaQueries"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _templateObject10() {
  var data = _taggedTemplateLiteral(["\n  z-index: 2;\n  transition: 0.3s;\n  background: ", ";\n  color: ", ";\n  justify-self: end;\n  :hover {\n    transform: scale(1.1);\n  }\n  ", " {\n    display: none;\n  }\n  ", ";\n"]);

  _templateObject10 = function _templateObject10() {
    return data;
  };

  return data;
}

function _templateObject9() {
  var data = _taggedTemplateLiteral(["\n  font-size: 1em;\n  margin-left: 1em;\n  border: 1px solid gray;\n  border-radius: 50%;\n  background: ", ";\n  color: ", ";\n"]);

  _templateObject9 = function _templateObject9() {
    return data;
  };

  return data;
}

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n  position: fixed;\n  z-index: 25;\n  top: calc(11.5vh + 5em);\n  ", " {\n    top: calc(7.5vh + 5em);\n  }\n  left: 0;\n  padding: 0.4em 0.5em 0.4em 0.2em;\n  background: ", ";\n  color: ", ";\n  border: 2px solid gray;\n  border-radius: 0 50% 50% 0;\n  transform: translate(", ");\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  font-size: 2em;\n  width: 2em;\n  height: 2em;\n  margin-right: 0.2em;\n  color: ", ";\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  font-size: 1.1em;\n  width: 1.1em;\n  height: 1.1em;\n  margin-right: 1em;\n  color: ", ";\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n  font-weight: ", ";\n  display: block;\n  margin-left: ", ";\n  border-top: ", ";\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  margin: 1em;\n  display: grid;\n  grid-auto-flow: column;\n  align-items: center;\n  grid-template-columns: auto auto 1fr;\n  color: ", "; \n  border-bottom: thin ", ";\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  padding: 1em;\n  margin: 1.1em;\n  border-radius: ", ";\n  border: 1px solid ", ";\n  background: radial-gradient(\n    circle closest-corner at center 75px,\n    ", ",\n    ", " 40%,\n  ) no-repeat;\n  text-align: center;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  grid-column-start: aside-start;\n  grid-column-end: aside-end;\n  padding: 1vmin;\n  height: max-content;\n  border-radius: 4px;\n  z-index: 3;\n  line-height: 2em;\n  margin: .2em;\n  min-width: 20em;\n  width: 165px;\n  overscroll-behavior: none;\n  box-shadow: 0 0 1em rgba(0, 0, 0, 0.5);\n  border: thin ", ";\n  overflow-x: hidden;\n  overflow-y: hidden;\n  ", " {\n    position: fixed;\n    bottom: 1em;\n    background: ", ";\n    color: ", ";\n    left: 1em;\n    ", ";\n    ", ";\n    visibility: ", ";\n    opacity: ", ";\n    transition: 0.3s;\n  }\n  ", " {\n    font-size: 0.85em;\n    grid-column: 4 / -1;\n    position: -webkit-sticky;\n    position: sticky;\n    top: 2em;\n    background: ", ";\n    color: ", ";\n  }\n  nav {\n    max-height: 70vh;\n    overflow-x: hidden;\n  }\n  .nav-scroll {\n    overflow-y: auto;\n    scrollbar-color: linear-gradient(to bottom,#201c29,#100e17);\n    scrollbar-width: 10px;\n    overflow-x: hidden;\n    padding-inline-start: 2em !important;\n    margin-top: 1em;\n  }\n  .nav-scroll::-webkit-scrollbar {\n    width: 10px;\n    height: 10px;\n  }\n  .nav-scroll::-webkit-scrollbar-thumb {\n    background: -webkit-gradient(linear,left top,left bottom,from(#d201c29),to(#100e17));\n    background: linear-gradient(to bottom,#201c29,#100e17);\n    border-radius: 10px;\n    -webkit-box-shadow: inset 2px 2px 2px rgba(255,255,255,.25),inset -2px -2px 2px rgba(0,0,0,.25);\n    box-shadow: inset 2px 2px 2px rgba(255,255,255,.25),inset -2px -2px 2px rgba(0,0,0,.25);\n  }\n  .nav-scroll::-webkit-scrollbar-track {\n    background: linear-gradient(to right,#201c29,#201c29 1px,#100e17 1px,#100e17);\n    border-bottom: 10px;\n    border-top: 10px;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background: ", ";\n  color: ", ";\n  padding: 0.7em 1.2em;\n  border-radius: 0.5em;\n  box-shadow: 0 0 1em rgba(0, 0, 0, 0.5);\n  border: 1px solid ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var openLogRocketDiv = (0, _styledComponents.css)(_templateObject(), function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
}, function (props) {
  return props.theme.black;
});

var LogRocketDiv = _styledComponents["default"].div(_templateObject2(), function (props) {
  return props.theme.black;
}, _mediaQueries["default"].maxPhablet, function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
}, function (props) {
  return !props.open && "height: 0;";
}, function (props) {
  return props.open && openLogRocketDiv;
}, function (props) {
  return props.open ? "visible" : "hidden";
}, function (props) {
  return props.open ? 1 : 0;
}, _mediaQueries["default"].minLaptop, function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
});

exports.LogRocketDiv = LogRocketDiv;

var Box = _styledComponents["default"].div(_templateObject3(), function (props) {
  return props.theme.mediumBorderRadius;
}, function (props) {
  return props.theme.darkerGray;
}, function (props) {
  return props.theme.darkestGray;
}, function (props) {
  return props.theme.black;
});

exports.Box = Box;

var LogRocketTitle = _styledComponents["default"].h3(_templateObject4(), function (props) {
  return props.theme.white;
}, function (props) {
  return props.theme.lightGray;
});

exports.LogRocketTitle = LogRocketTitle;

var LogRocketLink = _styledComponents["default"].a(_templateObject5(), function (_ref) {
  var theme = _ref.theme,
      active = _ref.active;
  return active ? theme.activeLinks : theme.activeLinks;
}, function (props) {
  return props.active && "bold";
}, function (props) {
  return props.depth + "em";
}, function (props) {
  return props.depth === 0 && "1px solid " + props.theme.white;
});

exports.LogRocketLink = LogRocketLink;
var LogRocketIcon = (0, _styledComponents["default"])(_Rocket.Rocket)(_templateObject6(), function (props) {
  return props.theme.white;
});
exports.LogRocketIcon = LogRocketIcon;
var BookIcon = (0, _styledComponents["default"])(_Rocket.Rocket)(_templateObject7(), function (props) {
  return props.theme.white;
});
exports.BookIcon = BookIcon;
var openedCss = (0, _styledComponents.css)(_templateObject8(), _mediaQueries["default"].minLaptop, function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
}, function (props) {
  return props.open ? "-100%" : 0;
});
var closedCss = (0, _styledComponents.css)(_templateObject9(), function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
});
var LogRocketToggle = (0, _styledComponents["default"])(_Close.Close).attrs(function (props) {
  return {
    as: props.opener && _Rocket.Rocket,
    size: props.opener ? "2em" : "2.4em"
  };
})(_templateObject10(), function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
}, _mediaQueries["default"].minPhablet, function (props) {
  return props.opener ? openedCss : closedCss;
});
exports.LogRocketToggle = LogRocketToggle;