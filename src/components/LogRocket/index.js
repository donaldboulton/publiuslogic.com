import React, { useRef, useState, useLayoutEffect } from 'react'
import Rocket from '../../../static/logos/logrocket.svg'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { LogRocketTitle, LogRocketDiv, LogRocketToggle, LogRocketIcon, Box } from './styles'
import { TableOfContents } from '../styles/ArticleStyles'

function useLockBodyScroll () {
  useLayoutEffect(() => {
    // Get original body overflow
    const originalStyle = window.getComputedStyle(document.body).overflow
    // Prevent scrolling on mount
    document.body.style.overflow = 'hidden'
    // Re-enable scrolling when component unmounts
    return () => document.body.style.overflow = originalStyle
  }, []) // Empty array ensures effect is only run on mount and unmount
}

const LogRocket = ({ group, ...rest }) => {
  const ref = useRef()
  const { addsTitle = `LogRocket` } = rest
  const [open, setOpen] = useState(false)
  useOnClickOutside(ref, () => open && setOpen(false))
  useLockBodyScroll()

  return (
    <>
      <LogRocketToggle opener open={open} onClick={() => setOpen(true)} />
      <LogRocketDiv ref={ref} open={open} className='nav-background' onScroll={e => e.preventDefault()}>
        <LogRocketTitle>
          <LogRocketIcon />
          {addsTitle}
          <LogRocketToggle onClick={() => setOpen(false)} />
        </LogRocketTitle>
        <TableOfContents>
          <div className='center'>
            <a href='https://logrocket.com/' target='_blank' rel='noopener noreferrer'>
              <img
                src={Rocket}
                alt='LogRocket'
                style={{ width: '140px', height: '79px' }}
              />
            </a>
          </div>
          <div>
            <span>
              <h3 className='center'>Stop guessing why bugs happen.</h3>
              <Box>
                LogRocket lets you replay what users do on your site, helping you reproduce bugs and fix issues faster..
              </Box>
            </span>
          </div>
        </TableOfContents>
      </LogRocketDiv>
    </>
  )
}

export default LogRocket

