import React from 'react'
import { OutboundLink } from "gatsby-plugin-google-gtag"
import foxNews from '../../../static/img/fox-news.webp'

const FoxNews = () => {
  return (
    <>
      <div className='card card3'>
        <div className='message-body'>
          <div>
            <OutboundLink title='Fox News' href='https://nation.foxnews.com/?cmpid=org=NAT::ag=MediaStorm::mc=CPC::src=google::cmp=TuckerCarlson&utm_source=CPC&utm_medium=google&utm_campaign=TuckerCarlson&gclid=CjwKCAjwnPOEBhA0EiwA609ReaDAI89zCHdsX6MG1ksgV2KnxLN1GxUEts80pNbIWTRm3EVIhNmdjhoClwAQAvD_BwE&gclsrc=aw.ds' target='_blank' rel='noopener noreferrer'>
              <img
                className='icon-logo-star'
                src={foxNews}
                alt='Fox News'
              />
            </OutboundLink>
          </div>
          <div>
            <OutboundLink title='Tucker Carlson Today!' href='https://nation.foxnews.com/' target='_blank' rel='noopener noreferrer'>
              <strong>Tucker Carlson Today!</strong>
            </OutboundLink>
          </div>
          <p><h3 className='a'>The Truth In the Latest World and National News.</h3></p>
        </div>
      </div>
    </>
  )
}

export default FoxNews

