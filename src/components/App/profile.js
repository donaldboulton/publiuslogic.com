import React from "react"
import UserAvatar from 'react-user-avatar'
import { useIdentityContext } from 'react-netlify-identity-widget'

function Profile() {
  const identity = useIdentityContext()
  const name =
  (identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.full_name) || 'NoName'
  const email =
  (identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.id) || 'NoEmail'
  const avatar_url = identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.avatar_url
  console.log(JSON.stringify(identity))

  return (              
    <div
      style={{
        marginBottom: 2,
      }}
    >                                                                                                        
      <h3>
        Your Identity Profile
      </h3>
      <div
         style={{
          marginBottom: 2,
        }}
      >
        <span>GitHub Avatar{avatar_url && <img alt={name} src={avatar_url} className='user-icon' />}</span>
        <span>&nbsp;Global Avatar <UserAvatar size='24' className='user-icon' name={name} src={avatar_url} />
          <h3>&nbsp; Hello {name}!</h3>
        </span>
        <br />
        <h4>Email: {email}</h4>
        <br />
      </div>
        <hr
          style={{
            marginBottom: 2,
          }}
        /> 
    </div>           
  )
}

export default Profile