import React from "react"
import styled from 'styled-components'
import axios from "axios"

const Form = styled.form`
  overflow-x: hidden;
  margin: 1em 2em 1em 2em;
`

const Message = styled.textarea`
  padding: 1em;
  background: ${props => props.theme.darkGray};
  border-radius: ${props => props.theme.mediumBorderRadius};
  border: 1px solid ${props => props.theme.darkerGray};
  margin: 1em 1em 1em 2em;
  border-radius: 6px;
  width: 80%;
  height: 180px;
  color: #cccccc;
`

const Submit = styled.button`
  padding: 15px 30px;
  margin: 1em 1em 1em 2em;
  text-transform: uppercase;
  font-weight: bold;
  cursor: pointer;
  border-radius: 6px;
`

const config = {
  mode: "no-cors",
  headers: {
      'Access-Control-Allow-Origin': '*',
      "Content-Type": "application/x-www-form-urlencoded"
  }
}

const NotesForm = ({reloadNotes}) => {
  const [text, setText] = React.useState('')
  const handleSubmit = async event => {
    event.preventDefault();

    if(text === '') return

    await axios.post('/api/create-note', { text }, config)
    .then(response => { 
      console.log(response)
    })
    .catch(error => {
        console.log(error.response)
    });
    setText('')
    reloadNotes();
  } 

  return (
    <div className='footer-background'>
    <Form
      name='note-form'
      onSubmit={handleSubmit}
    >          
      <label htmlFor='textarea' className='label'>Bio</label>
      <br></br>
      <Message
        className='textarea'
        id="textarea"
        value={text}
        onChange={event => setText(event.target.value)}
        required
      />
      <Submit          
        type='submit'
        aria-label='Submit Bio'
        className='save-button button'
      >
        Save Bio
      </Submit>
    </Form>
    </div>
  )
}

export default NotesForm