import React from "react"
import fetch from 'isomorphic-fetch'
import {
  useIdentityContext,
} from 'react-netlify-identity-widget'

function Home() {
  const [data, setData] = React.useState(null)
  const [loading, setLoading] = React.useState(false)
  const [err, setErr] = React.useState("")
  const identity = useIdentityContext()
  const user =
  (identity && identity.user && identity.user.user_metadata && identity.user.user_metadata.full_name) || 'NoName'
  const token =
  (identity && identity.token && identity.token.access_token) || 'NoToken'

  console.log(JSON.stringify(identity))

  const handleClick = e => {
    e.preventDefault()
    setLoading(true)
    fetch("/.netlify/functions/auth-hello", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token ? `Bearer ${token}` : "",
      },
    })
      .then(response => response.json())
      .then(json => {
        setLoading(false)
        setData(json)
      })
      .catch(err => {
        if (window.location.origin === "http://localhost:8000")
          setErr(
            'your origin is "http://localhost:8000". You are likely not using Netlify Dev so the functions server isnt running. Please read the docs, use Netlify Dev, and go to http://localhost:8888'
          )
        else setErr(err)
        throw err
      })
  }

  return (
    <>
      <section>
        <h1>Api Test</h1>
        <div>Must Be Logged in With Netlify on the site menu For API test.</div>
        <ul>
          <li>API: {user.api && user.api.apiURL}</li>
          <li>ID: {user.full_name}</li>
        </ul>
        <hr />
        <button className='button' onClick={handleClick}>
          {loading ? "Loading..." : "Call Lambda Function"}
        </button>
        {err && <pre>{JSON.stringify(err, null, 2)}</pre>}
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </section>
    </>
  )
}

export default Home
