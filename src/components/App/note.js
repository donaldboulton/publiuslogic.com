import React from "react"
import axios from "axios"
import Form from './notesForm'

const params =  {
  "wait": false,
  "format": "json"
}
const config = {
  headers: {
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
    "Content-Type": "application/json"
  }
}

const Note = ({ note, reloadNotes }) => {
  const handleDelete = () => {
    axios.post('/api/delete-note', { id: note._id}).then(reloadNotes), params, config
  }

  return (
    <> 
      <section>
        <p>{note.text}</p>
        <button className='button' onClick={handleDelete}>Delete</button>
      </section>
      <section>
        <Form />
      </section>      
    </>
  )
}

export default Note