import React from 'react'
import { SettingToggleWrapper } from './styles'

const SettingIcon = (props) => {
  const className = props.className || ''
  return (
    <SettingToggleWrapper onClick={props.onClick} className={`toggle-wrapper ${className}`}>
     ⛭
    </SettingToggleWrapper>
  )
}

export default SettingIcon
