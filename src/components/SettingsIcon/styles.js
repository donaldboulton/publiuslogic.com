import styled from 'styled-components'

export const SettingToggleWrapper = styled.div`
  display: inline;
  justify-content: center;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`
