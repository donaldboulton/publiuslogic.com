"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "NavLink", {
  enumerable: true,
  get: function get() {
    return _styles.NavLink;
  }
});
Object.defineProperty(exports, "ArrowDown", {
  enumerable: true,
  get: function get() {
    return _KeyboardArrowDown.KeyboardArrowDown;
  }
});
Object.defineProperty(exports, "ArrowUp", {
  enumerable: true,
  get: function get() {
    return _KeyboardArrowUp.KeyboardArrowUp;
  }
});
exports.ControlsDiv = exports.NavToggle = exports.Children = exports.Item = exports.MobileNavDiv = void 0;

var _reactSpring = require("react-spring");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _ThMenu = require("@styled-icons/typicons/ThMenu");

var _styles = require("../styles");

var _XCircle = require("@styled-icons/boxicons-regular/XCircle");

var _mediaQueries = _interopRequireDefault(require("../../../utils/mediaQueries"));

var _KeyboardArrowDown = require("@styled-icons/material/KeyboardArrowDown");

var _KeyboardArrowUp = require("@styled-icons/material/KeyboardArrowUp");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n  grid-auto-flow: column;\n  border-bottom: 1px solid ", ";\n  padding-bottom: 0.3em;\n  align-items: center;\n  justify-content: space-between;\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n  transition: 0.3s;\n  cursor: pointer;\n  :hover {\n    transform: scale(1.1);\n  }\n  ", " {\n    display: none;\n  }\n  ", ";\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  z-index: 2;\n  position: fixed;\n  bottom: 11.5vh;\n  left: 0;\n  padding: 0.4em 0.5em 0.4em 0.2em;\n  background: ", ";\n  border: 2px solid gray;\n  color: ", ";\n  border-radius: 0 50% 50% 0;\n  transform: translate(", ");\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  will-change: transform, opacity, height;\n  margin-left: 0.5em;\n  padding-left: 0.8em;\n  border-left: thin dashed white;\n  overflow: hidden;\n  height: 105%;\n  padding-bottom: ", ";\n  > div {\n    height: max-content;\n    margin-top: 0.6em;\n    margin-bottom: 0.6em;\n    display: grid;\n    grid-gap: 0.6em;\n  }\n  * > :last-child {\n    margin-bottom: 1em !important; \n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  a {\n    color: #f5f5f5;\n  }\n  /* Target arrow icons prefixing nav links with children. */\n  svg:first-child {\n    width: 1em;\n    margin-right: 0.3em;\n    cursor: pointer;\n    vertical-align: -0.1em;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  z-index: 2;\n  position: fixed;\n  top: 0;\n  height: max-content;\n  max-width: 80vw;\n  overflow-y: auto;\n  overflow-x: hidden;\n  -webkit-overflow-scrolling: touch;\n  height: 100%;\n  background: #1d1d1d !important;\n  color: #f5f5f5;\n  padding: 6vmin;\n  font-size: 1.2em;\n  right: 100%;\n  display: grid;\n  grid-gap: 1em;\n  grid-auto-columns: max-content;\n  grid-auto-rows: max-content;\n  transform: translate(", ");\n  transition: 0.3s;\n  /* Needed to scroll past last element in case of overflow. */\n  :after {\n    content: '';\n    height: 1em;\n    margin-bottom: 1em;\n    padding-bottom: 1em;\n  }\n  nav {\n    max-height: 70vh;\n    overflow-x: hidden;\n  }\n  .nav-scroll {\n    overflow-y: auto;\n    scrollbar-color: linear-gradient(to bottom,#201c29,#100e17);\n    scrollbar-width: 10px;\n    overflow-x: hidden;\n    padding-inline-start: 2em !important;\n    margin-top: 1em;\n  }\n  .nav-scroll::-webkit-scrollbar {\n    width: 10px;\n    height: 10px;\n  }\n  .nav-scroll::-webkit-scrollbar-thumb {\n    background: -webkit-gradient(linear,left top,left bottom,from(#d201c29),to(#100e17));\n    background: linear-gradient(to bottom,#201c29,#100e17);\n    border-radius: 10px;\n    -webkit-box-shadow: inset 2px 2px 2px rgba(255,255,255,.25),inset -2px -2px 2px rgba(0,0,0,.25);\n    box-shadow: inset 2px 2px 2px rgba(255,255,255,.25),inset -2px -2px 2px rgba(0,0,0,.25);\n  }\n  .nav-scroll::-webkit-scrollbar-track {\n    background: linear-gradient(to right,#201c29,#201c29 1px,#100e17 1px,#100e17);\n    border-bottom: 10px;\n    border-top: 10px;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var MobileNavDiv = _styledComponents["default"].nav(_templateObject(), function (props) {
  return props.open ? "99%" : "0";
});

exports.MobileNavDiv = MobileNavDiv;

var Item = _styledComponents["default"].div(_templateObject2());

exports.Item = Item;
var Children = (0, _styledComponents["default"])(_reactSpring.animated.div)(_templateObject3(), function (props) {
  return props.open && "0.6em";
});
exports.Children = Children;
var openerCss = (0, _styledComponents.css)(_templateObject4(), function (props) {
  return props.theme.black;
}, function (props) {
  return props.theme.white;
}, function (props) {
  return props.open ? "-100%" : 0;
});
var NavToggle = (0, _styledComponents["default"])(_XCircle.XCircle).attrs(function (props) {
  return {
    as: props.opener && _ThMenu.ThMenu,
    size: props.opener ? "2em" : "2.4em"
  };
})(_templateObject5(), function (_ref) {
  var theme = _ref.theme,
      opener = _ref.opener;
  return opener ? theme.white : "white";
}, _mediaQueries["default"].minLaptop, function (props) {
  return props.opener && openerCss;
});
exports.NavToggle = NavToggle;

var ControlsDiv = _styledComponents["default"].div(_templateObject6(), function (props) {
  return props.theme.white;
});

exports.ControlsDiv = ControlsDiv;