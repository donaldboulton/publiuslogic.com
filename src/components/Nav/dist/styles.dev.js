"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NavLink = void 0;

var _gatsby = require("gatsby");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  color: inherent;\n  transition: 0.3s;\n  &.active {\n    color: ", ";\n  }\n  :hover {\n    color: ", ";\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NavLink = (0, _styledComponents["default"])(_gatsby.Link).attrs({
  activeClassName: "active",
  partiallyActive: true
})(_templateObject(), function (props) {
  return props.theme.links;
}, function (props) {
  return props.theme.hoveredLinks;
});
exports.NavLink = NavLink;