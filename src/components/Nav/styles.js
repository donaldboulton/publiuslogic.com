import { Link } from 'gatsby'
import styled from 'styled-components'

export const NavLink = styled(Link).attrs({
  activeClassName: `active`,
  partiallyActive: true,
})`
  color: inherent;
  transition: 0.3s;
  &.active {
    color: ${props => props.theme.links};
  }
  :hover {
    color: ${props => props.theme.hoveredLinks};
  }
`
