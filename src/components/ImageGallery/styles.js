import styled from 'styled-components'
import Img from 'gatsby-image'

export { Img }

export const Thumbnail = styled(Img)`
  transition: 0.3s;
  width: 100%;
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  :hover {
    transform: scale(1.05);
  }
`