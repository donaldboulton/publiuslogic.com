import React, { useState } from "react"
import PT from "prop-types"
import { useStaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import { LightgalleryProvider, LightgalleryItem } from 'react-lightgallery'
import "lightgallery.js/dist/css/lightgallery.css"
import Masonry from 'react-masonry-css'

const SectionTitle = styled.h3`
  font-size: 1.5em;
  margin: 0.67em 0;
`
const Caption = styled.div`
  top: -1em;
  text-align: center;
  border-radius: 0px 0px 4px 4px;
}
`

function ImageGallery({ image, thumb, group, ...rest }) {
  const data = useStaticQuery(graphql`
    query CloudinaryImage {
      allCloudinaryMedia(sort: {fields: tags, order: ASC}) {
        edges {
          node {
            public_id
            resource_type
            secure_url
            tags
            context {
              custom {
                alt
                caption
              }
            }
          }
        }
      }
    }
  `)
  const clImages = data.allCloudinaryMedia.edges
  const [open, setOpen] = useState(false)
  const breakpointColumnsObj = {
    default: 3,
    1100: 3,
    700: 2,
    500: 1
  }

  return (
    <>
      <SectionTitle>Cat Gallery</SectionTitle>
      <LightgalleryProvider>
        <Masonry
          breakpointCols={breakpointColumnsObj}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {clImages.map((image, index) => (
            <LightgalleryItem key={`${index}-cl`} group='any' src={image.node.secure_url} thumb={image.node.secure_url}>
              <img
                className='my-masonry-img'
                src={image.node.secure_url}
                alt={image.node.context.custom.alt}
                onClick={() => setOpen(true)}
              >
              </img>
              <Caption className='caption' data-sub-html={image.node.context.custom.caption}>{image.node.context.custom.caption}</Caption>
            </LightgalleryItem>
          ))}
        </Masonry>
      </LightgalleryProvider>
      <hr
        style={{
          marginBottom: 2,
        }}
      />
    </>
  )
}

ImageGallery.propTypes = {
  image: PT.string.isRequired,
  thumb: PT.string,
  group: PT.string.isRequired
};

export default ImageGallery