import React from 'react'
import { OutboundLink } from "gatsby-plugin-google-gtag"
import cssTricks from '../../../static/img/css-tricks-star-colors-164.webp'

const CssTricksAds = () => {
  return (
    <>
      <div className='card card2'>
        <div className='message-body'>
          <div>
            <OutboundLink title='Css Tricks' href='https://css-tricks.com/' target='_blank' rel='noopener noreferrer'>
              <img
                className='icon-logo-star'
                src={cssTricks}
                alt='Css Tricks'
              />
            </OutboundLink>
          </div>
          <div>
            <OutboundLink title='Css Tricks Inspiration' href='https://css-tricks.com/' target='_blank' rel='noopener noreferrer'>
              <strong>Css Tricks Inspiration's</strong>
            </OutboundLink>
          </div>
          <p><h3 className='a'>Front-End UI, Javascript & Functions, HTML Tips, and More!</h3></p>
        </div>
      </div>
    </>
  )
}

export default CssTricksAds
