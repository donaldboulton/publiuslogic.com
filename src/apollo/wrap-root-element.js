  
import React from 'react';
import { ApolloProvider, ApolloClient, HttpLink, ApolloLink, InMemoryCache, concat } from '@apollo/client';
import fetch from 'isomorphic-fetch'
import { getToken } from "../networking/auth"

const token =
  getToken() ||
    process.env.GATSBY_DEVELOPER_FAUNADB_SECRET_KEY ||
    process.env.DEVELOPER_FAUNADB_SECRET_KEY

const httpLink = new HttpLink({ 
  uri: process.env.GRAPHQL_ENDPOINT, 
  credentials: 'same-origin', 
  fetch 
});

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }));

  return forward(operation);
})

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink),
});

export const wrapRootElement = ({ element }) => (
  <ApolloProvider client={client}>{element}</ApolloProvider>
);