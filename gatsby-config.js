const config = require('./_data/config')
const queries = require(`./src/utils/algolia`)
const siteAcronyms = require('./gatsby-site-acronyms')

require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})
const { createProxyMiddleware } = require("http-proxy-middleware") //v1.x.x
// Use implicit require for v0.x.x of 'http-proxy-middleware'
// const proxy = require('http-proxy-middleware')
// be sure to replace 'createProxyMiddleware' with 'proxy' where applicable
module.exports = {
  siteMetadata: {
    siteTitle: 'PubliusLogic',
    title: 'PubliusLogic',
    titleTemplate: 'To Publish Logic',
    titleAlt: 'To Publish Logic',
    siteDescription: 'PubliusLogic is built and written by Donald Boulton, I write about God, Logic gov and tech on my blogs',
    author: 'Donald Boulton',
    siteUrl: 'https://publiuslogic-old.netlify.app',
    keywords: 'Publiuslogic, React',
    image: '/img/icon.png',
    twitterUserName: 'donboulton',
    twitterSite: '1135998',
    headline: 'Writing and publishing content for PubliusLogic', // Headline for schema.org JSONLD
    url: 'https://publiuslogic-old.netlify.app',
    siteLanguage: 'en', // Language Tag on <html> element
    logo: '/img/logo.png', // Used for SEO
    ogLanguage: 'en_US', // Facebook Language
    twitter: 'donboulton',
    facebook: 'don.boulton',
    gitHubEditMe: 'https://github.com/donaldboulton/publiuslogic/blob/master/src/pages/',
    social: {
      twitter: `donboulton`,
    },
    rssMetadata: {
      site_url: 'https://publiuslogic-old.netlify.app',
      feed_url: config.siteUrl + config.siteRss,
      title: config.siteTitle,
      description: config.siteDescription,
      image_url: `${config.siteUrl}/logos/icon-512x512.png`,
      author: config.userName,
      copyright: config.copyright,
      twitterCreator: `@donboulton`,
      social: {
        twitter: `donboulton`,
      },
    },
  },
  developMiddleware: app => {
    app.use(
      "/.netlify/functions/",
      createProxyMiddleware({
        target: "http://localhost:9000",
        pathRewrite: {
          "/.netlify/functions/": "",
        },
      })
    )
  },
  plugins: [    
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/content/assets/img`,
        name: 'uploads',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/images`,
        name: `images`,
      },
    },
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `comments`,
        path: `${__dirname}/_data/comments`,
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/static/logos`,
        name: 'logos',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/static/img`,
        name: 'icons',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'tech',
        path: `${__dirname}/_data/tech`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'nav',
        path: `${__dirname}/_data/nav`,
      },
    },
    {
      resolve: 'gatsby-transformer-remark',
        options: {
        plugins: [
          {
            resolve: 'gatsby-remark-copy-linked-files',
            options: {
              destinationDir: 'static',
            },
          },
          {
            resolve: 'gatsby-remark-embed-video',
            options: {
              width: 800,
              ratio: 1.77, // Optional: Defaults to 16/9 = 1.77
              height: 400, // Optional: Overrides optional.ratio
              related: false, // Optional: Will remove related videos from the end of an embedded YouTube video.
              noIframeBorder: true, // Optional: Disable insertion of <style> border: 0
              loadingStrategy: 'lazy', 
            },
          },
          `gatsby-remark-code-titles`,
          {
            resolve: 'gatsby-remark-component',
            options: { components: ['interactive-accordion', 'interactive-center-span', 'audio', 'custom-image', 'interactive-upload-widget', 'interactive-cloudinary-video','interactive-counter', 'interactive-todo', 'interactive-colorbox', 'interactive-contact', 'interactive-comments', 'interactive-fork', 'interactive-issues', 'custom-image', 'sLide-presentations', 'interactive-sparkles', `interactive-center`, `interactive-comment-form`, `interactive-comments`, `interactive-gallery`, `interactive-arrow`, `interactive-cloudinary-video`, 'subscriptions',] },
          },
          {
            resolve: 'gatsby-remark-images',
            options: {
              maxWidth: 800,
              linkImagesToOriginal: false,
              sizeByPixelDensity: true,
              showCaptions: true,
              backgroundColor: 'none',
              disableBgImage: true,
              withWebp: true
            }
          },
          {
            resolve: `gatsby-remark-acronyms`,
            options: {
              acronyms: siteAcronyms,
            },
          },
          {
            resolve: 'gatsby-remark-normalize-paths',
            options: {
              pathFields: ['image', 'cover'],
            },
          },
          {
            resolve: `gatsby-remark-autolink-headers`,
            options: {
              offsetY: `100`,
              icon: `<svg aria-hidden='true' height='20' version='1.1' viewBox='0 0 16 16' width='20'><path fill='#d64000' d='M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z'></path></svg>`,
              className: `link-icon`,
              maintainCase: true,
              removeAccents: true,
            },
          },
          {
            resolve: `gatsby-remark-prismjs`,
              options: {
                classPrefix: "language-",
                inlineCodeMarker: null,
                aliases: {},
                showLineNumbers: false,
                noInlineHighlight: false,
                languageExtensions: [
                  {
                    language: "superscript",
                    extend: "javascript",
                    definition: {
                    superscript_types: /(SuperType)/,
                  },
                  insertBefore: {
                  function: {
                    superscript_keywords: /(superif|superelse)/,
                  },
                },
              },
            ],
            // Customize the prompt used in shell output
            // Values below are default
            prompt: {
              user: "root",
              host: "localhost",
              global: false,
            },
            // By default the HTML entities <>&'" are escaped.
            // Add additional HTML escapes by providing a mapping
            // of HTML entities and their escape value IE: { '}': '&#123;' }
            escapeEntities: {},
          },
          },
          {
            resolve: 'gatsby-remark-relative-images',
            options: {
              name: 'images',
            },
          },
          {
            resolve: "gatsby-remark-images-anywhere",
            options: {
              staticDir: "static",
              createMarkup: ({
                src,
                srcSet,
                sizes,
                aspectRatio,
                alt,
                base64,
                presentationWidth,
              }) => {
                return `<custom-image src="${src}" srcset="${srcSet}" sizes="${sizes}" aspectratio="${aspectRatio}" alt="${alt}" base64="${base64}" presentationwidth="${presentationWidth}"></custom-image>`
              },
              sharpMethod: "fluid",
              backgroundColor: 'none',
              loading: 'lazy',
              wrapperStyle: 'background-color: transparent;',
              // Additional sharp image arguments: https://www.gatsbyjs.org/packages/gatsby-plugin-sharp/
              maxWidth: 600,
            },
          },
          {
            resolve: "gatsby-remark-external-links",
            options: {
              target: "_blank",
              rel: "nofollow noopener noreferrer"
            }
          },
          {
            resolve: `gatsby-remark-emojis`,
            options: {
              active: true,
              size: 24,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-transformer-sharp`,
      options: {
        // The option defaults to true
        checkSupportedExtensions: false,
      },
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        stripMetadata: true,
      },
    },
    {
      resolve: "gatsby-plugin-anchor-links",
      options: {
        offset: -100,
        duration: 1000
      }
    },
    `gatsby-plugin-catch-links`,
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: [
          'UA-24847941-1',
        ],
        gtagConfig: {
          anonymize_ip: true,
        },
        pluginConfig: {
          head: false,
          respectDNT: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-use-dark-mode`,
      options: {
        classNameDark: `dark-mode`,
        classNameLight: `light-mode`,
        storageKey: `darkMode`,
        minify: true,
      },
    },
    'gatsby-plugin-styled-components',
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-plugin-purgecss',
      options: {
        develop: true,
        purgeOnly: ['/content/assets/sass/styles.scss'],
      },
    },
    {
      resolve: `gatsby-plugin-postcss`, // Implements PostCSS
      options: {
        postCssPlugins: [
          require('postcss-import')(), // Add support for sass-like '@import'
          require('postcss-extend')(), // Add support for sass-like '@extend'
          require('postcss-nesting')(), // Add support for sass-like nesting of rules
          require('postcss-pxtorem')({
            mediaQuery: true, // Ignore media queries
            minPixelValue: 0, // Minimal pixel value that will be processed
            propList: [], // List of CSS properties that can be changed from px to rem; empty array means that all CSS properties can change from px to rem
            replace: true, // Replace pixels with rems
            rootValue: 20, // Root font-size
            selectorBlackList: ['html'], // Ignore pixels used for html
            unitPrecision: 4, // Round rem units to 4 digits
          }),
          require(`postcss-preset-env`)({ stage: '3' }),
          require('cssnano')(), // Minify CSS
        ],
      },
    },
    {
      resolve: `gatsby-plugin-algolia`,
      options: {
        appId: process.env.GATSBY_ALGOLIA_APP_ID,
        apiKey: process.env.ALGOLIA_ADMIN_KEY,
        indexName: process.env.GATSBY_ALGOLIA_INDEX_NAME,
        queries,
        chunkSize: 10000, // default: 1000
        enablePartialUpdates: true,
        skipIndexing: false,
        matchFields: ['slug'],
      },
    },
    'gatsby-plugin-robots-txt',
    `gatsby-plugin-twitter`,
    'gatsby-transformer-yaml',
    `gatsby-transformer-json`,
    {
      resolve: 'gatsby-plugin-netlify-cms',
      options: {
        modulePath: `${__dirname}/src/cms/cms.js`,
      },
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: config.themeColor,
        showSpinner: false,
      },
    },
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/app/*`] },
    },    
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: config.siteTitle,
        short_name: config.siteTitleAlt,
        start_url: '/index.html',
        background_color: config.backgroundColor,
        theme_color: config.themeColor,
        display: 'standalone',
        cache_busting_mode: 'none',
        icons: [
          {
            src: `/img/icon-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/img/icon-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-feed',
      options: {
        setup (ref) {
          const ret = ref.query.site.siteMetadata.rssMetadata
          ret.allMarkdownRemark = ref.query.allMarkdownRemark
          ret.generator = config.siteTitle
          return ret
        },
        query: `
                {
                  site {
                    siteMetadata {
                      rssMetadata {
                        site_url
                        feed_url
                        title
                        description
                        image_url
                        author
                        copyright
                      }
                    }
                  }
                }
              `,
        feeds: [
          {
            serialize (ctx) {
              const rssMetadata = ctx.query.site.siteMetadata.rssMetadata
              return ctx.query.allMarkdownRemark.edges
                .filter(
                  edge => edge.node.frontmatter.templateKey === 'posts'
                )
                .map(edge => ({
                  tags: edge.node.frontmatter.tags,
                  date: edge.node.frontmatter.date,
                  cover: edge.node.frontmatter.cover,
                  title: edge.node.frontmatter.title,
                  category: edge.node.frontmatter.category,
                  description: edge.node.excerpt,
                  author: rssMetadata.author,
                  url: rssMetadata.site_url + edge.node.fields.slug,
                  guid: rssMetadata.site_url + edge.node.fields.slug,
                  custom_elements: [{ 'content:encoded': edge.node.html }],
                }))
            },
            query: `
                    {
                      allMarkdownRemark(
                        limit: 1000,
                        sort: { order: DESC, fields: [frontmatter___date] },
                      ) {
                        edges {
                          node {
                            excerpt(pruneLength: 400)
                            html
                            id
                            fields { slug }
                            frontmatter {
                              title
                              templateKey
                              cover
                              date(formatString: "MMMM DD, YYYY")
                              category
                              tags
                              tweet_id
                            }
                          }
                        }
                      }
                    }
                  `,
            output: config.siteRss,
            title: "Don Boulton PubliusLogic Blog RSS Feed",
          },
        ],
      },
    },
    `gatsby-plugin-lodash`,
    {
      resolve: `gatsby-source-cloudinary`,
      options: {
        cloudName: process.env.CLOUDINARY_CLOUD_NAME,
        apiKey: process.env.CLOUDINARY_API_KEY,
        apiSecret: process.env.CLOUDINARY_API_SECRET,
        resourceType: `image`,
        prefix: `photos/`,
        context: true,
        tags: true,
      },
    },
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [`/home`, `/about`, `/contact`, `/gallery`, `/blog/*`],
        workboxConfig: {
          importWorkboxFrom: `cdn`,
        },
      },
    },
    {
      resolve: `gatsby-plugin-netlify`,
      options: {
        headers: {
          '/*.js': [
            'cache-control: max-age=86400, public'
          ],
          '/*.css': [
            'cache-control: max-age=31536000, public'
          ],
          '/sw.js': [
            'cache-control: max-age=0, public, must-revalidate'
          ],
          "/public/page-data/*": [
            "cache-control: max-age=0, public, must-revalidate"
          ],
        },
      },
    },
  ],
}